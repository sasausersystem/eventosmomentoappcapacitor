import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AccesosService } from 'src/app/services/accesos.service';
import { AnimacionesService } from 'src/app/services/animaciones.service';
import { ApiRestService } from 'src/app/services/api-rest.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  employe = {
    email: '',
    token: ''
  };

  datosEmpleado: any = [];

  response : any = {};

  error : any;

  constructor(
    private Router: Router,
    private SAccesso: AccesosService, 
    private SApi: ApiRestService,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private anima: AnimacionesService,
  ) { 
    this.error = 0;
    this.SAccesso.comprobeLogin();
  }

  ionViewWillEnter(){
    let token = this.route.snapshot.paramMap.get('token');
    if(token){
      this.employe.token = token;
      this.verifyToken();
    }
    
  }  

  ngOnInit() {
    
  }

  goToIndex(){
    this.anima.transitionWithSlide('right', 400);
    this.navCtrl.navigateRoot('/');
  }  

  verifyTwoFactory(){
    this.SApi.EmpleadoQueryByEmail( this.employe.email ).then((done:any)=>{

      this.response = done;
      this.error    = done.error;
      
    }, (error)=>{

      this.response = null;
      this.error    = 1;

    });
  }

  async verifyToken(){

    this.SApi.tokenVerify( this.employe.token ).then( (done:any)=>{

      if(done.error == null){

        this.SAccesso.iniciarSession(done.data);
        this.datosEmpleado = this.SAccesso.obtenerDatos('empleadoConectado');
        //this.Router.navigate(['/index']);
        this.anima.transitionWithSlide('left', 400);
        this.navCtrl.navigateRoot('/');

      }

    }, (error:any)=>{

    } );
    
  }

}
