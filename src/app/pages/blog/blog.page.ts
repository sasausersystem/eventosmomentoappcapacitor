import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { ApiRestService } from 'src/app/services/api-rest.service';
import { HelperGlobalService } from 'src/app/services/helper-global.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.page.html',
  styleUrls: ['./blog.page.scss'],
})
export class BlogPage implements OnInit {

  articulos : any    = [];
  error     : number = 0;
  loader    : any    = null;

  constructor(
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private navigCtrl: NavController,
    private helper:    HelperGlobalService,
    private SApi:      ApiRestService
  ) { }

  ngOnInit() {
  }

}
