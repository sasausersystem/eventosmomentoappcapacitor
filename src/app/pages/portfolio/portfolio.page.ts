import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { PortfolioDetailtPage } from 'src/app/modals/portfolio-detailt/portfolio-detailt.page';
import { AnimacionesService } from 'src/app/services/animaciones.service';
import { ApiRestService } from 'src/app/services/api-rest.service';
import { HelperGlobalService } from 'src/app/services/helper-global.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.page.html',
  styleUrls: ['./portfolio.page.scss'],
})
export class PortfolioPage implements OnInit {

  constructor(
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private navigatCtrl: NavController,
    private animacServ: AnimacionesService,
    private helper: HelperGlobalService,
    private SApi: ApiRestService
  ) { }

  datos : any = [];
  error : number = 0;

  ngOnInit() {
    this.showLoader();
    this.findPortfolio();
  }

  findPortfolio(){
    this.SApi
    .SiteQueryPortfolio()
    .subscribe( (data: any) => {
      if(data.error == null){
        this.error = 0;
        this.datos = data.data;
        return false;
      }

      this.datos = [];
      this.error = 1;
      return false;
    } );
  }

  async showLoader(){
    const loadingE = await this.loadingCtrl.create({
      message: 'Cargando',
      duration: 5000,
    });

    await loadingE.present();
    await loadingE.dismiss();
  }

  async openDetail(data: any){
    let modal = this.modalCtrl.create({
      component: PortfolioDetailtPage,
      componentProps: data,
    });

    return ( await modal ).present();
  }

  /**NAVIGATION FUNCTIONS**/
  goToIndex(){
    this.helper.goPageWithAnimation('fade', 'index', 'left', 500);
  }
  /**NAVIGATION FUNCTIONS**/


}
