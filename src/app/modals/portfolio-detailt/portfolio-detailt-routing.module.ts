import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PortfolioDetailtPage } from './portfolio-detailt.page';

const routes: Routes = [
  {
    path: '',
    component: PortfolioDetailtPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PortfolioDetailtPageRoutingModule {}
