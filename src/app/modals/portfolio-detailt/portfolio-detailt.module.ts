import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PortfolioDetailtPageRoutingModule } from './portfolio-detailt-routing.module';

import { PortfolioDetailtPage } from './portfolio-detailt.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PortfolioDetailtPageRoutingModule
  ],
  declarations: [PortfolioDetailtPage]
})
export class PortfolioDetailtPageModule {}
