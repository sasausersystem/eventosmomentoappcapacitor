import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.page.html',
  styleUrls: ['./activities.page.scss'],
})
export class ActivitiesPage implements OnInit {

  @Input() titulo : string;
  @Input() text : string;
  @Input() image : string;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }



  closeModal(){
    this.modalCtrl.dismiss();
  }

}
