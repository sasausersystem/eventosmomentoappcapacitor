import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { AccesosService } from '../services/accesos.service';
import { ApiRestService } from '../services/api-rest.service';
import { HelperGlobalService } from '../services/helper-global.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {

  constructor(
    public navCtrl: NavController,
    public modCtrl: ModalController,
    
    private router: Router,
    private zone: NgZone,
    private changeDetect: ChangeDetectorRef,
    private sanitaizer: DomSanitizer,
    private SApi: ApiRestService,
    private SAccesso: AccesosService, 
    private helper: HelperGlobalService
  ) {

    this.SAccesso.comprobeLogin();

  }

  reponse : any;

  employe = {
    email: '',
    token: ''
  };

  datosMomento : any = {
    'facebook': 'https://www.facebook.com/eventosmomentomx',
    'youtube' : 'https://www.youtube.com/channel/UCS2tDAP429LenqdSprPApmw',
    'instagram': 'https://www.instagram.com/eventosmomentomx/',
    'twitter' : 'https://twitter.com/eventomomentomx',
    'whatsapp': 'https://wa.me/+525541847954',
    'web'     : 'https://eventosmomento.com/'
  };

  isConnected : any;

  datosEmpleado: any = [];

  empresa     : any = Object;

  colors      : any = Object;

  eventos    : any = [];

  notificiaciones : any = [];

  itinhome : any = [];

  enlaceeventoitin : string;



  ngOnInit() {
  }

  ionViewWillLeave(){
    this.SAccesso.comprobeLogin();
  }

  ionViewWillEnter(){
    this.syncUp();
  }

  goToLogin(){
    //this.helper.openPage('/login', "left", 800);
    this.helper.goTo();
  }

  goToPortafolio(){
    this.helper.goPageWithAnimation('fade', 'portfolio', 'left', 500);
  }

  doRefresh(event:any){

    setTimeout(() => {
      this.syncUp();
      event.target.complete();
    }, 2000);
  
  }

  /**GO TO DETAIL EVENT**/
  goToDetaildEvent(url: string){
    //alert(url);
    this.helper.goPageWithAnimation('flip', url, 'left', 500);
  }
  /**GO TO DETAIL EVENT**/

  /**CLOSE SESSION USER */
  cerrar(){
    this.SAccesso.cerrarSession().then( (resp) => {
      this.helper.openPage('/index', 'right', 400);
      this.syncUp();
    } );
  }
  /**CLOSE SESSION USER */

  syncUp(){
    this.SAccesso.isConectado()
    .then( (done) => {
      this.eventos = [];
      this.notificiaciones = [];
      this.isConnected = done;

      if( this.isConnected === true )
      {
        this.SAccesso.obtenerDatos('empleadoConectado').then((done)=>{
          this.datosEmpleado = done;
  
          this.SApi.ItinerarioQueryByTime(parseInt(this.datosEmpleado.id)).subscribe( (done:any) => {
  
            if(  done.error == null  ){
              this.itinhome = done.data;
              if(done.data.length > 0){
                this.enlaceeventoitin = '/evento/'+this.datosEmpleado.id+'/'+done.data[0].ideven;
              }
              
            } else {
              this.itinhome = [];
            }
  
          } );
          
          //DATOS EVENTOS POR EMPRESA
          this.SApi.EventosQueryByEmpresa( parseInt(this.datosEmpleado.id) ).subscribe((done:any)=>{
            if(done.error == null){
              
              done.data.forEach((element:any) => {
                this.eventos.push({
                  id: element.id,
                  titulo: element.titulo,
                  subtitulo: element.subtitulo,
                  image: element.mainimage,
                  enlace: '/evento/'+this.datosEmpleado.id+'/'+element.id
                });
              });
  
            } else {
              this.eventos = null;
            }
          });
                    
          //DATOS EMPRESA.....................................................
          this.SApi.EmpresaQueryById( parseInt(this.datosEmpleado.empresa) ).subscribe((done:any)=>{
  
            this.empresa ={
              nombre: done.data.nombre,
              descripcion: done.data.descripcion,
              facebook: this.sanitaizer.bypassSecurityTrustUrl('https://facebook.com/'+done.data.facebook),
              haveFb  : (done.data.facebook!="") ? true : false,
              twitter:  this.sanitaizer.bypassSecurityTrustUrl('https://twitter.com/'+done.data.twitter),
              haveTw  : (done.data.twitter!="") ? true : false,
              youtube:  this.sanitaizer.bypassSecurityTrustUrl('https://www.youtube.com/channel/'+done.data.youtube_channel),
              haveYt  : (done.data.youtube_channel!="") ? true : false,
              instagram:  this.sanitaizer.bypassSecurityTrustUrl('https://www.instagram.com/'+done.data.instagram),
              haveIg  : (done.data.instagram!="") ? true : false,
              web: this.sanitaizer.bypassSecurityTrustUrl('https://'+done.data.paginaweb),
              haveWb  : (done.data.paginaweb!="") ? true : false,
              telefono: done.data.telefono,
              direccion: done.data.direccion,
              colors: {
                primary: done.data.primary_color,
                background: done.data.background_color,
                schema: 'https://app.eventosmomento.com' + done.data.esquema
              },
              logo: done.data.Intro_imagen
            };
  
           
  
            this.colors.background = done.data.background_color;
            this.colors.primary    = done.data.primary_color;
            this.colors.schema     = 'https://app.eventosmomento.com' + done.data.esquema;          
  
          },(error:any)=>{
  
          });
        },(error)=>{
  
        });
      }
      else
      {

      }

    } )
    .catch( (error) => {} );
  }

}
