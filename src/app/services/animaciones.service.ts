import { Injectable } from '@angular/core';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';

@Injectable({
  providedIn: 'root'
})
export class AnimacionesService {

  constructor(private npt: NativePageTransitions) { }


  transitionWithFade(dir: string = "left", time:number){
    let options: NativeTransitionOptions = {
      direction: dir,
      duration: time,
      slowdownfactor: -1,
      iosdelay: 100,
      androiddelay: 150
    }

    this.npt.fade(options).then((done:any) => {
      console.log(done);
    }, (error:any) => {
      console.error(error);
    });
  }

  transitionWithFlip(dir: string = "left", time:number){
    let options: NativeTransitionOptions = {
      direction: dir,
      duration: time,
      slowdownfactor: -1,
      iosdelay: 100,
      androiddelay: 150
    }

    this.npt.flip(options).then((done:any) => {
      console.log(done);
    }, (error:any) => {
      console.error(error);
    });
  }

  transitionWithSlide(dir: string = "left", time:number){
    let options: NativeTransitionOptions = {
      direction: dir,
      duration: time,
      slowdownfactor: -1,
      iosdelay: 100,
      androiddelay: 150
    }

    this.npt.slide(options).then((done:any) => {
      console.log(done);
    }, (error:any) => {
      console.error(error);
    });
  }

  transitionWithCurl(dir: string = "left", time:number){
    let options: NativeTransitionOptions = {
      direction: dir,
      duration: time,
      slowdownfactor: -1,
      iosdelay: 100,
      androiddelay: 150
    }

    this.npt.curl(options).then((done:any) => {
      console.log(done);
    }, (error:any) => {
      console.error(error);
    });
  }
}
