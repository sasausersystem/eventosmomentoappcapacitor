import { TestBed } from '@angular/core/testing';

import { HelperGlobalService } from './helper-global.service';

describe('HelperGlobalService', () => {
  let service: HelperGlobalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HelperGlobalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
