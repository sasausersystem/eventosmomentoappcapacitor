import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class AccesosService {

  Empleado : any = {};

  constructor(private storage : Storage, private route: Router) { 
    this.storage.create();
  }

  iniciarSession(data:any){
    return this.storage.set('empleadoConectado', data);
  }

  cerrarSession(){
    return this.storage.remove('empleadoConectado');
  }

  obtenerDatos(key:string=null){

    return new Promise(resolve => {
      this.storage.get(key).then( (val) => {
        resolve(val);
      } );
    });

  }

  comprobeLogin(){
    this.isConectado().then((done:any) => {
      if(done == true && this.route.url == '/login'){
        this.route.navigate(['/index']);
      }
    });
  }

  isConectado() {
    return new Promise(resolve => {
      let bool: boolean;
      this.storage.get('empleadoConectado').then( (val) => {
        
        if(val == null){
  
          bool = false;
  
        } else {
          bool = true;
        }
        resolve(bool);
      } );
    });    
  }
}
