import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiRestService {

  URIBASE = 'https://app.eventosmomento.com/api/index.php';

  constructor(private httpClient: HttpClient) { }

  /** FOR EMPLOYES **/

  //Verificar por email
  EmpleadoQueryByEmail(email:string){
    return new Promise( (resolve, reject) => {

      var data   = JSON.stringify({
        correo: email
      });      

      this.httpClient.post(this.URIBASE+'?m=empresa&a=verifyByEmail', data).subscribe((done) => {
        resolve(done);
      }, (err) => {
        reject(err);
      });

    });
  }

  //Verificar token.
  tokenVerify(token:string){
    return new Promise( (resolve, reject) => {

      var data   = JSON.stringify({
        codigo: token
      });      

      this.httpClient.post(this.URIBASE+'?m=empresa&a=verifyToke', data).subscribe((done) => {
        resolve(done);
      }, (err) => {
        reject(err);
      });

    });
  }

  /** FOR EMPLOYES **/

  //Para empresas

  EmpresaQueryById(empresa:number){
    return this.httpClient.get( this.URIBASE + '?m=empresas&a=byId&p='+empresa );
  }

  EventosQueryByEmpresa(empleado:number){
    return this.httpClient.get( this.URIBASE + '?m=eventos&a=even_porempleado&p=' + empleado );
  }

  //Para eventos
  EventosQueryByEmpleado(empleado:number){

    return this.httpClient.get( this.URIBASE + '?m=eventos&a=even_porempleadoinvitado&p='+empleado );

  }

  EventoQueryById(id:number){
    return this.httpClient.get( this.URIBASE + '?m=eventos&a=even_detalle2&p=' + id );
  }

  EventoQueryComprobeInvitation(evento:number, usuario:number){
    return this.httpClient.get( this.URIBASE + '?m=eventos&a=invit_usuario_evento&p='+usuario+'&ev='+evento );
  }

  //Itinerario
  ItinerarioQueryByEvento(evento:number){
    return this.httpClient.get( this.URIBASE + '?m=itin&a=tabla_itine&p='+evento );
  }

  ItinerarioQueryByEmploye(employe:number){
    return this.httpClient.get( this.URIBASE + '?m=itin&a=empl_not&p='+employe );
  }
  
  ItinerarioQueryByTime(employe:number){
    return this.httpClient.get( this.URIBASE + '?m=itin&a=taskbycurrentday&p='+employe );
  }

  //PARTE PUBLICA

  SiteQueryPortfolio(){
    return this.httpClient.get( this.URIBASE + '?m=pub&a=all_portfolio');
  }

  SiteQueryBlog(){
    return this.httpClient.get( this.URIBASE + '?m=pub&a=all_blog');
  }

  SiteQueryServicios(){
    return this.httpClient.get( this.URIBASE + '?m=pub&a=all_serv');
  }

  SiteQueryAbout(){
    return this.httpClient.get( this.URIBASE + '?m=pub&a=who_we_are');
  }
}
