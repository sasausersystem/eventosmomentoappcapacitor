import { Component, NgZone, OnInit } from '@angular/core';

import { StatusBar, Style } from '@capacitor/status-bar';
import { Platform } from '@ionic/angular';

import { App } from '@capacitor/app';
import { Router } from '@angular/router';
import { LocalNotifications } from '@capacitor/local-notifications';
import { AccesosService } from './services/accesos.service';
import { ApiRestService } from './services/api-rest.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  notificaciones : any = [];
  
  constructor(private platform: Platform, private zone: NgZone, private router: Router, private SAcceso: AccesosService, private SApi: ApiRestService) {

    this.initializeApp();

  }

  ngOnInit(){
    LocalNotifications.checkPermissions()
    .then( (done) => {
      //alert("Done" + JSON.stringify(done));
      if( (done.display != "granted") ){
        this.requestAllowNotifications();       
      }

      
    } )
    .catch( (error) => {
      
    });
  }

  async changeStatusBar(){
    await StatusBar.setStyle( { style: Style.Dark } );
  }

  initializeApp(){

    this.platform.ready().then( () => {
      if( this.platform.is('capacitor') ){
        //StatusBar.setOverlaysWebView({ overlay: true });
        this.changeStatusBar();

        App.addListener("appUrlOpen", ( data: any ) => {
          this.zone.run( () => {
            const slug = data.url.split(".app").pop();
            if(slug){
              this.router.navigateByUrl(slug);
            }
          } );
        });
    
        setInterval( () => {
          this.updateApp();
        }, 3000 );
      }
    } );
  }

  updateApp(){

    this.SAcceso.obtenerDatos('empleadoConectado')
    .then( (done: any) => {
      if( null != done )
      {
        this.notificaciones = [];
        this.SApi.ItinerarioQueryByEmploye( parseInt( done.id ) ).subscribe( (data: any) => {
            let idusuario= parseInt(done.id);
            let idn = 1;
            data.data.forEach((element:any) => {
              let fechaE = element.fecha.split('-');
              let nuevaF = fechaE[1] + '-' + fechaE[2] + '-' + fechaE[0] + ' ' + element.hora;
              let times  = (new Date( nuevaF ).getTime()) - 300000;
              this.notificaciones.push({
                id: idn,
                title: element.evento,
                body: element.nombre+'\n'+element.descripcion,
                attachments: [element.evento_imagen],
                shedule: {
                    at:  new Date(times)
                },
                extra: {
                  usuario: idusuario,
                  evento: element.ideven
                }
              });
              idn++;
            });
          });

          LocalNotifications.schedule({
            notifications: this.notificaciones,
          });
      };
    } )
    .catch( (err) => {
      console.log( err );
    } );

  }



  requestAllowNotifications(){
    LocalNotifications
    .requestPermissions()
    .then((done: any) => {
      //alert(JSON.stringify(done));
    })
    .catch((error:any) => {
      //alert(JSON.stringify(error));
    });
  }
}
