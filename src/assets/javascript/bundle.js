window.onload = function(){
    
    ! function(t) {
        for (var e, i = [], a = 0; a < t.length; a++) e = {
            exports: {}
        }, t[a](i, e.exports, e), i.push(e.exports)
    }([function(t, e, i) {
        ! function(t) {
            var e = setTimeout;
    
            function a() {}
    
            function o(t) {
                if ("object" != typeof this) throw new TypeError("Promises must be constructed via new");
                if ("function" != typeof t) throw new TypeError("not a function");
                this.y = 0, this.p = !1, this.w = void 0, this.b = [], d(t, this)
            }
    
            function s(t, e) {
                for (; 3 === t.y;) t = t.w;
                0 !== t.y ? (t.p = !0, o.j(function() {
                    var i = 1 === t.y ? e.onFulfilled : e.onRejected;
                    if (null !== i) {
                        var a;
                        try {
                            a = i(t.w)
                        } catch (t) {
                            return void n(e.promise, t)
                        }
                        r(e.promise, a)
                    } else(1 === t.y ? r : n)(e.promise, t.w)
                })) : t.b.push(e)
            }
    
            function r(t, e) {
                try {
                    if (e === t) throw new TypeError("A promise cannot be resolved with itself.");
                    if (e && ("object" == typeof e || "function" == typeof e)) {
                        var i = e.then;
                        if (e instanceof o) return t.y = 3, t.w = e, void l(t);
                        if ("function" == typeof i) return void d((a = i, s = e, function() {
                            a.apply(s, arguments)
                        }), t)
                    }
                    t.y = 1, t.w = e, l(t)
                } catch (e) {
                    n(t, e)
                }
                var a, s
            }
    
            function n(t, e) {
                t.y = 2, t.w = e, l(t)
            }
    
            function l(t) {
                2 === t.y && 0 === t.b.length && o.j(function() {
                    t.p || o.T(t.w)
                });
                for (var e = 0, i = t.b.length; e < i; e++) s(t, t.b[e]);
                t.b = null
            }
    
            function d(t, e) {
                var i = !1;
                try {
                    t(function(t) {
                        i || (i = !0, r(e, t))
                    }, function(t) {
                        i || (i = !0, n(e, t))
                    })
                } catch (t) {
                    if (i) return;
                    i = !0, n(e, t)
                }
            }
            o.prototype.catch = function(t) {
                return this.then(null, t)
            }, o.prototype.then = function(t, e) {
                var i = new this.constructor(a);
                return s(this, new function(t, e, i) {
                    this.onFulfilled = "function" == typeof t ? t : null, this.onRejected = "function" == typeof e ? e : null, this.promise = i
                }(t, e, i)), i
            }, o.all = function(t) {
                var e = Array.prototype.slice.call(t);
                return new o(function(t, i) {
                    if (0 === e.length) return t([]);
                    var a = e.length;
    
                    function o(s, r) {
                        try {
                            if (r && ("object" == typeof r || "function" == typeof r)) {
                                var n = r.then;
                                if ("function" == typeof n) return void n.call(r, function(t) {
                                    o(s, t)
                                }, i)
                            }
                            e[s] = r, 0 == --a && t(e)
                        } catch (t) {
                            i(t)
                        }
                    }
                    for (var s = 0; s < e.length; s++) o(s, e[s])
                })
            }, o.resolve = function(t) {
                return t && "object" == typeof t && t.constructor === o ? t : new o(function(e) {
                    e(t)
                })
            }, o.reject = function(t) {
                return new o(function(e, i) {
                    i(t)
                })
            }, o.race = function(t) {
                return new o(function(e, i) {
                    for (var a = 0, o = t.length; a < o; a++) t[a].then(e, i)
                })
            }, o.j = function(t) {
                e(t, 0)
            }, o.T = function(t) {
                "undefined" != typeof console && console
            }, o.A = function(t) {
                o.j = t
            }, o.P = function(t) {
                o.T = t
            }, i.exports = o
        }()
    }, function(t, e, i) {
        function a() {
            this.k = {}
        }
        a.prototype.on = function(t, e) {
            this.k[t] || (this.k[t] = []);
            var i = this.k[t];
            return i.push(e),
                function() {
                    i.splice(i.indexOf(e), 1)
                }
        }, a.prototype.off = function(t, e) {
            var i = this.k[t];
            return i && i.splice(i.indexOf(e), 1), this
        }, a.prototype.trigger = function(t, e) {
            var i = this.k[t];
            if (i)
                for (var a = 0, o = i.length; a < o; a++) i[a](e);
            return this
        }, a.prototype.emit = a.prototype.trigger, i.exports = a
    }, function(t, e, i) {
        var a = t[0],
            o = t[1];
    
        function s() {
            o.call(this), this.l = null, this.v = !1, this.isScrolling = !1, this.animationEngine = null
        }
        for (var r in o.prototype) s.prototype[r] = o.prototype[r];
        s.prototype.attach = function(t) {
            var el = document.createElement('div');
            el.id = "famous-application";
            this.l = t, this.element = this.l.getElements()[0] || el, this.v = !this.element.classList.contains("hidden"), this.isScrolling = this.element.classList.contains("scrolling"), this.animationEngine && this.animationEngine.attach(this.element);
        }, s.prototype.detach = function() {
            this.animationEngine && this.animationEngine.detach(this.element), this.l = null, this.v = !1, this.element = null, this.isScrolling = !1
        }, s.prototype.destroy = function() {
            this.l && (this.m(), this.l.destroy())
        }, s.prototype.edit = function(t, e, i) {
            this.l && this.l.edit && this.l.edit(t, e, i)
        }, s.prototype.getComponent = function() {
            return this.l
        }, s.prototype.getProgress = function() {
            return this.animationEngine ? this.animationEngine.getProgress() : this.v ? 1 : 0
        }, s.prototype.setProgress = function(t, e) {
            return this.animationEngine ? e ? this.animationEngine.run(t, e) : (this.animationEngine.setProgress(t), a.resolve()) : a.resolve()
        }, s.prototype.setParameters = function(t) {
            return this.animationEngine && this.animationEngine.setParameters && this.animationEngine.setParameters(t), this
        }, s.prototype.setAnimationEngine = function(t) {
            return this.animationEngine && this.animationEngine.detach.apply(this.animationEngine), this.animationEngine = t, t && this.l && this.animationEngine.attach.apply(this.animationEngine, this.l.getElements()), this
        }, i.exports = s
    }, function(t, e, i) {
        function a(t) {
            this.q = t
        }
        a.prototype.on = function(t, e) {
            this.q.addEventListener(t, e)
        }, a.prototype.off = function(t, e) {
            this.q.removeEventListener(t, e)
        }, a.prototype.getElements = function() {
            return this.q ? [this.q] : []
        }, a.prototype.destroy = function() {
            this.q.parentNode.removeChild(this.q), this.q = null
        }, i.exports = a
    }, function(t, e, i) {
        function a(t, e) {
            for (var i = e, a = t.length - 1; a >= 0; a--) {
                var o = {};
                o[t[a]] = i, i = o
            }
            return i
        }
        i.exports = {
            route: function(t) {
                return function(e, i, a) {
                    return function t(e, i, a, o, s) {
                        for (;
                            "function" != typeof i && e < a.length;) {
                            if (!(i = i[a[e]])) return;
                            e++
                        }
                        if ("function" == typeof i) return i.call(this, a.slice(e), o, s);
                        if ("object" == typeof i)
                            for (var r in o) r in i && t.call(this, e, i[r], a, o[r], s)
                    }.call(this, 0, t, e, i, a)
                }
            },
            pathMin: function(t, e) {
                return function(i, a, o) {
                    return function t(e, i, a, o, s) {
                        if (e <= a.length) return i.call(this, a, o, s);
                        for (var r in o) {
                            var n = a.slice(0);
                            n.push(r), t.call(this, e, i, n, o[r], s)
                        }
                    }.call(this, t, e, i, a, o)
                }
            },
            expand: function(t) {
                return function(e, i, o) {
                    t.call(this, a(e, i), o)
                }
            },
            normalize: a,
            overlap: function(t) {
                return function(e, i, a) {
                    for (var o = 0; o < t.length; o++) t[o].call(this, e, i, a)
                }
            }
        }
    }, function(t, e, i) {
        function a(t) {
            if ("string" == typeof t) throw new Error("Type not found: " + t);
            return t
        }
        a.setTypesRegistry = function(t) {
            t
        }, a.deserialize = function(t) {
            return t ? new(a(t.type))(t) : null
        }, i.exports = a
    }, function(t, e, i) {
        var a = t[4];
    
        function o() {
            this.gestures = {
                swipeLeft: null,
                swipeRight: null,
                swipeUp: null,
                swipeDown: null,
                keyDown: null,
                keyUp: null,
                keyRight: null,
                keyLeft: null,
                wheelUp: null,
                wheelDown: null,
                tap: null,
                tapRight: null,
                tapLeft: null,
                timed: null
            }
        }
        o.prototype.edit = a.pathMin(1, function(t, e) {
            const i = t.slice(-1)[0];
            "gestures" === i && Object.keys(e).forEach(function(t) {
                this.gestures[t] = e[t]
            }.bind(this)), 2 === t.length && (this.gestures[i] = e), 3 === t.length && (this.gestures[t[1]][i] = e), "transition" === t[t.length - 2] && (this.gestures[t[1]].transition[i] = e)
        }), o.prototype.getTargetTransition = function(t) {
            const e = this.transformGesture(t);
            return this.gestures[e] && this.gestures[e].transition ? this.gestures[e].transition : null
        }, o.prototype.getTimedTransition = function() {
            return this.gestures.timed
        }, o.prototype.getTargetId = function(t) {
            const e = this.transformGesture(t);
            return this.gestures[e] && this.gestures[e].target ? this.gestures[e].target : null
        }, o.prototype.getGesture = function(t) {
            return this.gestures[t]
        }, o.prototype.getGestures = function() {
            return this.gestures
        }, o.prototype.getEdge = function(t) {
            return this.gestures[t] ? this.gestures[t].edge : null
        }, o.prototype.transformGesture = function(t) {
            switch (t) {
                case "keyLeft":
                    return "swipeRight";
                case "keyRight":
                    return "swipeLeft";
                case "keyDown":
                    return "swipeUp";
                case "keyUp":
                case "wheelUp":
                    return "swipeDown";
                case "wheelDown":
                    return "swipeUp";
                default:
                    return t
            }
        }, i.exports = o
    }, function(t, e, i) {
        function a(t, e, i) {
            this.element = t, this.configData = e, this.successCb = i, this.stripeCheckoutHandler = null, this.dt = !1, this.requireStripeAPI = this.requireStripeAPI.bind(this), this.setStripeAPIParams = this.setStripeAPIParams.bind(this), this.updateConfig = this.updateConfig.bind(this), this.attachModuleListener = this.attachModuleListener.bind(this), this.detachModuleListener = this.detachModuleListener.bind(this), this.resetModule = this.resetModule.bind(this), this.openStripeModal = this.openStripeModal.bind(this), this.handleStripeModalOpen = this.handleStripeModalOpen.bind(this), this.handleStripeModalClose = this.handleStripeModalClose.bind(this), this.forwardIframeClick = this.forwardIframeClick.bind(this), this.simulateIframeTouch = this.simulateIframeTouch.bind(this), this.toggleSimulatedTouchMove = this.toggleSimulatedTouchMove.bind(this), this.notifyTransitionProgress = this.notifyTransitionProgress.bind(this), window.activeStripeConfig = null, document.getElementById("stripe-api") ? this.attachModuleListener() : this.requireStripeAPI(() => {
                this.attachModuleListener()
            })
        }
        a.prototype.constructor = a, a.prototype.requireStripeAPI = function(t) {
            const e = document.createElement("script");
            e.id = "stripe-api", e.type = "text/javascript", e.async = !0, e.src = "https://checkout.stripe.com/checkout.js", e.onload = t, document.head.appendChild(e)
        }, a.prototype.setStripeAPIParams = function() {
            const t = document.querySelectorAll(".stripe_checkout_app");
            t.length && Array.from(t).forEach(t => document.body.removeChild(t)), this.stripeCheckoutHandler = StripeCheckout.configure({
                key: this.configData.key,
                image: this.configData.image,
                locale: "auto",
                allowRememberMe: !1,
                token: t => {
                    if (!t) return !1;
                    this.handleSuccess(t)
                }
            })
        }, a.prototype.updateConfig = function(t) {
            this.stripeCheckoutHandler && this.stripeCheckoutHandler.close();
            let e = !0;
            if (t.amount || "number" == typeof t.amount)
                if ("string" == typeof t.amount) {
                    if (!(e = function(t) {
                            return t.match(/^([1-9]{1}[0-9]{0,2})(\,\d{3})*(\.\d{2})?$|^(\$)?([1-9]{1}[0-9]{0,2})(\d{3})*(\.\d{2})?$|^(0)?(\.\d{2})?$|^(\$0)?(\.\d{2})?$|^(\$\.)(\d{2})?$/gm)
                        }(t.amount) || !isNaN(+t.amount))) return;
                    t.amount.includes(".") ? (t.amount = t.amount.split("."), t.amount[1] = t.amount[1].slice(0, 2), t.amount = 100 * parseFloat(t.amount.join("."))) : t.amount = 100 * parseInt(t.amount)
                } else "number" == typeof t.amount && (t.amount = 100 * parseInt(t.amount));
            else t.amount = 0;
            isNaN(+t.amount) || (t.amount = +t.amount, this.configData = t, t.key && t.key !== this.configData.key && (this.setStripeAPIParams(), window.activeStripeConfig = this.element.dataset.id))
        }, a.prototype.attachModuleListener = function() {
            this.element.classList.add("stripe-link"), this.element.addEventListener("click", this.handleStripeModalOpen)
        }, a.prototype.detachModuleListener = function() {
            this.stripeCheckoutHandler && window.activeStripeConfig === this.element.dataset.id && this.stripeCheckoutHandler.close(), this.element.classList.remove("stripe-link"), this.element.removeEventListener("click", this.handleStripeModalOpen);
            const t = document.querySelector(".stripe_checkout_app");
            if (t && (t.contentWindow.addEventListener("mousedown", this.simulateIframeTouch), t.contentWindow.addEventListener("mousemove", this.simulateIframeTouch), t.contentWindow.addEventListener("mouseup", this.simulateIframeTouch)), !document.getElementsByClassName("stripe-link").length) {
                const t = document.getElementById("stripe-api");
                t && document.head.removeChild(t)
            }
        }, a.prototype.resetModule = function() {
            this.stripeCheckoutHandler && window.activeStripeConfig === this.element.dataset.id && this.stripeCheckoutHandler.close()
        }, a.prototype.openStripeModal = function(t) {
            if (!this.dt) {
                this.stripeCheckoutHandler.open(this.configData), t.preventDefault();
                const e = document.querySelector(".stripe_checkout_app");
                e && (e.contentWindow.addEventListener("click", this.forwardIframeClick), e.contentWindow.addEventListener("mousedown", this.simulateIframeTouch), e.contentWindow.addEventListener("mouseup", this.simulateIframeTouch))
            }
        }, a.prototype.handleStripeModalOpen = function(t) {
            window.activeStripeConfig !== this.element.dataset.id ? (this.setStripeAPIParams(), window.activeStripeConfig = this.element.dataset.id, this.openStripeModal(t)) : this.openStripeModal(t)
        }, a.prototype.handleStripeModalClose = function() {
            this.stripeCheckoutHandler.close()
        }, a.prototype.forwardIframeClick = function(t) {
            t.target.classList.contains("trackingArea") && t.target.parentElement && t.target.parentElement.classList.contains("close") && this.stripeCheckoutHandler.close()
        }, a.prototype.simulateIframeTouch = function(t) {
            let e = document.querySelector(".stripe_checkout_app");
            if (e && e.contentWindow && (e = e.contentWindow.document, t.target.classList.contains("same-address") || t.target.classList.contains("addressSelector") || e.querySelector(".same-address").contains(t.target) || e.querySelector(".addressSelector").contains(t.target))) {
                const e = {
                        mousedown: "touchstart",
                        mousemove: "touchmove",
                        mouseup: "touchend"
                    } [t.type],
                    i = new MouseEvent(e, t);
                t.target.dispatchEvent(i), this.toggleSimulatedTouchMove(e)
            }
        }, a.prototype.toggleSimulatedTouchMove = function(t) {
            const e = document.querySelector(".stripe_checkout_app");
            e && "touchstart" === t ? e.contentWindow.addEventListener("mousemove", this.simulateIframeTouch) : e && "touchend" === t && e.contentWindow.removeEventListener("mousemove", this.simulateIframeTouch)
        }, a.prototype.notifyTransitionProgress = function(t) {
            this.dt = t % 1 != 0
        }, a.prototype.handleSuccess = function(t) {
            this.successCb()
        }, i.exports = a
    }, function(t, e, i) {
        var a = t[1],
            o = t[4],
            s = t[5],
            r = t[6],
            n = t[7];
    
        function l(t, e, i, o) {
            a.call(this), e || (e = t.$id), this.elementId = t.$id, this.onSplashScreen = !1, this.inAnimation = null, this.outAnimation = null, this.currentAnimation = null, this.O = 10, this.M = this.x.bind(this), this.D = this.I.bind(this), this.S = this.S.bind(this), this.firstBuildInTriggered = !1, this.V = null, this.n = new r, this.$ = "", this.C = !1, this.forward = o.forward || !0, this.currentProgress = o.progress || 0, this.role = o.role || "end", this.visible = o.visible || !1, this.locked = !1, this.G = 0, this.J = 0, this.N = 0, this.U = 0, this.a = null, this._ = null, this.z = null, this.B = null, this.element = document.querySelector('[data-id="' + e + '"]'), i && (this.element = i), this.u = null
        }
        l.prototype = Object.create(a.prototype), l.prototype.constructor = a.constructor, l.prototype.isStubUrl = function(t) {
            return "string" != typeof t || !t.startsWith("mailto") && !t.startsWith("tel") && (t.indexOf("//") < 0 || t.indexOf(".") < 0 || "//" === t.substr(-2) || "about:blank" === t)
        }, l.prototype.F = function() {
            var t = this.n.getGesture("tap");
            t && t.target && this.trigger("stateChange", {
                progress: 0,
                settleOnTarget: 1,
                gesture: "tap",
                edge: t.edge,
                target: t.target,
                transition: t.transition
            })
        }, l.prototype.H = function(t) {
            switch (this.V) {
                case "internal":
                    this.K();
                    break;
                case "external":
                    this.L();
                    break;
                case "checkoutModule":
                    this.Q()
            }
            this.V = t
        }, l.prototype.K = function() {
            this.element && (this.element.style.cursor = "", this.element.removeEventListener("mousedown", this.M), this.element.removeEventListener("mouseup", this.D), this.element.removeEventListener("touchstart", this.M), this.element.removeEventListener("touchend", this.D))
        }, l.prototype.L = function() {
            if (this.element && this.z) {
                if (this.z.parentNode.replaceChild(this.element, this.z), "DIV" === this.element.nodeName)
                    for (; this.z.childNodes.length > 0;) this.element.appendChild(this.z.firstChild);
                this.element.dataset.id = this.z.dataset.id, delete this.z.dataset.id, "svg" === this.element.nodeName ? this.element.removeAttribute("style") : this._ && this.element.setAttribute("style", this._), this._ = null, this.z = null
            }
        }, l.prototype.Q = function() {
            this.B && (this.B.detachModuleListener(), this.B = null, this.element.style.cursor = "")
        }, l.prototype.R = function() {
            this.element && (this.element.style.cursor = "pointer", this.element.addEventListener("mousedown", this.M), this.element.addEventListener("mouseup", this.D), this.element.addEventListener("touchstart", this.M), this.element.addEventListener("touchend", this.D))
        }, l.prototype.W = function() {
            if (this.element) {
                if (!this.z)
                    if (this.z = document.createElement("a"), this.z.dataset.id = this.element.dataset.id, delete this.element.dataset.id, this._ = this.element.getAttribute("style"), this._ && this.z.setAttribute("style", this._), this.z.style.textDecoration = "none", this.element.removeAttribute("style"), "svg" === this.element.nodeName && (this.element.style.position = "absolute", this.element.style.width = "100%", this.element.style.height = "100%"), this.element.parentNode.replaceChild(this.z, this.element), "DIV" === this.element.nodeName)
                        for (; this.element.childNodes.length > 0;) this.z.appendChild(this.element.firstChild);
                    else this.z.appendChild(this.element);
                this.z.href = this.$, this.C ? this.z.target = "_blank" : this.z.removeAttribute("target")
            }
        }, l.prototype.X = function(t) {
            this.B || "object" != typeof t || (this.B = new n(this.element, t, () => {
                this.F()
            }))
        }, l.prototype.Y = function(t) {
            switch (this.V) {
                case "internal":
                    this.Z(t.gestures);
                    var e = this.n.getGesture("tap");
                    e && e.target ? this.R() : this.K();
                    break;
                case "external":
                    if (t.link && t.link.href) {
                        this.tt(t.link);
                        const e = this.isStubUrl(this.$);
                        this.$ && !e ? this.W() : this.L()
                    }
                    break;
                case "checkoutModule":
                    this.B || this.X(t.stripeConfig), this.it(t)
            }
        }, l.prototype.Z = function(t) {
            let e = ["gestures"],
                i = t;
            if (t.tap && 1 === Object.keys(t.tap).length) {
                const a = Object.keys(t.tap)[0];
                e = ["gestures", "tap", a], i = t.tap[a]
            }
            this.n.edit(e, i)
        }, l.prototype.tt = function(t) {
            "string" == typeof t ? this.$ = t.indexOf(":") < 0 ? `https://${t}` : t : t ? (this.$ = t.href.indexOf(":") < 0 ? `https://${t.href}` : t.href, this.C = t.targetBlank) : (this.$ = "", this.C = !1)
        }, l.prototype.it = function(t) {
            t.gestures && this.Z(t.gestures), this.B && "object" == typeof t.stripeConfig && this.B.updateConfig(t.stripeConfig)
        }, l.prototype.st = function(t) {
            var e = this.z || this.element;
            if (null === t) this.inAnimation && this.inAnimation.detach(), this.inAnimation = null;
            else if (t.type) {
                var i = s(t.type);
                t.buildType = "in", t.frame = this.a, this.inAnimation instanceof i ? this.inAnimation.setOptions(t) : (this.inAnimation && this.inAnimation.detach(), this.inAnimation = s.deserialize(t), e && this.inAnimation.attach(e))
            } else this.inAnimation.setOptions(t)
        }, l.prototype.ht = function(t) {
            var e = this.z || this.element;
            if (null === t) this.outAnimation && this.outAnimation.detach(), this.outAnimation = null;
            else if (t.type) {
                var i = s(t.type);
                t.buildType = "out", t.frame = this.a, this.outAnimation instanceof i ? this.outAnimation.setOptions(t) : (this.outAnimation && this.outAnimation.detach(), this.outAnimation = s.deserialize(t), e && this.outAnimation.attach(e))
            } else this.outAnimation.setOptions(t)
        }, l.prototype.setMediaQuery = function(t) {
            this.u = t
        }, l.prototype.setDOMGenerator = function(t) {
            this.domGenerator = t
        }, l.prototype.nt = function(t) {
            return t.type, !1
        }, l.prototype.et = function(t) {
            if (this.nt(t)) {
                if (this.currentAnimation) {
                    var e = this.currentAnimation.getProgress();
                    0 === e && 1 === e || this.currentAnimation.pause()
                }
                const i = {
                        width: this.u.width,
                        height: this.u.height
                    },
                    a = (void 0)(this.domGenerator.contentToDOM(t, i));
                this.element.parentNode.replaceChild(a, this.element), this.element = a, this.currentAnimation && this.currentAnimation.isPaused() && (this.currentAnimation.attach(this.element), this.currentAnimation.resume())
            }
        }, l.prototype.edit = o.expand(function(t) {
            if (this.et(t), t.frame) {
                this.a = t.frame;
                var e = 1 === Object.keys(t).length;
                this.inAnimation && (this.inAnimation.setOptions({
                    frame: this.a
                }), e && this.inAnimation.detach()), this.outAnimation && (this.outAnimation.setOptions({
                    frame: this.a
                }), e && this.outAnimation.detach())
            }
            void 0 !== t.inAnimation && this.st(t.inAnimation), void 0 !== t.outAnimation && this.ht(t.outAnimation);
            const i = void 0 === t.isLink && "internal" === this.V && t.gestures;
            void 0 !== t.isLink ? (t.isLink !== this.V && this.H(t.isLink, t), this.Y(t)) : i ? this.Y(t) : t.gestures && t.gestures.syntheticGesture && this.Z(t.gestures)
        }), l.prototype.detach = function() {
            this.currentAnimation && this.currentAnimation.detach(), this.currentAnimation = null
        }, l.prototype.replaceElement = function(t) {
            if (this.element) return this.ut = document.createRange(), this.ut.selectNode(this.element), this.ot = this.ut.extractContents(), this.ut.insertNode(t), this.element = t, this.currentAnimation && (this.currentAnimation.detach(), this.currentAnimation.attach(t)), this.element
        }, l.prototype.svgToNewDomWrapper = function() {
            if (!this.element || !this.element.parentNode) return;
            this._ = this.element.getAttribute("style");
            const t = this.element.getAttribute("height"),
                e = this.element.getAttribute("width"),
                i = document.createElement("div");
            i.setAttribute("style", this._), i.dataset.id = this.element.dataset.id, i.style.height = t, i.style.width = e, this.element.parentNode.replaceChild(i, this.element), this.element = i
        }, l.prototype.x = function(t) {
            0 !== this.currentProgress && 1 !== this.currentProgress || (this.G = t.touches ? t.touches[0].screenX : t.screenX, this.J = t.touches ? t.touches[0].screenY : t.screenY)
        }, l.prototype.I = function(t) {
            if (0 === this.currentProgress || 1 === this.currentProgress) {
                this.N = t.changedTouches ? t.changedTouches[0].screenX : t.screenX, this.U = t.changedTouches ? t.changedTouches[0].screenY : t.screenY;
                var e = Math.abs(this.G - this.N),
                    i = Math.abs(this.J - this.U);
                e < this.O && i < this.O && this.n.getGesture("tap") && this.F()
            }
        }, l.prototype.setProgress = function(t) {
            this.currentAnimation.setProgress(t, !1)
        }, l.prototype.start = function() {
            this.onSplashScreen && !this.firstBuildInTriggered && this.inAnimation && this.S()
        }, l.prototype.S = function() {
            this.currentAnimation = this.inAnimation, this.currentAnimation.setProgress(0), this.firstBuildInTriggered = !0, this.currentAnimation.runToEnd(1)
        }, l.prototype.setInitialState = function(t) {
            this.updateState(t, !1), this.locked = !1;
            var e = this.z || this.element,
                i = null;
            if ("start" === this.role && this.forward ? i = this.outAnimation : "start" !== this.role || this.forward ? "end" === this.role && this.forward ? i = this.inAnimation : "end" !== this.role || this.forward || (i = this.outAnimation) : i = this.inAnimation, this.shouldAnimate = !!i, 0 === this.currentProgress && this.currentAnimation && this.shouldAnimate && (this.currentAnimation.setProgress(0), this.currentAnimation.detach(), this.currentAnimation = null), i && this.currentAnimation !== i && (!this.currentAnimation || this.currentAnimation && !this.currentAnimation.isActive()) && (this.currentAnimation && this.currentAnimation.detach(), this.currentAnimation = i, this.currentAnimation.attach(e), "time" === this.currentAnimation.animationBasis)) {
                var a = "start" === this.role ? 1 : 0;
                this.currentAnimation.setProgress(a)
            }
            this.shouldAnimate || i || !this.currentAnimation || 0 !== this.currentProgress || (this.currentAnimation.detach(), this.currentAnimation = null, this.element && this.visible && this.element.classList.remove(this.lt))
        }, l.prototype.updateState = function(t, e) {
            this.B && this.B.notifyTransitionProgress(t.progress), this.currentProgress = t.progress, this.forward = t.forward, this.role = t.role, this.visible = t.visible, e && this.updateAnimation()
        }, l.prototype.updateAnimation = function() {
            if (!this.locked && this.currentAnimation && this.shouldAnimate)
                if ("position" === this.currentAnimation.animationBasis) this.setProgress(this.currentProgress);
                else if ("time" === this.currentAnimation.animationBasis) {
                if (!this.currentAnimation.duration) return void this.currentAnimation.setProgress(1);
                if ("start" === this.role && this.currentAnimation.isActive() && !this.forward) {
                    this.locked = !0;
                    var t = this.currentAnimation.getProgress();
                    this.currentAnimation.ct = this.currentAnimation.duration * (1 - t), this.currentAnimation.setProgress(t), this.currentAnimation.rt = !1, this.currentAnimation.runToEnd(1), this.currentAnimation.ct = this.currentAnimation.duration
                } else "end" === this.role && this.currentProgress >= this.currentAnimation.delay && !this.currentAnimation.isActive() ? this.currentAnimation.runToEnd(1) : "start" === this.role && 1 - this.currentProgress >= this.currentAnimation.delay && !this.currentAnimation.isActive() && this.currentAnimation.runToEnd(0)
            }
        }, i.exports = l
    }, function(t, e, i) {
        var a = t[1];
    
        function o(t, e) {
            this.q = t, this.ii = this.ti.bind(this), this.s = this.h.bind(this), window.addEventListener("resize", this.ii), this.ii(), this.enableScroll(), a.call(this)
        }
        for (var s in a.prototype) o.prototype[s] = a.prototype[s];
        o.prototype.h = function(t) {
            0 === this.q.scrollTop || this.si - this.q.scrollTop === this.hi ? setTimeout(function() {
                this.trigger("scrolling", !1)
            }.bind(this), 2e3) : this.trigger("scrolling", !0)
        }, o.prototype.ti = function() {
            this.si = this.q.scrollHeight, this.hi = this.q.clientHeight
        }, o.prototype.enableScroll = function() {
            this.q.addEventListener("scroll", this.s)
        }, o.prototype.disableScroll = function() {
            this.q.removeEventListener("scroll", this.s)
        }, i.exports = o
    }, function(t, e, i) {
        var a = t[1];
    
        function o() {
            a.call(this), this.ni = [], this.ct = 0, this.ei = 0, this.oi = 0, this.ri = 0, this.ui = this.ai.bind(this), this.fi = !1
        }
        for (var s in a.prototype) o.prototype[s] = a.prototype[s];
        o.prototype.setSequence = function(t) {
            this.ni = t, this.ni.forEach(function(t) {
                t.time > this.ct && (this.ct = t.time)
            }.bind(this))
        }, o.prototype.play = function() {
            this.fi = !1, this.ei = Date.now(), this.oi = this.ei + this.ct, requestAnimationFrame(this.ui)
        }, o.prototype.skip = function() {
            for (this.fi = !0; this.ri < this.ni.length;) this.ci(this.ni[this.ri])
        }, o.prototype.ci = function(t) {
            t.action(), this.ri++
        }, o.prototype.stop = function() {
            this.fi = !0, cancelAnimationFrame(this.ui)
        }, o.prototype.clear = function() {
            this.ct = 0, this.ei = 0, this.oi = 0, this.ri = 0, this.ni = [], this.fi = !1
        }, o.prototype.ai = function() {
            if (!this.fi) {
                var t = Date.now();
                t >= this.ei + this.ni[this.ri].time && this.ci(this.ni[this.ri]), t > this.oi ? this.trigger("ended") : requestAnimationFrame(this.ui)
            }
        }, i.exports = o
    }, function(t, e, i) {
        var a = t[2],
            o = t[3],
            s = t[4],
            r = t[5],
            n = t[8],
            l = t[6],
            d = t[9],
            c = t[10];
    
        function h() {
            a.call(this), this.t = {}, this.currentProgress = 0, this.previousProgress = 0, this.forward = !0, this.role = "start", this.visible = !1, this.splash = !1, this.firstTimedTriggered = !1, this.boundHandleProgress = this.handleProgress.bind(this), this.i = null, this.s = this.h.bind(this), this.n = new l, this.r = !1, this.e = !1, this.o = new c, this.start = this.start.bind(this), this.a = null, this.u = null, this.c = null
        }
        h.prototype = Object.create(a.prototype), h.prototype.f = function(t, e, i, a, o) {
            for (var s in t) {
                var l = t[s],
                    d = n;
                try {
                    d = r(l.type)
                } catch (t) {}
                var c = e + s;
                this.t[c] || (this.t[c] = {}), this.t[c].id || (this.t[c].id = l.$id);
                var h = !1;
                if (this.t[c].instance && (this.t[c].instance.edit ? d && this.t[c].type !== d && (h = !0) : (h = !0, l.type || (d = this.t[c].type))), !this.t[c].instance || !this.t[c].instance.edit || h) {
                    if (h && this.t[c].instance.detach && this.t[c].instance.detach(), "function" == typeof d) {
                        var p = this.getContentElement(c),
                            u = {
                                visible: this.visible,
                                progress: this.currentProgress,
                                role: this.role,
                                forward: this.forward
                            };
                        this.t[c].instance = new d(l, this.t[c].id, p, u), this.t[c].instance && this.t[c].instance.on && (this.t[c].instance.on("stateChange", function(t) {
                            this.trigger("stateChange", t)
                        }.bind(this)), this.t[c].instance.on("sendAnalytics", function(t) {
                            this.trigger("sendAnalytics", t)
                        }.bind(this)))
                    }
                    this.t[c].type = d
                }
                var f = a || o;
                this.t[c].instance.edit && this.t[c].instance.edit([], l, f), this.t[c].instance instanceof n && l && l.content && this.f(l.content, e + s + "/", i, f)
            }
        }, h.prototype.getContentElement = function(t) {
            var e = this.l.getElements()[0],
                i = this.t[t],
                a = e.querySelectorAll('[data-id="' + i.id + '"]');
            if (a.length) {
                if (1 === a.length) return a[0];
                if (a.length > 1) {
                    for (var o = t.split("/"), s = e.childNodes[0].childNodes, r = 0; r < o.length; r++) {
                        var n = s[parseInt(o[r])];
                        if (!n.childNodes) break;
                        s = n.childNodes
                    }
                    return n || a[0]
                }
                return a[0]
            }
            return null
        }, h.prototype.setSplash = function(t) {
            for (var e in this.splash = t, this.t) {
                var i = this.t[e].instance;
                i instanceof n && (i.onSplashScreen = t)
            }
        }, h.prototype.setMediaQuery = function(t) {
            for (var e in this.u = t, this.t) {
                var i = this.t[e].instance;
                i instanceof n && i.setMediaQuery(t)
            }
        }, h.prototype.setDOMGenerator = function(t) {
            for (var e in this.domGenerator = t, this.t) {
                var i = this.t[e].instance;
                i instanceof n && i.setDOMGenerator(t)
            }
        }, h.prototype.start = function() {
            for (const t in this.t) {
                const e = this.t[t];
                e && e.instance && e.instance.start()
            }
        }, h.prototype.edit = s.pathMin(1, function(t, e, i) {
            var a = i || "content" === t[0];
            if ("frame" === t[0] && (this.a = e), "$id" === t[0]) return this.getComponent() && this.detach(this.getComponent()), this.attach(new o(document.querySelector('[data-id="' + e + '"]')));
            "scrollReset" === t[0] && (this.e = e);
            var r = "gestures" === t[0];
            if (r && this.n.edit(t, e), !r && a) {
                var n = s.normalize(i ? t.slice(2) : t.slice(1), e);
                return this.f(n, "", t, this.v, this.currentProgress)
            }
            "scrolling" === t[0] && this.element && this.a && this.setScrolling(e)
        }), h.prototype.setScrolling = function(t) {
            if (!this.a.width && this.a.height) return;
            const e = this.element.classList.contains("scrolling");
            if (!0 !== t || e) !1 === t && e && (this.element.classList.remove("scrolling"), this.element.querySelector(".wrapper").style["min-height"] = "100%");
            else {
                this.element.classList.add("scrolling");
                const t = Math.round(this.a.height / this.a.width * 100);
                this.element.querySelector(".wrapper").style["min-height"] = `${t}vw`
            }
        }, h.prototype.scrollCheck = function() {
            var t = this.l.getElements()[0];
            return this.i && (this.i.disableScroll(), this.i.off("scrolling", this.s), this.i = null), "scroll" === window.getComputedStyle(t).overflowY && (this.i = new d(t), this.i.on("scrolling", this.s), !0)
        }, h.prototype.h = function(t) {
            this.trigger("scrolling", t), this.n.getTimedTransition() && this.n.getTimedTransition().resetOnTouch && this.checkTimed()
        }, h.prototype.checkTimed = function() {
            this.clearTimed();
            var t = this.n.getTimedTransition();
            t && this.d(t)
        }, h.prototype.d = function(t) {
            const e = [{
                time: t.delay,
                action: function() {
                    this.trigger("timed", {
                        transition: t.transition,
                        target: t.target,
                        progress: 0,
                        settleOnTarget: 1,
                        gesture: "timed",
                        edge: t.edge
                    })
                }.bind(this)
            }];
            this.o.setSequence(e), this.o.play()
        }, h.prototype.clearTimed = function() {
            this.o.stop(), this.o.clear()
        }, h.prototype.setProgress = function(t, e) {
            return a.prototype.setProgress.call(this, t, e)
        }, h.prototype.handleProgress = function(t) {
            this.setContentState(t, !1), this.isScrolling && (!this.r && t > 0 && t < 1 && (this.element.style.overflowY = "hidden", this.r = !0), (this.r && 1 === t || 0 === t) && (this.element.style.overflowY = "", this.r = !1)), this.animationEngine && (this.visible = this.animationEngine.g)
        }, h.prototype.setContentState = function(t) {
            for (var e in this.previousProgress = this.currentProgress, this.currentProgress = t, "start" === this.role && this.currentProgress < this.previousProgress ? this.forward = !0 : "start" === this.role && this.currentProgress > this.previousProgress ? this.forward = !1 : "end" === this.role && this.currentProgress > this.previousProgress ? this.forward = !0 : "end" === this.role && this.currentProgress < this.previousProgress && (this.forward = !1), this.t) {
                var i = this.t[e].instance;
                i instanceof n && i.updateState({
                    progress: this.currentProgress,
                    forward: this.forward,
                    role: this.role,
                    visible: this.visible
                }, !0)
            }
        }, h.prototype.setTransition = function(t) {
            for (var e in this.clearTimed(), this.forward = !(t.appDirection < 0), this.role = t.role || "start", this.animationEngine && this.animationEngine.off("progress", this.boundHandleProgress), a.prototype.setAnimationEngine.call(this, t.engine), this.animationEngine && (this.animationEngine.on("progress", this.boundHandleProgress), this.visible = this.animationEngine.g), this.t) {
                var i = this.t[e].instance;
                i instanceof n && i.setInitialState({
                    progress: this.currentProgress,
                    forward: this.forward,
                    role: this.role,
                    visible: this.visible
                }, !1)
            }
            a.prototype.setParameters.call(this, {
                role: t.role,
                appDirection: "start" === t.role ? -t.appDirection : t.appDirection,
                orientation: t.orientation,
                elementIds: t.elementIds || {}
            })
        }, h.prototype.getTargetTransition = function(t) {
            return this.n.getTargetTransition(t)
        }, h.prototype.getTargetId = function(t) {
            return this.n.getTargetId(t)
        }, h.prototype.getTimedTransition = function() {
            return this.n.getTimedTransition()
        }, h.prototype.getGestureEdge = function(t) {
            return this.n.getEdge(t)
        }, h.prototype.resetAnimations = function() {
            this.animationEngine && this.animationEngine.detach(), this.element && "start" === this.role && this.e && this.element.classList.contains("hidden") && (this.element.classList.remove("hidden"), this.element.scrollTop = 0, this.element.classList.add("hidden"))
        }, i.exports = function(t) {
            var e = new h;
            return e.edit([], t), e
        }
    }, function(t, e, i) {
        var a = t[8],
            o = t[4];
    
        function s() {
            a.apply(this, arguments)
        }
        s.prototype = Object.create(a.prototype), s.prototype.constructor = s, s.prototype.ft = function(t) {
            if (this.element) {
                var e = this.element.getElementsByTagName("image")[0];
                e && t.src && e.setAttributeNS("http://www.w3.org/1999/xlink", "href", t.src)
            }
        }, s.prototype.edit = o.overlap([a.prototype.edit, o.expand(function(t) {
            t.src && this.ft(t)
        })]), i.exports = s
    }, function(t, e, i) {
        function a(t) {
            this.at = [], this.vt = null, this.y = null, this.Dt = 0, this.yt = 0, null != t && this.from(t)
        }
    
        function o(t) {
            return t
        }
        a.prototype.to = function(t, e, i, a) {
            return 0 === this.at.length && (this.Dt = Date.now(), this.yt = 0), this.at.push([t, null !== e ? e : o, null !== i ? i : 100, a]), this
        }, a.prototype.from = function(t) {
            for (this.y = this.bt(null, t), this.vt = t; this.at.length;) {
                var e = this.at.shift()[3];
                e && e(!0)
            }
            return this.Dt = Date.now(), this.yt = 0, this
        }, a.prototype.mt = function(t, e, i, a) {
            if (i instanceof Object)
                if (i instanceof Array)
                    for (var o = 0, s = i.length; o < s; o++) t[o] = this.mt(t[o], e[o], i[o], a);
                else
                    for (var r in i) t[r] = this.mt(t[r], e[r], i[r], a);
            else t = e + a * (i - e);
            return t
        }, a.prototype.bt = function t(e, i) {
            if ("number" == typeof i) return i;
            for (var a in e || (e = Array.isArray(i) ? [] : {}), i) e[a] = t(e[a], i[a]);
            return e
        }, a.prototype.get = function(t) {
            if (!this.at.length) return this.y;
            t || (t = this.yt || Date.now());
            var e = this.at[0],
                i = (t - this.Dt) / e[2];
            if (i >= 1) {
                this.y = this.bt(this.y, e[0]), this.Dt = this.Dt + e[2], this.vt = e[0];
                var a = e[3];
                a && a(!1), this.at.shift()
            } else this.y = this.mt(this.y, this.vt, e[0], e[1](i));
            return this.y
        }, a.prototype.isActive = function() {
            return this.at.length > 0
        }, a.prototype.halt = function() {
            return this.from(this.get())
        }, a.prototype.pause = function() {
            return this.yt = Date.now(), this
        }, a.prototype.isPaused = function() {
            return !!this.yt
        }, a.prototype.resume = function() {
            var t = this.yt - this.Dt;
            return this.Dt = Date.now() - t, this.yt = null, this
        }, i.exports = a
    }, function(t, e, i) {
        var a = t[0],
            o = t[13],
            s = t[1],
            r = t[5];
    
        function n(t) {
            s.call(this), this.element = null, this.transitionable = new o(0), this.g = !0, this.lt = "hidden", this.pi = null, this.ct = 350, this.yi = 0, this.Ai = null, this.rt = !1, this.Ei = this.Fi.bind(this), this.Oi = 0, this.appDirection = 1, this.orientation = 0, this.role = "start", this.elementIds = {}, t && this.setOptions(t)
        }
        for (var l in s.prototype) n.prototype[l] = s.prototype[l];
        n.prototype.setParameters = function(t) {
            void 0 !== t.appDirection && (this.appDirection = t.appDirection), void 0 !== t.orientation && (this.orientation = t.orientation), void 0 !== t.role && (this.role = t.role), void 0 !== t.elementIds && (this.elementIds = t.elementIds)
        }, n.prototype.setOptions = function(t) {
            void 0 !== t.curve && (this.pi = r(t.curve)), void 0 !== t.duration && (this.ct = t.duration), void 0 !== t.margin && (this.yi = t.margin)
        }, n.prototype.attach = function(t) {
            this.element = t, this.g = !t.classList.contains(this.lt)
        }, n.prototype.detach = function() {
            this.element && (this.element.style = ""), this.element = null
        }, n.prototype.show = function() {
            !this.g && this.element && this.element.classList.remove(this.lt), this.g = !0
        }, n.prototype.hide = function() {
            this.g && this.element && this.element.classList.add(this.lt), this.g = !1
        }, n.prototype.isActive = function() {
            return this.transitionable.isActive()
        }, n.prototype.getProgress = function() {
            return this.transitionable.get()
        }, n.prototype.setProgress = function(t, e) {
            return new a(function(i) {
                if (e) {
                    var a = this.pi,
                        o = this.ct;
                    "object" == typeof e && (e.curve && (a = e.curve), e.duration && (o = e.duration)), this.transitionable.to(t, a, o, i)
                } else this.transitionable.from(t), i();
                this.step()
            }.bind(this))
        }, n.prototype.run = function(t, e) {
            return this.rt ? this.Ai : (this.rt = !0, requestAnimationFrame(this.Ei), this.Ai = void 0 !== t ? this.setProgress(t, e) : a.resolve(), this.Ai)
        }, n.prototype.Fi = function() {
            this.isActive() ? requestAnimationFrame(this.Ei) : this.rt = !1, this.step()
        }, n.prototype.step = function() {
            this.Oi = this.getProgress(), this.Oi && !this.g ? this.show() : !this.Oi && this.g && this.hide(), this.trigger("progress", this.Oi)
        }, i.exports = n
    }, function(t, e, i) {
        i.exports = {
            filter: void 0 !== document.body.style.filter ? "filter" : "webkit-filter",
            transform: void 0 !== document.body.style.transform ? "transform" : "webkit-transform",
            transformOrigin: void 0 !== document.body.style.transformOrigin ? "transform-origin" : "webkit-transform-origin",
            transition: void 0 !== document.body.style.transition ? "transition" : "webkit-transition",
            documentHidden: void 0 !== document.hidden ? "hidden" : "webkitHidden",
            documentVisibilityChange: void 0 !== document.hidden ? "visibilitychange" : "webkitvisibilitychange"
        }
    }, function(t, e, i) {
        var a = t[14],
            o = t[15];
    
        function s(t) {
            this.initialProperties = {
                transform: ""
            }, this.buildType = "in", this.animationBasis = "position", this.duration = 300, this.delay = .5, this.frame = {}, this.wi = null, this.mi = null, this.qi = null, a.call(this, t)
        }
        s.prototype = Object.create(a.prototype), s.prototype.constructor = s, s.prototype.setOptions = function(t) {
            a.prototype.setOptions.call(this, t), void 0 !== t.buildType && (this.buildType = t.buildType), void 0 !== t.frame && (this.frame = t.frame), void 0 !== t.animationBasis && (this.animationBasis = t.animationBasis), void 0 !== t.duration && (this.duration = t.duration), void 0 !== t.delay && (this.delay = t.delay), void 0 !== t.blurRadius && (this.mi = t.blurRadius), void 0 !== t.opacity && (this.qi = t.opacity), void 0 !== t.scale && (this.wi = 0 === t.scale ? .01 : t.scale), this.element && (this.element.style.opacity = "", this.element.style[o.filter] = "", this.element.style[o.transform] = "")
        }, s.prototype.attach = function(t) {
            this.element = t, this.g = !t.classList.contains(this.lt), this.triggered = !1, this.endValue = null, this.initialProperties && (this.element.style[o.transform] = this.initialProperties.transform)
        }, s.prototype.detach = function() {
            this.element && (this.element.style[o.filter] = "", this.element.style[o.transform] = this.initialProperties.transform, this.element.style.opacity = "", this.g || this.show()), this.endValue = null, this.element = null
        }, s.prototype.runToEnd = function(t) {
            this.triggered = !0, this.endValue = t, this.run(t, !0)
        }, s.prototype.pause = function() {
            this.transitionable.pause()
        }, s.prototype.resume = function() {
            this.transitionable.resume()
        }, s.prototype.isPaused = function() {
            return this.transitionable.isPaused()
        }, i.exports = s
    }, function(t, e, i) {
        i.exports = {
            formatTransform: function(t, e, i) {
                const a = [];
                if (t.rotateAngle) {
                    const e = t.rotateAxis || [0, 0, 1];
                    a.unshift(`rotate3d(${e.join(",")},${t.rotateAngle}deg)`)
                }
                const o = t.scaleX || 1,
                    s = t.scaleY || 1;
                var r = e ? o * e : o,
                    n = e ? s * e : s;
                return a.unshift(`scale3d(${r},${n},1)`), i ? a.unshift(i) : a.unshift("translate3d(0, 0, 0)"), a.join(" ")
            },
            isContentVisible: function(t) {
                return !(t && t.style && "visibility" in t.style && "inherit" !== t.style.visibility)
            },
            getCurrentFrame: function(t) {
                return t
            }
        }
    }, function(t, e, i) {
        var a = t[16],
            o = t[15],
            s = t[17].formatTransform;
    
        function r(t) {
            this.li = r.Edge.TOP, this.vi = 0, this.di = 100, a.call(this, t)
        }
        r.prototype = Object.create(a.prototype), r.prototype.constructor = r, r.Edge = {
            TOP: "top",
            RIGHT: "right",
            BOTTOM: "bottom",
            LEFT: "left"
        }, r.prototype.setOptions = function(t) {
            a.prototype.setOptions.call(this, t), void 0 !== t.edge && (this.li = t.edge, this.vi = this.li === r.Edge.TOP || this.li === r.Edge.BOTTOM ? 1 : 0, this.di = this.li === r.Edge.RIGHT || this.li === r.Edge.BOTTOM ? 100 : -100, "out" === this.buildType && (this.di = -1 * this.di)), void 0 !== t.translateY ? this.Ti = t.translateY : this.Ti = null, void 0 !== t.translateX ? this.bi = t.translateX : this.bi = null, this.element && (this.element.style.scale = "")
        }, r.prototype.step = function() {
            if (this.element) {
                if (a.prototype.step.call(this), this.mi) {
                    var t = "blur(" + this.mi * (1 - this.Oi).toFixed(2) + "px)";
                    this.element.style[o.filter] = t
                }
                null !== this.qi && (this.element.style.opacity = this.Oi * (1 - this.qi) + this.qi);
                var e = this.Ti || 0,
                    i = this.bi || 0; - 100 === this.di && (i = -i, e = -e);
                var r, n, l = this.di * (1 - this.Oi);
                0 === this.vi ? (r = 1 + i, n = 0 - e) : (n = 1 - e, r = 0 + i);
                var d = this.wi ? this.wi + this.Oi * (1 - this.wi) : 1,
                    c = "translate3d(" + r * l + "vw," + n * l + "vw,0) ",
                    h = s(this.frame, d, c);
                this.element.style[o.transform] = h
            }
        }, i.exports = r
    }, function(t, e, i) {
        i.exports = function(t) {
            return t * (2 - t)
        }
    }, function(t, e, i) {
        var a = t[8];
    
        function o() {
            a.apply(this, arguments)
        }
        o.prototype = Object.create(a.prototype), o.prototype.constructor = o, i.exports = o
    }, function(t, e, i) {
        var a = t[8];
    
        function o() {
            a.apply(this, arguments)
        }
        o.prototype = Object.create(a.prototype), o.prototype.constructor = o, i.exports = o
    }, function(t, e, i) {
        var a = t[14],
            o = t[15];
    
        function s(t) {
            a.call(this, t), this.yi = t.margin || 0, this.li = t.edge || 0, this.wt = null, this.appDirection = 1, this.orientation = t.orientation || s.ORIENTATION_X
        }
        s.prototype = Object.create(a.prototype), s.ORIENTATION_X = 0, s.ORIENTATION_Y = 1, s.prototype.findFirstLargeChild = function(t, e, i) {
            var a = t.children;
            if (!a || 0 === a.length) return null;
            for (var o = [], s = 0; s < a.length; s++) {
                var r = a[s];
                r.clientWidth >= e && r.clientHeight >= i && o.push(r)
            }
            return o.length > 0 ? o[0] : null
        }, s.prototype.attach = function(t) {
            a.prototype.attach.call(this, t);
            var e = t.querySelector(".wrapper");
            if (e) {
                var i = t.offsetWidth,
                    o = t.offsetHeight;
                this.wt = this.findFirstLargeChild(e, i, o)
            }
            this.wt && (this.wt.style["transform-style"] = "preserve-3d", this.wt.style["z-index"] = "-1")
        }, s.prototype.detach = function() {
            this.wt && (this.wt.style = "", this.wt = null), a.prototype.detach.call(this)
        }, s.prototype.step = function() {
            if (a.prototype.step.call(this), this.element) {
                var t = 100 * (this.li || this.appDirection) * (1 - this.Oi);
                t += Math.min(Math.max(-this.yi, t), this.yi), Math.abs(t) < 1e-5 && (t = 0);
                var e, i = null;
                if (e = this.orientation === s.ORIENTATION_X ? "translate3d(" + t + "vw,0,0)" : this.orientation === s.ORIENTATION_Y ? "translate3d(0," + t + "%,0)" : "translate3d(" + t + "vw,0,0)", this.element.style[o.transform] = e, this.wt) {
                    var r = t / 2;
                    i = this.orientation === s.ORIENTATION_X ? "translate3d(" + -r + "vw,0,0)" : this.orientation === s.ORIENTATION_Y ? "translate3d(0," + -r + "vh,0)" : "translate3d(" + -r + "vw,0,0)", this.wt.style[o.transform] = i, "start" === this.role && (this.element.style.opacity = this.Oi)
                }
            }
        }, i.exports = s
    }, function(t, e, i) {
        var a = t[14],
            o = t[15];
    
        function s(t) {
            a.call(this, t), this.xi = !1, this.gi = 0, this.xOrigin = window.innerWidth / 2, this.yOrigin = window.innerHeight / 2, this.boundHandleResize = function() {
                this.xOrigin = window.innerWidth / 2, this.yOrigin = window.innerHeight / 2
            }.bind(this), t && this.setOptions(t), this.appDirection = 1, this.orientation = t.orientation || s.ORIENTATION_X
        }
        s.prototype = Object.create(a.prototype), s.prototype.attach = function(t) {
            this.xOrigin = window.innerWidth / 2, this.yOrigin = window.innerHeight / 2, window.addEventListener("resize", this.boundHandleResize), a.prototype.attach.call(this, t)
        }, s.ORIENTATION_X = 0, s.ORIENTATION_Y = 1, s.prototype.detach = function() {
            window.removeEventListener("resize", this.boundHandleResize), a.prototype.detach.call(this)
        }, s.prototype.setOptions = function(t) {
            a.prototype.setOptions.call(this, t), void 0 !== t.cutoff && (this.gi = Math.min(t.cutoff || 0, .999)), void 0 !== t.edge && (this.li = t.edge || 0), void 0 !== t.opacitate && (this.Mi = t.opacitate)
        }, s.prototype.step = function() {
            if (a.prototype.step.call(this), this.element) {
                var t = 90 * this.appDirection * (this.Oi - 1);
                Math.abs(t) < 1e-5 && (t = 0);
                var e, i = 1 - Math.abs(t / 90);
                e = this.orientation === s.ORIENTATION_X ? "translate3d(0,0," + -this.xOrigin + "px) rotate3d(0,1,0," + -t + "deg) translate3d(0,0," + this.xOrigin + "px)" : "translate3d(0,0," + -this.yOrigin + "px) rotate3d(1,0,0," + t + "deg) translate3d(0,0," + this.yOrigin + "px)", this.element.style[o.transform] = e, this.element.style.opacity = i
            }
        }, i.exports = s
    }, function(t, e, i) {
        i.exports = function(t) {
            return --t * t * t * t * t + 1
        }
    }, function(t, e, i) {
        var a = t[16],
            o = t[15],
            s = t[17].formatTransform;
    
        function r(t) {
            this.wi = 2, a.call(this, t)
        }
        r.prototype = Object.create(a.prototype), r.prototype.constructor = r, r.prototype.step = function() {
            if (this.element) {
                if (a.prototype.step.call(this), Math.abs(this.Oi) < 1e-5 && (this.Oi = 0), this.mi) {
                    var t = "blur(" + this.mi * (1 - this.Oi).toFixed(2) + "px)";
                    this.element.style[o.filter] = t
                }
                null !== this.qi && (this.element.style.opacity = this.Oi * (1 - this.qi) + this.qi);
                var e = 1 + (1 - this.Oi) * (this.wi - 1),
                    i = s(this.frame, e);
                this.element.style[o.transform] = i
            }
        }, i.exports = r
    }, function(t, e, i) {
        var a = t[16],
            o = t[15],
            s = t[17].formatTransform;
    
        function r(t) {
            this.wi = .5, a.call(this, t)
        }
        r.prototype = Object.create(a.prototype), r.prototype.constructor = r, r.prototype.step = function() {
            if (this.element) {
                if (a.prototype.step.call(this), this.mi) {
                    var t = "blur(" + this.mi * (1 - this.Oi).toFixed(2) + "px)";
                    this.element.style[o.filter] = t
                }
                null !== this.qi && (this.element.style.opacity = this.Oi * (1 - this.qi) + this.qi);
                var e = this.wi + this.Oi * (1 - this.wi),
                    i = s(this.frame, e);
                this.element.style[o.transform] = i
            }
        }, i.exports = r
    }, function(t, e, i) {
        var a = t[8];
    
        function o() {
            a.apply(this, arguments)
        }
        o.prototype = Object.create(a.prototype), o.prototype.constructor = o, i.exports = o
    }, function(t, e, i) {
        i.exports = {
            type: "deck",
            cards: [{
                $id: "0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55",
                name: "Start",
                type: t[11],
                frame: {
                    x: -13,
                    y: -13,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 667,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    }
                },
                maskId: null,
                content: [{
                    $id: "55020CDE-798A-4F23-BAD8-4356C48CAEA8",
                    src: "../../assets/images/mom_bg.jpg",
                    name: "cover",
                    type: t[12],
                    frame: {
                        top: 0,
                        left: 0,
                        right: 0,
                        width: 375,
                        bottom: 0,
                        height: 667,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "px",
                                right: "px"
                            },
                            y: {
                                pos: "center",
                                top: "px",
                                bottom: "px"
                            },
                            scaling: "both"
                        },
                        rotateAngle: 0
                    },
                    subId: "aa8f1c7b",
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "2c04ebba-9dbd-4139-93d5-d23cb76c8745",
                    src: "../../assets/images/f00d3dbd5eeb81e5242694c31d64f0c1.gif",
                    name: "swipe left hint",
                    type: t[12],
                    frame: {
                        x: 59.33333333333337,
                        y: 549.3333333333327,
                        top: 572,
                        left: 110,
                        right: 110,
                        width: 154,
                        alignX: "left",
                        alignY: "top",
                        bottom: 55,
                        height: 38,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 296,
                        originalHeight: 72,
                        constrainProportions: !0
                    },
                    imageFit: "cover",
                    outAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: 0,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "618E76A3-CBDF-43FA-B064-D8D074D4BDF1",
                    name: "words to live by…",
                    type: t[20],
                    frame: {
                        top: 467,
                        left: 87,
                        right: 91,
                        width: 196,
                        bottom: 180,
                        height: 19,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    },
                    maskId: null,
                    inAnimation: {
                        edge: "bottom",
                        type: t[18],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        duration: 2e3,
                        undefined: 0,
                        blurRadius: 0,
                        translateX: 0,
                        translateY: .95,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: 0,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    hasClippingMask: !1
                }, {
                    $id: "a46d7a25-f6e1-4efc-99b0-2af268166478",
                    name: "words to live by… copy",
                    type: t[20],
                    frame: {
                        top: 621,
                        left: 87,
                        right: 92,
                        width: 196,
                        bottom: 26,
                        height: 17,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    },
                    maskId: null,
                    inAnimation: {
                        edge: "bottom",
                        type: t[18],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        duration: 2e3,
                        undefined: 0,
                        blurRadius: 0,
                        translateX: 0,
                        translateY: .95,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: 0,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    hasClippingMask: !1
                }, {
                    $id: "4E105924-DF73-490D-9CF8-84A2E4C59216",
                    name: "logo",
                    type: t[21],
                    frame: {
                        x: 64,
                        y: 80,
                        top: 80,
                        left: 64,
                        right: 64,
                        width: 247,
                        bottom: 363,
                        height: 224,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    },
                    maskId: null,
                    content: [{
                        $id: "73456F0F-FB13-4517-ABAC-900B76213135",
                        name: "serenitynow",
                        type: t[20],
                        frame: {
                            top: 149,
                            left: 0,
                            right: 0,
                            width: 247,
                            bottom: 27,
                            height: 68,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "0a8daedd-d6fd-4a98-9d7d-157171e46b82",
                        src: "../../assets/images/f3c14a267b2e6f44b375c91fef631056.png",
                        name: "mini-logo-blanco.png",
                        type: t[12],
                        frame: {
                            x: 121.41666666666686,
                            y: 100.24074074074076,
                            top: 0,
                            left: 50,
                            right: 47,
                            width: 150,
                            alignX: "left",
                            alignY: "top",
                            bottom: 74,
                            height: 150,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "center",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "center",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 150,
                            originalHeight: 150,
                            constrainProportions: !0
                        },
                        imageFit: "cover",
                        imageFocalPoint: "xMidYMid"
                    }, {
                        $id: "D143C30C-E842-41F5-AA34-5282482C2B14",
                        name: "CENTER FOR REFLECTIO",
                        type: t[20],
                        frame: {
                            top: 208,
                            left: 17,
                            right: 18,
                            width: 212,
                            bottom: 0,
                            height: 19,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }],
                    inAnimation: {
                        edge: "bottom",
                        type: t[18],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 0,
                        duration: 1e3,
                        undefined: 0,
                        blurRadius: 0,
                        translateX: 0,
                        translateY: .95,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    hasClippingMask: !1
                }, {
                    $id: "ce049ab9-29e8-4e8f-a6a9-27bb1a02b4a7",
                    src: "../../assets/images/b177180f9448395068cc0742b018ef19.png",
                    name: "biometric.png",
                    type: t[12],
                    frame: {
                        x: 141.25,
                        y: 374.5,
                        top: 375,
                        left: 147,
                        right: 147,
                        width: 81,
                        alignX: "left",
                        alignY: "top",
                        bottom: 211,
                        height: 81,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 128,
                        originalHeight: 128,
                        constrainProportions: !0
                    },
                    isLink: "internal",
                    gestures: {
                        tap: {
                            edge: "right",
                            target: "75b32d68-c1a3-40fe-a2f8-7d6435026c9c",
                            transition: {
                                type: t[22],
                                curve: t[19],
                                title: "Parallax",
                                margin: 1,
                                duration: 500
                            }
                        }
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }],
                gestures: {
                    swipeLeft: {
                        edge: "right",
                        target: "8A730960-47ED-4804-9598-E351DDBE3E6B",
                        timestamp: 1,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    }
                },
                $ordering: 0,
                scrolling: !1,
                hasClippingMask: !1
            }, {
                $id: "8A730960-47ED-4804-9598-E351DDBE3E6B",
                name: "Screen 1",
                type: t[11],
                frame: {
                    x: 451,
                    y: -13,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 667,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    }
                },
                maskId: null,
                content: [{
                    $id: "D8C08B00-E465-412F-8BEB-BE791BBC8FB5",
                    src: "../../assets/images/9f47340771d184a0dc809922375cf511.jpg",
                    name: "photo1b",
                    type: t[12],
                    frame: {
                        top: -15,
                        left: -447,
                        right: -201,
                        width: 1023,
                        bottom: 0,
                        height: 682,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "both"
                        }
                    },
                    subId: "808629cb",
                    imageFit: "cover",
                    inAnimation: {
                        type: t[25],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1.2,
                        title: "Scale Down",
                        styleId: "e53a1c64-098b-4dd3-94a1-7f82e2e8f06e",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        type: t[25],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1.2,
                        title: "Scale Up",
                        styleId: "unlink",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "21EACC7C-6912-4E47-B0AB-08DF8B6CC27C",
                    src: "../../assets/images/mom_scr_a_2.jpg",
                    name: "photo1a",
                    type: t[12],
                    frame: {
                        top: -102,
                        left: -191,
                        right: -385.0213243546576,
                        width: 951.0213243546576,
                        bottom: 107,
                        height: 662,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vh",
                                bottom: "vw"
                            },
                            scaling: "both"
                        }
                    },
                    subId: "126930b4",
                    imageFit: "cover",
                    inAnimation: {
                        type: t[26],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: .8,
                        title: "Scale Up",
                        styleId: "2f06fdb0-5201-4ace-b8b6-3601532e4c1a",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        type: t[25],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1.2,
                        title: "Scale Down",
                        styleId: "e53a1c64-098b-4dd3-94a1-7f82e2e8f06e",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "99ADF95D-8239-4A5A-8D66-29CDAD95AB6D",
                    name: "content",
                    type: t[21],
                    frame: {
                        x: 22,
                        y: 443,
                        top: 443,
                        left: 22,
                        right: 49,
                        width: 304,
                        bottom: 67,
                        height: 157,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    },
                    maskId: null,
                    content: [{
                        $id: "180A8DF0-11A9-4DB3-80B3-8F61C85DC1B1",
                        name: "continued effort to",
                        type: t[20],
                        frame: {
                            top: 109,
                            left: 0,
                            right: 0,
                            width: 304,
                            bottom: 0,
                            height: 48,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "22DF118B-294F-4627-A3E0-FC0F5E34055B",
                        name: "verb",
                        type: t[20],
                        frame: {
                            top: 86,
                            left: 0,
                            right: 183,
                            width: 121,
                            bottom: 52,
                            height: 19,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "78D6DDB9-0A89-4812-B558-DC4F8B5C8617",
                        name: "per·se·ver·ance",
                        type: t[20],
                        frame: {
                            top: 48,
                            left: 0,
                            right: 138,
                            width: 166,
                            bottom: 90,
                            height: 19,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "97A529DC-5C64-40AC-AE13-13698678ABAB",
                        name: "perseverance",
                        type: t[20],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 36,
                            width: 268,
                            bottom: 109,
                            height: 48,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }],
                    inAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    outAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    hasClippingMask: !1
                }],
                gestures: {
                    swipeLeft: {
                        edge: "right",
                        target: "4875BC62-1759-4F94-87F8-249946AC7555",
                        timestamp: 1,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    },
                    swipeRight: {
                        edge: "left",
                        target: "0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55",
                        timestamp: 2,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    }
                },
                $ordering: 1,
                scrolling: !1,
                hasClippingMask: !1
            }, {
                $id: "4875BC62-1759-4F94-87F8-249946AC7555",
                name: "Screen 2",
                type: t[11],
                frame: {
                    x: 915,
                    y: -13,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 667,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    }
                },
                maskId: null,
                content: [{
                    $id: "F0402241-92B6-4842-97C3-9EC5FD9FFA5C",
                    src: "../../assets/images/73b1077332ef944b17b303357782748c.jpg",
                    name: "photo2b",
                    type: t[12],
                    frame: {
                        top: -128,
                        left: -132.3333333333333,
                        right: -161.33333333333337,
                        width: 668.6666666666666,
                        bottom: -208,
                        height: 1003,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "both"
                        }
                    },
                    subId: "feddbe33",
                    imageFit: "cover",
                    inAnimation: {
                        type: t[26],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: .8,
                        title: "Scale Up",
                        styleId: "2f06fdb0-5201-4ace-b8b6-3601532e4c1a",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        type: t[26],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: .8,
                        title: "Scale Down",
                        styleId: "unlink",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "00F8606D-FE24-4A5F-A9DB-5F3DC85FE2CE",
                    src: "../../assets/images/7e54984c10f34d3a6d20b8e0971961fb.jpg",
                    name: "photo2a",
                    type: t[12],
                    frame: {
                        top: -168.1598086124401,
                        left: -132,
                        right: -161,
                        width: 668,
                        bottom: -1.1368683772161603e-13,
                        height: 835.1598086124402,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "both"
                        }
                    },
                    subId: "62dee7c8",
                    imageFit: "cover",
                    inAnimation: {
                        type: t[25],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1.2,
                        title: "Scale Down",
                        styleId: "e53a1c64-098b-4dd3-94a1-7f82e2e8f06e",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        type: t[25],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1.2,
                        title: "Scale Up",
                        styleId: "unlink",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "3232B0D6-C712-428E-9E86-CC1F247693B0",
                    name: "content",
                    type: t[21],
                    frame: {
                        x: 38,
                        y: 57,
                        top: 57,
                        left: 38,
                        right: 33,
                        width: 304,
                        bottom: 431,
                        height: 179,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    },
                    maskId: null,
                    content: [{
                        $id: "92B8D1BB-ABF6-4934-A5A5-DD7C86F569CA",
                        name: "the quality of mind",
                        type: t[20],
                        frame: {
                            top: 109,
                            left: 0,
                            right: 0,
                            width: 304,
                            bottom: 0,
                            height: 70,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "CAF8920B-94C8-4D2C-977E-FB4B60333D8C",
                        name: "verb",
                        type: t[20],
                        frame: {
                            top: 86,
                            left: 0,
                            right: 183,
                            width: 121,
                            bottom: 73,
                            height: 19,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "D687001B-C47C-4C6E-9AD0-30A05B91B664",
                        name: "cour·age",
                        type: t[20],
                        frame: {
                            top: 48,
                            left: 0,
                            right: 138,
                            width: 166,
                            bottom: 111,
                            height: 19,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "E755211E-80A8-4189-990D-8642B47AC26F",
                        name: "courage",
                        type: t[20],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 44,
                            width: 260,
                            bottom: 130,
                            height: 48,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }],
                    inAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    outAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    hasClippingMask: !1
                }],
                gestures: {
                    swipeLeft: {
                        edge: "right",
                        target: "78AF3047-7E04-4101-98E0-A71DDC18E506",
                        timestamp: 1,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    },
                    swipeRight: {
                        edge: "left",
                        target: "8A730960-47ED-4804-9598-E351DDBE3E6B",
                        timestamp: 2,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    }
                },
                $ordering: 2,
                scrolling: !1,
                hasClippingMask: !1
            }, {
                $id: "78AF3047-7E04-4101-98E0-A71DDC18E506",
                name: "Screen 3",
                type: t[11],
                frame: {
                    x: 1379,
                    y: -13,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 667,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    }
                },
                maskId: null,
                content: [{
                    $id: "1547FC76-E0E7-478F-9F3A-00D7DF95C911",
                    src: "../../assets/images/0dd5820a1b3f3ff852914ce253f40096.jpg",
                    name: "photo3b",
                    type: t[12],
                    frame: {
                        top: -75,
                        left: -465,
                        right: -524,
                        width: 1364,
                        bottom: -87,
                        height: 828,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "both"
                        },
                        rotateAngle: 0
                    },
                    subId: "2472b260",
                    imageFit: "cover",
                    inAnimation: {
                        type: t[26],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: .8,
                        title: "Scale Up",
                        styleId: "2f06fdb0-5201-4ace-b8b6-3601532e4c1a",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        type: t[26],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: .8,
                        title: "Scale Down",
                        styleId: "unlink",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "4ED541B6-4A7F-465F-BE69-5C783B28DBD0",
                    src: "../../assets/images/c41f624bcfafa300e9fb2f0679882a21.jpg",
                    name: "photo3a",
                    type: t[12],
                    frame: {
                        top: -6,
                        left: -343.874279123414,
                        right: -343.874279123414,
                        width: 1062.748558246828,
                        bottom: -6,
                        height: 679,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "both"
                        }
                    },
                    subId: "b677fa51",
                    imageFit: "cover",
                    inAnimation: {
                        type: t[25],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1.2,
                        title: "Scale Down",
                        styleId: "e53a1c64-098b-4dd3-94a1-7f82e2e8f06e",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        type: t[25],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1.2,
                        title: "Scale Up",
                        styleId: "unlink",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "804DE8A9-8968-4990-9FBD-D18EE6AFC107",
                    name: "content",
                    type: t[21],
                    frame: {
                        x: 38,
                        y: 447,
                        top: 447,
                        left: 38,
                        right: 62,
                        width: 275,
                        bottom: 65,
                        height: 155,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    },
                    maskId: null,
                    content: [{
                        $id: "CB626D74-4FCF-4586-96E9-090DD7F67E33",
                        name: "humility",
                        type: t[20],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 49,
                            width: 226,
                            bottom: 107,
                            height: 48,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "37554E6A-A620-4C46-BFA5-5E65E1F8AB75",
                        name: "hu·​mil·​i·​ty",
                        type: t[20],
                        frame: {
                            top: 48,
                            left: 0,
                            right: 109,
                            width: 166,
                            bottom: 88,
                            height: 19,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "C1F648B8-EE63-4FC6-9D55-CB4C423B3DD5",
                        name: "noun",
                        type: t[20],
                        frame: {
                            top: 86,
                            left: 0,
                            right: 154,
                            width: 121,
                            bottom: 50,
                            height: 19,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "03B98670-B85C-443B-8552-83F32958DA7A",
                        name: "freedom from pride o",
                        type: t[20],
                        frame: {
                            top: 107,
                            left: 0,
                            right: 0,
                            width: 275,
                            bottom: 0,
                            height: 48,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }],
                    inAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    outAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    hasClippingMask: !1
                }],
                gestures: {
                    swipeLeft: {
                        edge: "right",
                        target: "E4D65595-1DA8-41B2-B0D7-B2E3586404A9",
                        timestamp: 1,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    },
                    swipeRight: {
                        edge: "left",
                        target: "4875BC62-1759-4F94-87F8-249946AC7555",
                        timestamp: 2,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    }
                },
                $ordering: 3,
                scrolling: !1,
                hasClippingMask: !1
            }, {
                $id: "E4D65595-1DA8-41B2-B0D7-B2E3586404A9",
                name: "Screen 4",
                type: t[11],
                frame: {
                    x: 1843,
                    y: -13,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 667,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    }
                },
                maskId: null,
                content: [{
                    $id: "BE2C2E72-8EE7-429D-A307-57FE1CB76642",
                    src: "../../assets/images/e8edf9abb946fee95460f98f47755434.jpg",
                    name: "photo4b",
                    type: t[12],
                    frame: {
                        top: 0,
                        left: -35.08786491127648,
                        right: -35.08786491127643,
                        width: 445.1757298225529,
                        bottom: 0,
                        height: 667,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "both"
                        }
                    },
                    subId: "df7a7e1c",
                    imageFit: "cover",
                    inAnimation: {
                        type: t[25],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1.2,
                        title: "Scale Down",
                        styleId: "e53a1c64-098b-4dd3-94a1-7f82e2e8f06e",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        type: t[25],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1.2,
                        title: "Scale Up",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "c05eeab9-f1bc-4698-9ee4-83962853879c",
                    src: "../../assets/images/cf940423f358f1624efd2de13f447df9.jpg",
                    name: "photo4a",
                    type: t[12],
                    frame: {
                        x: 130,
                        y: 250.3333333333336,
                        top: -69,
                        left: -130,
                        right: -81,
                        width: 587,
                        alignX: "left",
                        alignY: "top",
                        bottom: -98,
                        height: 834,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "both"
                        },
                        rotateAngle: 0,
                        originalWidth: 1142,
                        originalHeight: 1628,
                        constrainProportions: !0
                    },
                    subId: "d0b18789",
                    imageFit: "cover",
                    inAnimation: {
                        type: t[26],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: .8,
                        title: "Scale Up",
                        styleId: "2f06fdb0-5201-4ace-b8b6-3601532e4c1a",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        type: t[26],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: .8,
                        title: "Scale Down",
                        duration: 4e3,
                        undefined: 0,
                        animationBasis: "time"
                    },
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "084A0385-8768-4CF2-AE02-703268C01C43",
                    name: "content",
                    type: t[21],
                    frame: {
                        x: 38,
                        y: 57,
                        top: 57,
                        left: 38,
                        right: 59,
                        width: 278,
                        bottom: 455,
                        height: 155,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    },
                    maskId: null,
                    content: [{
                        $id: "06BDAA54-F03F-48D4-9302-CAD8344B07F7",
                        name: "to train or develop",
                        type: t[20],
                        frame: {
                            top: 107,
                            left: 0,
                            right: 0,
                            width: 278,
                            bottom: 0,
                            height: 48,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "20A5E2AA-26F3-4F56-B13E-C1A50D374D78",
                        name: "verb",
                        type: t[20],
                        frame: {
                            top: 86,
                            left: 0,
                            right: 157,
                            width: 121,
                            bottom: 50,
                            height: 19,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "66579892-F7FB-4387-9638-F25380FD9647",
                        name: "dis·ci·pline",
                        type: t[20],
                        frame: {
                            top: 48,
                            left: 0,
                            right: 112,
                            width: 166,
                            bottom: 88,
                            height: 19,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "6C681A76-99BA-45B9-AD64-563FAA623013",
                        name: "discipline",
                        type: t[20],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 28,
                            width: 250,
                            bottom: 107,
                            height: 48,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }],
                    inAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    outAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    hasClippingMask: !1
                }],
                gestures: {
                    swipeLeft: {
                        edge: "right",
                        target: "2EA69AE2-03D2-48F6-B667-7CAE3D2C4100",
                        timestamp: 1,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    },
                    swipeRight: {
                        edge: "left",
                        target: "78AF3047-7E04-4101-98E0-A71DDC18E506",
                        timestamp: 2,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    }
                },
                $ordering: 4,
                scrolling: !1,
                hasClippingMask: !1
            }, {
                $id: "2EA69AE2-03D2-48F6-B667-7CAE3D2C4100",
                name: "End",
                type: t[11],
                frame: {
                    x: 2307,
                    y: -13,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 667,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    }
                },
                maskId: null,
                content: [{
                    $id: "C0666904-6E33-46CC-AC18-4F2D73EDD541",
                    src: "../../assets/images/mom_bg.jpg",
                    name: "cover",
                    type: t[12],
                    frame: {
                        top: 0,
                        left: 0,
                        right: 0,
                        width: 375,
                        bottom: 0,
                        height: 667,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "px",
                                right: "px"
                            },
                            y: {
                                pos: "center",
                                top: "px",
                                bottom: "px"
                            },
                            scaling: "both"
                        }
                    },
                    subId: "9837e389",
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "e2ec743b-44f6-4ef4-ab5e-9afe78246b94",
                    name: "logo copy",
                    type: t[21],
                    frame: {
                        x: 64,
                        y: 80,
                        top: 80,
                        left: 64,
                        right: 64,
                        width: 247,
                        bottom: 360,
                        height: 227,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    },
                    maskId: null,
                    content: [{
                        $id: "9c21ac66-77f1-4688-ab7c-7cf48e2fc779",
                        name: "serenitynow",
                        type: t[20],
                        frame: {
                            top: 149,
                            left: 0,
                            right: 0,
                            width: 247,
                            bottom: 10,
                            height: 68,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "efcc2c92-aa2b-4924-900f-fe32b99fe630",
                        src: "../../assets/images/f3c14a267b2e6f44b375c91fef631056.png",
                        name: "mini-logo-blanco.png",
                        type: t[12],
                        frame: {
                            x: 121.41666666666686,
                            y: 100.24074074074076,
                            top: 0,
                            left: 50,
                            right: 47,
                            width: 150,
                            alignX: "left",
                            alignY: "top",
                            bottom: 77,
                            height: 150,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "center",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "center",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 150,
                            originalHeight: 150,
                            constrainProportions: !0
                        },
                        imageFit: "cover",
                        imageFocalPoint: "xMidYMid"
                    }, {
                        $id: "44231c55-0c80-4b54-9a0d-a3f98d54b853",
                        name: "CENTER FOR REFLECTIO",
                        type: t[20],
                        frame: {
                            top: 208,
                            left: 17,
                            right: 18,
                            width: 212,
                            bottom: 0,
                            height: 19,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }],
                    inAnimation: {
                        edge: "bottom",
                        type: t[18],
                        curve: t[19],
                        delay: 0,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 0,
                        duration: 1e3,
                        undefined: 0,
                        blurRadius: 0,
                        translateX: 0,
                        translateY: .95,
                        animationBasis: "time"
                    },
                    outAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: -.5,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    hasClippingMask: !1
                }, {
                    $id: "F93A6F64-E8B8-4E31-B29F-463B573DDEEC",
                    name: "social",
                    type: t[21],
                    frame: {
                        x: 87,
                        y: 450,
                        top: 450,
                        left: 87,
                        right: 87,
                        width: 200,
                        bottom: 136,
                        height: 81,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    },
                    maskId: null,
                    content: [{
                        $id: "44BF805F-10F4-43AD-A72A-6E966CAB765F",
                        name: "CONNECT & REFLECT",
                        type: t[20],
                        frame: {
                            top: 65,
                            left: 13,
                            right: 15,
                            width: 172,
                            bottom: 0,
                            height: 16,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        maskId: null,
                        hasClippingMask: !1
                    }, {
                        $id: "6C9FFB58-30C4-4D7F-AE90-CE8E9C238305",
                        link: {
                            0: {
                                $id: "B3CA3D2A-923F-43C2-B134-14D713016B49",
                                name: "Oval",
                                type: "svg",
                                frame: {
                                    top: -3.552713678800501e-15,
                                    left: 0,
                                    right: .47857459937358726,
                                    width: 48.52142540062641,
                                    bottom: .4785745993734736,
                                    height: 48.52142540062653,
                                    responsive: {
                                        x: {
                                            pos: "left",
                                            left: "vw",
                                            right: "vw"
                                        },
                                        y: {
                                            pos: "top",
                                            top: "vw",
                                            bottom: "vw"
                                        },
                                        scaling: "vw"
                                    }
                                },
                                layer: {
                                    name: "Oval",
                                    frame: {
                                        x: 0,
                                        y: -3.552713678800501e-15,
                                        top: -3.552713678800501e-15,
                                        left: 0,
                                        right: .47857459937358726,
                                        width: 48.52142540062641,
                                        _class: "rect",
                                        bottom: .4785745993734736,
                                        height: 48.52142540062653,
                                        constrainProportions: !0
                                    },
                                    style: {
                                        blur: {
                                            type: 0,
                                            _class: "blur",
                                            center: [.5, .5],
                                            radius: 10,
                                            isEnabled: !1,
                                            saturation: 1,
                                            motionAngle: 0
                                        },
                                        fills: [{
                                            color: {
                                                red: 1,
                                                blue: 1,
                                                alpha: 1,
                                                green: 1,
                                                _class: "color"
                                            },
                                            _class: "fill",
                                            opacity: 1,
                                            fillType: 0,
                                            gradient: {
                                                to: [.5, 1],
                                                from: [.5, 0],
                                                stops: [{
                                                    color: {
                                                        red: 1,
                                                        blue: 1,
                                                        alpha: 1,
                                                        green: 1,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 0
                                                }, {
                                                    color: {
                                                        red: 0,
                                                        blue: 0,
                                                        alpha: 1,
                                                        green: 0,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 1
                                                }],
                                                _class: "gradient",
                                                elipseLength: 0,
                                                gradientType: 0
                                            },
                                            blendMode: "normal",
                                            isEnabled: !0,
                                            noiseIndex: 0,
                                            noiseIntensity: 0,
                                            patternFillType: 1,
                                            patternTileScale: 1
                                        }],
                                        _class: "style",
                                        borders: [{
                                            color: {
                                                red: .592,
                                                blue: .592,
                                                alpha: 1,
                                                green: .592,
                                                _class: "color"
                                            },
                                            _class: "border",
                                            fillType: 0,
                                            gradient: {
                                                to: [.5, 1],
                                                from: [.5, 0],
                                                stops: [{
                                                    color: {
                                                        red: 1,
                                                        blue: 1,
                                                        alpha: 1,
                                                        green: 1,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 0
                                                }, {
                                                    color: {
                                                        red: 0,
                                                        blue: 0,
                                                        alpha: 1,
                                                        green: 0,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 1
                                                }],
                                                _class: "gradient",
                                                elipseLength: 0,
                                                gradientType: 0
                                            },
                                            position: 0,
                                            isEnabled: !1,
                                            thickness: 1,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            }
                                        }],
                                        shadows: [],
                                        miterLimit: 10,
                                        do_objectID: "73C05DE9-A25F-4DFB-A337-4532508478FD",
                                        windingRule: 0,
                                        innerShadows: [],
                                        borderOptions: {
                                            _class: "borderOptions",
                                            isEnabled: !0,
                                            dashPattern: [],
                                            lineCapStyle: 0,
                                            lineJoinStyle: 0
                                        },
                                        colorControls: {
                                            hue: 0,
                                            _class: "colorControls",
                                            contrast: 1,
                                            isEnabled: !1,
                                            brightness: 0,
                                            saturation: 1
                                        },
                                        endMarkerType: 0,
                                        contextSettings: {
                                            _class: "graphicsContextSettings",
                                            opacity: .2021716889880952,
                                            blendMode: 0
                                        },
                                        startMarkerType: 0
                                    },
                                    _class: "oval",
                                    edited: !1,
                                    points: [{
                                        point: [.5, 1],
                                        _class: "curvePoint",
                                        curveTo: [.2238576251, 1],
                                        curveFrom: [.7761423749, 1],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }, {
                                        point: [1, .5],
                                        _class: "curvePoint",
                                        curveTo: [1, .7761423749],
                                        curveFrom: [1, .2238576251],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }, {
                                        point: [.5, 0],
                                        _class: "curvePoint",
                                        curveTo: [.7761423749, 0],
                                        curveFrom: [.2238576251, 0],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }, {
                                        point: [0, .5],
                                        _class: "curvePoint",
                                        curveTo: [0, .2238576251],
                                        curveFrom: [0, .7761423749],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }],
                                    isClosed: !0,
                                    isLocked: !1,
                                    rotation: 0,
                                    isVisible: !0,
                                    do_objectID: "B3CA3D2A-923F-43C2-B134-14D713016B49",
                                    nameIsFixed: !1,
                                    resizingType: 0,
                                    exportOptions: {
                                        _class: "exportOptions",
                                        shouldTrim: !1,
                                        layerOptions: 0,
                                        exportFormats: [],
                                        includedLayerIds: []
                                    },
                                    hasClippingMask: !1,
                                    booleanOperation: -1,
                                    clippingMaskMode: 0,
                                    isFixedToViewport: !1,
                                    isFlippedVertical: !1,
                                    resizingConstraint: 63,
                                    isFlippedHorizontal: !1,
                                    pointRadiusBehaviour: 1,
                                    shouldBreakMaskChain: !1,
                                    layerListExpandedType: 0
                                },
                                style: {
                                    blur: {
                                        type: 0,
                                        _class: "blur",
                                        center: [.5, .5],
                                        radius: 10,
                                        isEnabled: !1,
                                        saturation: 1,
                                        motionAngle: 0
                                    },
                                    fills: [{
                                        color: {
                                            red: 1,
                                            blue: 1,
                                            alpha: 1,
                                            green: 1,
                                            _class: "color"
                                        },
                                        _class: "fill",
                                        opacity: 1,
                                        fillType: 0,
                                        gradient: {
                                            to: [.5, 1],
                                            from: [.5, 0],
                                            stops: [{
                                                color: {
                                                    red: 1,
                                                    blue: 1,
                                                    alpha: 1,
                                                    green: 1,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 0
                                            }, {
                                                color: {
                                                    red: 0,
                                                    blue: 0,
                                                    alpha: 1,
                                                    green: 0,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 1
                                            }],
                                            _class: "gradient",
                                            elipseLength: 0,
                                            gradientType: 0
                                        },
                                        blendMode: "normal",
                                        isEnabled: !0,
                                        noiseIndex: 0,
                                        noiseIntensity: 0,
                                        patternFillType: 1,
                                        patternTileScale: 1
                                    }],
                                    _class: "style",
                                    borders: [{
                                        color: {
                                            red: .592,
                                            blue: .592,
                                            alpha: 1,
                                            green: .592,
                                            _class: "color"
                                        },
                                        _class: "border",
                                        fillType: 0,
                                        gradient: {
                                            to: [.5, 1],
                                            from: [.5, 0],
                                            stops: [{
                                                color: {
                                                    red: 1,
                                                    blue: 1,
                                                    alpha: 1,
                                                    green: 1,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 0
                                            }, {
                                                color: {
                                                    red: 0,
                                                    blue: 0,
                                                    alpha: 1,
                                                    green: 0,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 1
                                            }],
                                            _class: "gradient",
                                            elipseLength: 0,
                                            gradientType: 0
                                        },
                                        position: 0,
                                        isEnabled: !1,
                                        thickness: 1,
                                        contextSettings: {
                                            _class: "graphicsContextSettings",
                                            opacity: 1,
                                            blendMode: 0
                                        }
                                    }],
                                    opacity: .2021716889880952,
                                    shadows: [],
                                    blendMode: "normal",
                                    miterLimit: 10,
                                    do_objectID: "73C05DE9-A25F-4DFB-A337-4532508478FD",
                                    windingRule: 0,
                                    innerShadows: [],
                                    borderOptions: {
                                        _class: "borderOptions",
                                        isEnabled: !0,
                                        dashPattern: [],
                                        lineCapStyle: 0,
                                        lineJoinStyle: 0
                                    },
                                    colorControls: {
                                        hue: 0,
                                        _class: "colorControls",
                                        contrast: 1,
                                        isEnabled: !1,
                                        brightness: 0,
                                        saturation: 1
                                    },
                                    endMarkerType: 0,
                                    startMarkerType: 0
                                },
                                maskId: null,
                                hasClippingMask: !1
                            },
                            1: {
                                $id: "936CAE08-3BDC-451B-81CB-BA99B478549D",
                                name: "icon",
                                type: "svg",
                                frame: {
                                    top: 16.66071270031338,
                                    left: 14.67784050004901,
                                    right: 13.15641509942396,
                                    width: 21.16574440052703,
                                    bottom: 15.139287299686622,
                                    height: 17.2,
                                    responsive: {
                                        x: {
                                            pos: "left",
                                            left: "vw",
                                            right: "vw"
                                        },
                                        y: {
                                            pos: "top",
                                            top: "vw",
                                            bottom: "vw"
                                        },
                                        scaling: "vw"
                                    }
                                },
                                layer: {
                                    name: "icon",
                                    frame: {
                                        x: 14.67784050004901,
                                        y: 16.66071270031338,
                                        top: 16.66071270031338,
                                        left: 14.67784050004901,
                                        right: 13.15641509942396,
                                        width: 21.16574440052703,
                                        _class: "rect",
                                        bottom: 15.139287299686622,
                                        height: 17.2,
                                        constrainProportions: !1
                                    },
                                    style: {
                                        blur: {
                                            type: 0,
                                            _class: "blur",
                                            center: [.5, .5],
                                            radius: 13,
                                            isEnabled: !1,
                                            saturation: 1,
                                            motionAngle: 0
                                        },
                                        fills: [{
                                            color: {
                                                red: 1,
                                                blue: 1,
                                                alpha: 1,
                                                green: 1,
                                                _class: "color"
                                            },
                                            _class: "fill",
                                            opacity: 1,
                                            fillType: 0,
                                            gradient: {
                                                to: [.5, 1],
                                                from: [.5, 0],
                                                stops: [{
                                                    color: {
                                                        red: 1,
                                                        blue: 1,
                                                        alpha: 1,
                                                        green: 1,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 0
                                                }, {
                                                    color: {
                                                        red: 0,
                                                        blue: 0,
                                                        alpha: 1,
                                                        green: 0,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 1
                                                }],
                                                _class: "gradient",
                                                elipseLength: 0,
                                                gradientType: 0
                                            },
                                            blendMode: "normal",
                                            isEnabled: !0,
                                            noiseIndex: 0,
                                            noiseIntensity: 0,
                                            patternFillType: 1,
                                            patternTileScale: 1
                                        }],
                                        _class: "style",
                                        borders: [{
                                            color: {
                                                red: .533,
                                                blue: .533,
                                                alpha: 1,
                                                green: .533,
                                                _class: "color"
                                            },
                                            _class: "border",
                                            fillType: 0,
                                            position: 1,
                                            isEnabled: !1,
                                            thickness: 1
                                        }],
                                        shadows: [],
                                        miterLimit: 10,
                                        do_objectID: "AF7724DD-D969-4EF3-B059-BB8B0C3879B6",
                                        windingRule: 1,
                                        innerShadows: [],
                                        borderOptions: {
                                            _class: "borderOptions",
                                            isEnabled: !0,
                                            dashPattern: [],
                                            lineCapStyle: 0,
                                            lineJoinStyle: 0
                                        },
                                        colorControls: {
                                            hue: 0,
                                            _class: "colorControls",
                                            contrast: 1,
                                            isEnabled: !1,
                                            brightness: 0,
                                            saturation: 1
                                        },
                                        endMarkerType: 0,
                                        contextSettings: {
                                            _class: "graphicsContextSettings",
                                            opacity: 1,
                                            blendMode: 0
                                        },
                                        startMarkerType: 0
                                    },
                                    _class: "shapeGroup",
                                    layers: [{
                                        name: "icon",
                                        frame: {
                                            x: -1.048050535246148e-13,
                                            y: 2.309263891220326e-14,
                                            top: 2.309263891220326e-14,
                                            left: -1.048050535246148e-13,
                                            right: 1.0658141036401503e-13,
                                            width: 21.16574440052703,
                                            _class: "rect",
                                            bottom: -2.4868995751603507e-14,
                                            height: 17.2,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 13,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .1333333333333333,
                                                    blue: .3176470588235294,
                                                    alpha: 1,
                                                    green: .2509803921568627,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                gradient: {
                                                    to: [.5, 1],
                                                    from: [.5, 0],
                                                    stops: [{
                                                        color: {
                                                            red: 1,
                                                            blue: 1,
                                                            alpha: 1,
                                                            green: 1,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 0
                                                    }, {
                                                        color: {
                                                            red: 0,
                                                            blue: 0,
                                                            alpha: 1,
                                                            green: 0,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 1
                                                    }],
                                                    _class: "gradient",
                                                    elipseLength: 0,
                                                    gradientType: 0
                                                },
                                                isEnabled: !0,
                                                noiseIndex: 0,
                                                noiseIntensity: 0,
                                                contextSettings: {
                                                    _class: "graphicsContextSettings",
                                                    opacity: 1,
                                                    blendMode: 0
                                                },
                                                patternFillType: 1,
                                                patternTileScale: 1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "B2B9FBA0-CF44-4C4A-A400-F824F80C553D",
                                            windingRule: 1,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [.9999999999999999, .1183662714097497],
                                            _class: "curvePoint",
                                            curveTo: [.9999999999999999, .1183662714097497],
                                            curveFrom: [.9632119914346893, .13844532279314892],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.8821413276231262, .1581554677206852],
                                            _class: "curvePoint",
                                            curveTo: [.9236402569593146, .15204216073781293],
                                            curveFrom: [.9244539614561027, .12690382081686427],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.9723340471092076, .018445322793148783],
                                            _class: "curvePoint",
                                            curveTo: [.957002141327623, .0774176548089591],
                                            curveFrom: [.9326766595289079, .047430830039525654],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.8420556745182012, .07978919631093546],
                                            _class: "curvePoint",
                                            curveTo: [.8888222698072804, .06845849802371544],
                                            curveFrom: [.8046680942184153, .030671936758893157],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.6923340471092075, 0],
                                            _class: "curvePoint",
                                            curveTo: [.7513062098501069, 0],
                                            curveFrom: [.5790149892933618, 0],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.487152034261242, .2524901185770752],
                                            _class: "curvePoint",
                                            curveTo: [.487152034261242, .11299077733860342],
                                            curveFrom: [.487152034261242, .2722529644268776],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.4925053533190578, .3100395256916998],
                                            _class: "curvePoint",
                                            curveTo: [.48899357601713056, .2914888010540186],
                                            curveFrom: [.3219700214132762, .29949934123847194],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.06959314775160598, .046218708827404455],
                                            _class: "curvePoint",
                                            curveTo: [.17083511777301927, .1989459815546773],
                                            curveFrom: [.051948608137045, .08353096179183128],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.04184154175588865, .17312252964426883],
                                            _class: "curvePoint",
                                            curveTo: [.04184154175588865, .12690382081686427],
                                            curveFrom: [.04184154175588865, .260711462450593],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.1331049250535332, .38324110671936784],
                                            _class: "curvePoint",
                                            curveTo: [.0780728051391863, .3380237154150199],
                                            curveFrom: [.09948608137044967, .38197628458498045],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.04025695931477521, .3517259552042163],
                                            _class: "curvePoint",
                                            curveTo: [.06783725910064237, .37059288537549434],
                                            curveFrom: [.0401713062098502, .3527272727272729],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.0401713062098502, .3548880105401846],
                                            _class: "curvePoint",
                                            curveTo: [.0401713062098502, .3537812911725957],
                                            curveFrom: [.0401713062098502, .47720685111989497],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.20479657387580297, .6024242424242429],
                                            _class: "curvePoint",
                                            curveTo: [.1109207708779443, .5791831357048751],
                                            curveFrom: [.18753747323340472, .6081686429512522],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.15074946466809416, .6113306982872205],
                                            _class: "curvePoint",
                                            curveTo: [.1693790149892934, .6113306982872205],
                                            curveFrom: [.13743040685224833, .6113306982872205],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.11211991434689514, .6067457180500663],
                                            _class: "curvePoint",
                                            curveTo: [.12466809421841546, .609644268774704],
                                            curveFrom: [.13820128479657384, .7070355731225301],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.3037259100642399, .782081686429513],
                                            _class: "curvePoint",
                                            curveTo: [.21396145610278378, .7800263504611337],
                                            curveFrom: [.23357601713062098, .8498023715415026],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.04895074946466811, .8901712779973655],
                                            _class: "curvePoint",
                                            curveTo: [.14505353319057815, .8901712779973655],
                                            curveFrom: [.03237687366167023, .8901712779973655],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [0, .8865876152832681],
                                            _class: "curvePoint",
                                            curveTo: [.016059957173447534, .8889064558629781],
                                            curveFrom: [.09083511777301935, .958208168642952],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.3145182012847966, 1.0000000000000007],
                                            _class: "curvePoint",
                                            curveTo: [.1986723768736616, 1.0000000000000007],
                                            curveFrom: [.6918629550321198, 1.0000000000000007],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.8982012847965738, .2817391304347827],
                                            _class: "curvePoint",
                                            curveTo: [.8982012847965738, .615335968379447],
                                            curveFrom: [.8982012847965738, .27083003952569185],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.8976017130620985, .24906455862977617],
                                            _class: "curvePoint",
                                            curveTo: [.8980299785867236, .2599209486166009],
                                            curveFrom: [.9376873661670235, .21349143610013183],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.9999999999999999, .1183662714097497],
                                            _class: "curvePoint",
                                            curveTo: [.9724625267665952, .16901185770750995],
                                            curveFrom: [.9999999999999999, .1183662714097497],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }],
                                        isClosed: !1,
                                        isLocked: !1,
                                        rotation: 0,
                                        userInfo: {
                                            "com.animaapp.stc-sketch-plugin": {
                                                kModelPropertiesKey: {}
                                            }
                                        },
                                        isVisible: !0,
                                        do_objectID: "4C45C0A5-0C67-44E2-8ADC-79A6961EAC27",
                                        nameIsFixed: !0,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: 0,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 1
                                    }, {
                                        name: "icon",
                                        frame: {
                                            x: -1.048050535246148e-13,
                                            y: 2.309263891220326e-14,
                                            top: 2.309263891220326e-14,
                                            left: -1.048050535246148e-13,
                                            right: 1.0658141036401503e-13,
                                            width: 21.16574440052703,
                                            _class: "rect",
                                            bottom: -2.4868995751603507e-14,
                                            height: 17.2,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 13,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .1333333333333333,
                                                    blue: .3176470588235294,
                                                    alpha: 1,
                                                    green: .2509803921568627,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                gradient: {
                                                    to: [.5, 1],
                                                    from: [.5, 0],
                                                    stops: [{
                                                        color: {
                                                            red: 1,
                                                            blue: 1,
                                                            alpha: 1,
                                                            green: 1,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 0
                                                    }, {
                                                        color: {
                                                            red: 0,
                                                            blue: 0,
                                                            alpha: 1,
                                                            green: 0,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 1
                                                    }],
                                                    _class: "gradient",
                                                    elipseLength: 0,
                                                    gradientType: 0
                                                },
                                                isEnabled: !0,
                                                noiseIndex: 0,
                                                noiseIntensity: 0,
                                                contextSettings: {
                                                    _class: "graphicsContextSettings",
                                                    opacity: 1,
                                                    blendMode: 0
                                                },
                                                patternFillType: 1,
                                                patternTileScale: 1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "0ACF0065-5081-45F3-9F0C-24FCC3B871CD",
                                            windingRule: 1,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [.9999999999999999, .1183662714097497],
                                            _class: "curvePoint",
                                            curveTo: [.9999999999999999, .1183662714097497],
                                            curveFrom: [.9632119914346893, .13844532279314892],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.8821413276231262, .1581554677206852],
                                            _class: "curvePoint",
                                            curveTo: [.9236402569593146, .15204216073781293],
                                            curveFrom: [.9244539614561027, .12690382081686427],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.9723340471092076, .018445322793148783],
                                            _class: "curvePoint",
                                            curveTo: [.957002141327623, .0774176548089591],
                                            curveFrom: [.9326766595289079, .047430830039525654],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.8420556745182012, .07978919631093546],
                                            _class: "curvePoint",
                                            curveTo: [.8888222698072804, .06845849802371544],
                                            curveFrom: [.8046680942184153, .030671936758893157],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.6923340471092075, 0],
                                            _class: "curvePoint",
                                            curveTo: [.7513062098501069, 0],
                                            curveFrom: [.5790149892933618, 0],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.487152034261242, .2524901185770752],
                                            _class: "curvePoint",
                                            curveTo: [.487152034261242, .11299077733860342],
                                            curveFrom: [.487152034261242, .2722529644268776],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.4925053533190578, .3100395256916998],
                                            _class: "curvePoint",
                                            curveTo: [.48899357601713056, .2914888010540186],
                                            curveFrom: [.3219700214132762, .29949934123847194],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.06959314775160598, .046218708827404455],
                                            _class: "curvePoint",
                                            curveTo: [.17083511777301927, .1989459815546773],
                                            curveFrom: [.051948608137045, .08353096179183128],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.04184154175588865, .17312252964426883],
                                            _class: "curvePoint",
                                            curveTo: [.04184154175588865, .12690382081686427],
                                            curveFrom: [.04184154175588865, .260711462450593],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.1331049250535332, .38324110671936784],
                                            _class: "curvePoint",
                                            curveTo: [.0780728051391863, .3380237154150199],
                                            curveFrom: [.09948608137044967, .38197628458498045],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.04025695931477521, .3517259552042163],
                                            _class: "curvePoint",
                                            curveTo: [.06783725910064237, .37059288537549434],
                                            curveFrom: [.0401713062098502, .3527272727272729],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.0401713062098502, .3548880105401846],
                                            _class: "curvePoint",
                                            curveTo: [.0401713062098502, .3537812911725957],
                                            curveFrom: [.0401713062098502, .47720685111989497],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.20479657387580297, .6024242424242429],
                                            _class: "curvePoint",
                                            curveTo: [.1109207708779443, .5791831357048751],
                                            curveFrom: [.18753747323340472, .6081686429512522],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.15074946466809416, .6113306982872205],
                                            _class: "curvePoint",
                                            curveTo: [.1693790149892934, .6113306982872205],
                                            curveFrom: [.13743040685224833, .6113306982872205],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.11211991434689514, .6067457180500663],
                                            _class: "curvePoint",
                                            curveTo: [.12466809421841546, .609644268774704],
                                            curveFrom: [.13820128479657384, .7070355731225301],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.3037259100642399, .782081686429513],
                                            _class: "curvePoint",
                                            curveTo: [.21396145610278378, .7800263504611337],
                                            curveFrom: [.23357601713062098, .8498023715415026],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.04895074946466811, .8901712779973655],
                                            _class: "curvePoint",
                                            curveTo: [.14505353319057815, .8901712779973655],
                                            curveFrom: [.03237687366167023, .8901712779973655],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [0, .8865876152832681],
                                            _class: "curvePoint",
                                            curveTo: [.016059957173447534, .8889064558629781],
                                            curveFrom: [.09083511777301935, .958208168642952],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.3145182012847966, 1.0000000000000007],
                                            _class: "curvePoint",
                                            curveTo: [.1986723768736616, 1.0000000000000007],
                                            curveFrom: [.6918629550321198, 1.0000000000000007],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.8982012847965738, .2817391304347827],
                                            _class: "curvePoint",
                                            curveTo: [.8982012847965738, .615335968379447],
                                            curveFrom: [.8982012847965738, .27083003952569185],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.8976017130620985, .24906455862977617],
                                            _class: "curvePoint",
                                            curveTo: [.8980299785867236, .2599209486166009],
                                            curveFrom: [.9376873661670235, .21349143610013183],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.9999999999999999, .1183662714097497],
                                            _class: "curvePoint",
                                            curveTo: [.9724625267665952, .16901185770750995],
                                            curveFrom: [.9999999999999999, .1183662714097497],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }],
                                        isClosed: !1,
                                        isLocked: !1,
                                        rotation: 0,
                                        userInfo: {
                                            "com.animaapp.stc-sketch-plugin": {
                                                kModelPropertiesKey: {}
                                            }
                                        },
                                        isVisible: !0,
                                        do_objectID: "2F5639EA-8498-435E-B179-5DEE7A613F29",
                                        nameIsFixed: !0,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: 0,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 1
                                    }],
                                    isLocked: !1,
                                    rotation: 0,
                                    isVisible: !0,
                                    do_objectID: "936CAE08-3BDC-451B-81CB-BA99B478549D",
                                    groupLayout: {
                                        _class: "MSImmutableFreeformGroupLayout"
                                    },
                                    nameIsFixed: !0,
                                    windingRule: 1,
                                    resizingType: 0,
                                    exportOptions: {
                                        _class: "exportOptions",
                                        shouldTrim: !1,
                                        layerOptions: 0,
                                        exportFormats: [],
                                        includedLayerIds: []
                                    },
                                    hasClickThrough: !1,
                                    hasClippingMask: !1,
                                    booleanOperation: -1,
                                    clippingMaskMode: 0,
                                    isFixedToViewport: !1,
                                    isFlippedVertical: !1,
                                    resizingConstraint: 63,
                                    isFlippedHorizontal: !1,
                                    shouldBreakMaskChain: !1,
                                    layerListExpandedType: 1
                                },
                                style: {
                                    blur: {
                                        type: 0,
                                        _class: "blur",
                                        center: [.5, .5],
                                        radius: 13,
                                        isEnabled: !1,
                                        saturation: 1,
                                        motionAngle: 0
                                    },
                                    fills: [{
                                        color: {
                                            red: 1,
                                            blue: 1,
                                            alpha: 1,
                                            green: 1,
                                            _class: "color"
                                        },
                                        _class: "fill",
                                        opacity: 1,
                                        fillType: 0,
                                        gradient: {
                                            to: [.5, 1],
                                            from: [.5, 0],
                                            stops: [{
                                                color: {
                                                    red: 1,
                                                    blue: 1,
                                                    alpha: 1,
                                                    green: 1,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 0
                                            }, {
                                                color: {
                                                    red: 0,
                                                    blue: 0,
                                                    alpha: 1,
                                                    green: 0,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 1
                                            }],
                                            _class: "gradient",
                                            elipseLength: 0,
                                            gradientType: 0
                                        },
                                        blendMode: "normal",
                                        isEnabled: !0,
                                        noiseIndex: 0,
                                        noiseIntensity: 0,
                                        patternFillType: 1,
                                        patternTileScale: 1
                                    }],
                                    _class: "style",
                                    borders: [{
                                        color: {
                                            red: .533,
                                            blue: .533,
                                            alpha: 1,
                                            green: .533,
                                            _class: "color"
                                        },
                                        _class: "border",
                                        fillType: 0,
                                        position: 1,
                                        isEnabled: !1,
                                        thickness: 1
                                    }],
                                    opacity: 1,
                                    shadows: [],
                                    blendMode: "normal",
                                    miterLimit: 10,
                                    do_objectID: "AF7724DD-D969-4EF3-B059-BB8B0C3879B6",
                                    windingRule: 1,
                                    innerShadows: [],
                                    borderOptions: {
                                        _class: "borderOptions",
                                        isEnabled: !0,
                                        dashPattern: [],
                                        lineCapStyle: 0,
                                        lineJoinStyle: 0
                                    },
                                    colorControls: {
                                        hue: 0,
                                        _class: "colorControls",
                                        contrast: 1,
                                        isEnabled: !1,
                                        brightness: 0,
                                        saturation: 1
                                    },
                                    endMarkerType: 0,
                                    startMarkerType: 0
                                },
                                maskId: null,
                                hasClippingMask: !1
                            },
                            top: .5398507700456285,
                            blur: {
                                type: 0,
                                _class: "blur",
                                center: [.5, .5],
                                radius: 10,
                                isEnabled: !1,
                                saturation: 1,
                                motionAngle: 0
                            },
                            href: "https://twitter.com/befamous",
                            left: .7707725299142112,
                            fills: [{
                                color: {
                                    red: .8,
                                    blue: .8,
                                    alpha: 1,
                                    green: .8,
                                    _class: "color"
                                },
                                _class: "fill",
                                fillType: 0,
                                isEnabled: !1
                            }],
                            right: 152.2292274700858,
                            width: 49,
                            _class: "style",
                            bottom: 33.46014922995437,
                            height: 49,
                            scaleX: 1,
                            scaleY: 1,
                            borders: [{
                                color: {
                                    red: .533,
                                    blue: .533,
                                    alpha: 1,
                                    green: .533,
                                    _class: "color"
                                },
                                _class: "border",
                                fillType: 0,
                                position: 1,
                                isEnabled: !1,
                                thickness: 1
                            }],
                            opacity: 1,
                            shadows: [],
                            blendMode: "normal",
                            miterLimit: 10,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            do_objectID: "12E83AF1-F650-4264-9334-E95C7EAFC4FE",
                            rotateAngle: 0,
                            targetBlank: !0,
                            windingRule: 1,
                            innerShadows: [],
                            borderOptions: {
                                _class: "borderOptions",
                                isEnabled: !0,
                                dashPattern: [],
                                lineCapStyle: 0,
                                lineJoinStyle: 0
                            },
                            colorControls: {
                                hue: 0,
                                _class: "colorControls",
                                contrast: 1,
                                isEnabled: !1,
                                brightness: 0,
                                saturation: 1
                            },
                            endMarkerType: 0,
                            startMarkerType: 0
                        },
                        name: "Twitter",
                        type: t[21],
                        frame: {
                            x: 0,
                            y: 0,
                            top: 0,
                            left: 0,
                            right: 151,
                            width: 49,
                            bottom: 32,
                            height: 49,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        isLink: "external",
                        maskId: null,
                        content: [{
                            $id: "B3CA3D2A-923F-43C2-B134-14D713016B49",
                            name: "Oval",
                            type: t[27],
                            frame: {
                                top: 0,
                                left: 0,
                                right: 0,
                                width: 49,
                                bottom: 0,
                                height: 49,
                                responsive: {
                                    x: {
                                        pos: "left",
                                        left: "vw",
                                        right: "vw"
                                    },
                                    y: {
                                        pos: "top",
                                        top: "vw",
                                        bottom: "vw"
                                    },
                                    scaling: "vw"
                                }
                            },
                            maskId: null,
                            hasClippingMask: !1
                        }, {
                            $id: "936CAE08-3BDC-451B-81CB-BA99B478549D",
                            name: "icon",
                            type: t[27],
                            frame: {
                                top: 16,
                                left: 14,
                                right: 13,
                                width: 22,
                                bottom: 15,
                                height: 18,
                                scaleX: 1,
                                scaleY: 1,
                                responsive: {
                                    x: {
                                        pos: "left",
                                        left: "vw",
                                        right: "vw"
                                    },
                                    y: {
                                        pos: "top",
                                        top: "vw",
                                        bottom: "vw"
                                    },
                                    scaling: "vw"
                                },
                                rotateAngle: 0
                            },
                            maskId: null,
                            hasClippingMask: !1
                        }],
                        hasClippingMask: !1
                    }, {
                        $id: "A37778BE-B939-4C94-A371-F29D96EBDCD3",
                        link: {
                            0: {
                                $id: "C5B4529C-0D62-4C75-8688-F1DBB637BC35",
                                name: "Oval",
                                type: "svg",
                                frame: {
                                    top: -3.552713678800501e-15,
                                    left: 0,
                                    right: .47857459937358726,
                                    width: 48.52142540062641,
                                    bottom: .4785745993734736,
                                    height: 48.52142540062653,
                                    responsive: {
                                        x: {
                                            pos: "left",
                                            left: "vw",
                                            right: "vw"
                                        },
                                        y: {
                                            pos: "top",
                                            top: "vw",
                                            bottom: "vw"
                                        },
                                        scaling: "vw"
                                    }
                                },
                                layer: {
                                    name: "Oval",
                                    frame: {
                                        x: 0,
                                        y: -3.552713678800501e-15,
                                        top: -3.552713678800501e-15,
                                        left: 0,
                                        right: .47857459937358726,
                                        width: 48.52142540062641,
                                        _class: "rect",
                                        bottom: .4785745993734736,
                                        height: 48.52142540062653,
                                        constrainProportions: !0
                                    },
                                    style: {
                                        blur: {
                                            type: 0,
                                            _class: "blur",
                                            center: [.5, .5],
                                            radius: 10,
                                            isEnabled: !1,
                                            saturation: 1,
                                            motionAngle: 0
                                        },
                                        fills: [{
                                            color: {
                                                red: 1,
                                                blue: 1,
                                                alpha: 1,
                                                green: 1,
                                                _class: "color"
                                            },
                                            _class: "fill",
                                            opacity: 1,
                                            fillType: 0,
                                            gradient: {
                                                to: [.5, 1],
                                                from: [.5, 0],
                                                stops: [{
                                                    color: {
                                                        red: 1,
                                                        blue: 1,
                                                        alpha: 1,
                                                        green: 1,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 0
                                                }, {
                                                    color: {
                                                        red: 0,
                                                        blue: 0,
                                                        alpha: 1,
                                                        green: 0,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 1
                                                }],
                                                _class: "gradient",
                                                elipseLength: 0,
                                                gradientType: 0
                                            },
                                            blendMode: "normal",
                                            isEnabled: !0,
                                            noiseIndex: 0,
                                            noiseIntensity: 0,
                                            patternFillType: 1,
                                            patternTileScale: 1
                                        }],
                                        _class: "style",
                                        borders: [{
                                            color: {
                                                red: .592,
                                                blue: .592,
                                                alpha: 1,
                                                green: .592,
                                                _class: "color"
                                            },
                                            _class: "border",
                                            fillType: 0,
                                            gradient: {
                                                to: [.5, 1],
                                                from: [.5, 0],
                                                stops: [{
                                                    color: {
                                                        red: 1,
                                                        blue: 1,
                                                        alpha: 1,
                                                        green: 1,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 0
                                                }, {
                                                    color: {
                                                        red: 0,
                                                        blue: 0,
                                                        alpha: 1,
                                                        green: 0,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 1
                                                }],
                                                _class: "gradient",
                                                elipseLength: 0,
                                                gradientType: 0
                                            },
                                            position: 0,
                                            isEnabled: !1,
                                            thickness: 1,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            }
                                        }],
                                        shadows: [],
                                        miterLimit: 10,
                                        do_objectID: "603FFAF0-DEBA-4640-9CF9-BE9068AE3282",
                                        windingRule: 0,
                                        innerShadows: [],
                                        borderOptions: {
                                            _class: "borderOptions",
                                            isEnabled: !0,
                                            dashPattern: [],
                                            lineCapStyle: 0,
                                            lineJoinStyle: 0
                                        },
                                        colorControls: {
                                            hue: 0,
                                            _class: "colorControls",
                                            contrast: 1,
                                            isEnabled: !1,
                                            brightness: 0,
                                            saturation: 1
                                        },
                                        endMarkerType: 0,
                                        contextSettings: {
                                            _class: "graphicsContextSettings",
                                            opacity: .2,
                                            blendMode: 0
                                        },
                                        startMarkerType: 0
                                    },
                                    _class: "oval",
                                    edited: !1,
                                    points: [{
                                        point: [.5, 1],
                                        _class: "curvePoint",
                                        curveTo: [.2238576251, 1],
                                        curveFrom: [.7761423749, 1],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }, {
                                        point: [1, .5],
                                        _class: "curvePoint",
                                        curveTo: [1, .7761423749],
                                        curveFrom: [1, .2238576251],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }, {
                                        point: [.5, 0],
                                        _class: "curvePoint",
                                        curveTo: [.7761423749, 0],
                                        curveFrom: [.2238576251, 0],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }, {
                                        point: [0, .5],
                                        _class: "curvePoint",
                                        curveTo: [0, .2238576251],
                                        curveFrom: [0, .7761423749],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }],
                                    isClosed: !0,
                                    isLocked: !1,
                                    rotation: 0,
                                    isVisible: !0,
                                    do_objectID: "C5B4529C-0D62-4C75-8688-F1DBB637BC35",
                                    nameIsFixed: !1,
                                    resizingType: 0,
                                    exportOptions: {
                                        _class: "exportOptions",
                                        shouldTrim: !1,
                                        layerOptions: 0,
                                        exportFormats: [],
                                        includedLayerIds: []
                                    },
                                    hasClippingMask: !1,
                                    booleanOperation: -1,
                                    clippingMaskMode: 0,
                                    isFixedToViewport: !1,
                                    isFlippedVertical: !1,
                                    resizingConstraint: 63,
                                    isFlippedHorizontal: !1,
                                    pointRadiusBehaviour: 1,
                                    shouldBreakMaskChain: !1,
                                    layerListExpandedType: 0
                                },
                                style: {
                                    blur: {
                                        type: 0,
                                        _class: "blur",
                                        center: [.5, .5],
                                        radius: 10,
                                        isEnabled: !1,
                                        saturation: 1,
                                        motionAngle: 0
                                    },
                                    fills: [{
                                        color: {
                                            red: 1,
                                            blue: 1,
                                            alpha: 1,
                                            green: 1,
                                            _class: "color"
                                        },
                                        _class: "fill",
                                        opacity: 1,
                                        fillType: 0,
                                        gradient: {
                                            to: [.5, 1],
                                            from: [.5, 0],
                                            stops: [{
                                                color: {
                                                    red: 1,
                                                    blue: 1,
                                                    alpha: 1,
                                                    green: 1,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 0
                                            }, {
                                                color: {
                                                    red: 0,
                                                    blue: 0,
                                                    alpha: 1,
                                                    green: 0,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 1
                                            }],
                                            _class: "gradient",
                                            elipseLength: 0,
                                            gradientType: 0
                                        },
                                        blendMode: "normal",
                                        isEnabled: !0,
                                        noiseIndex: 0,
                                        noiseIntensity: 0,
                                        patternFillType: 1,
                                        patternTileScale: 1
                                    }],
                                    _class: "style",
                                    borders: [{
                                        color: {
                                            red: .592,
                                            blue: .592,
                                            alpha: 1,
                                            green: .592,
                                            _class: "color"
                                        },
                                        _class: "border",
                                        fillType: 0,
                                        gradient: {
                                            to: [.5, 1],
                                            from: [.5, 0],
                                            stops: [{
                                                color: {
                                                    red: 1,
                                                    blue: 1,
                                                    alpha: 1,
                                                    green: 1,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 0
                                            }, {
                                                color: {
                                                    red: 0,
                                                    blue: 0,
                                                    alpha: 1,
                                                    green: 0,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 1
                                            }],
                                            _class: "gradient",
                                            elipseLength: 0,
                                            gradientType: 0
                                        },
                                        position: 0,
                                        isEnabled: !1,
                                        thickness: 1,
                                        contextSettings: {
                                            _class: "graphicsContextSettings",
                                            opacity: 1,
                                            blendMode: 0
                                        }
                                    }],
                                    opacity: .2,
                                    shadows: [],
                                    blendMode: "normal",
                                    miterLimit: 10,
                                    do_objectID: "603FFAF0-DEBA-4640-9CF9-BE9068AE3282",
                                    windingRule: 0,
                                    innerShadows: [],
                                    borderOptions: {
                                        _class: "borderOptions",
                                        isEnabled: !0,
                                        dashPattern: [],
                                        lineCapStyle: 0,
                                        lineJoinStyle: 0
                                    },
                                    colorControls: {
                                        hue: 0,
                                        _class: "colorControls",
                                        contrast: 1,
                                        isEnabled: !1,
                                        brightness: 0,
                                        saturation: 1
                                    },
                                    endMarkerType: 0,
                                    startMarkerType: 0
                                },
                                maskId: null,
                                hasClippingMask: !1
                            },
                            1: {
                                $id: "9978C166-EE69-4E5E-977F-9122D54491CF",
                                name: "icon",
                                type: "svg",
                                frame: {
                                    top: 12.86071270031326,
                                    left: 12.78129200234549,
                                    right: 13.265687863426372,
                                    width: 22.95302013422814,
                                    bottom: 13.339287299686738,
                                    height: 22.8,
                                    responsive: {
                                        x: {
                                            pos: "left",
                                            left: "vw",
                                            right: "vw"
                                        },
                                        y: {
                                            pos: "top",
                                            top: "vw",
                                            bottom: "vw"
                                        },
                                        scaling: "vw"
                                    }
                                },
                                layer: {
                                    name: "icon",
                                    frame: {
                                        x: 12.78129200234549,
                                        y: 12.86071270031326,
                                        top: 12.86071270031326,
                                        left: 12.78129200234549,
                                        right: 13.265687863426372,
                                        width: 22.95302013422814,
                                        _class: "rect",
                                        bottom: 13.339287299686738,
                                        height: 22.8,
                                        constrainProportions: !1
                                    },
                                    style: {
                                        blur: {
                                            type: 0,
                                            _class: "blur",
                                            center: [.5, .5],
                                            radius: 10,
                                            isEnabled: !1,
                                            saturation: 1,
                                            motionAngle: 0
                                        },
                                        fills: [{
                                            color: {
                                                red: 1,
                                                blue: 1,
                                                alpha: 1,
                                                green: 1,
                                                _class: "color"
                                            },
                                            _class: "fill",
                                            opacity: 1,
                                            fillType: 0,
                                            gradient: {
                                                to: [.5, 1],
                                                from: [.5, 0],
                                                stops: [{
                                                    color: {
                                                        red: 1,
                                                        blue: 1,
                                                        alpha: 1,
                                                        green: 1,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 0
                                                }, {
                                                    color: {
                                                        red: 0,
                                                        blue: 0,
                                                        alpha: 1,
                                                        green: 0,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 1
                                                }],
                                                _class: "gradient",
                                                elipseLength: 0,
                                                gradientType: 0
                                            },
                                            blendMode: "normal",
                                            isEnabled: !0,
                                            noiseIndex: 0,
                                            noiseIntensity: 0,
                                            patternFillType: 1,
                                            patternTileScale: 1
                                        }],
                                        _class: "style",
                                        borders: [{
                                            color: {
                                                red: .533,
                                                blue: .533,
                                                alpha: 1,
                                                green: .533,
                                                _class: "color"
                                            },
                                            _class: "border",
                                            fillType: 0,
                                            position: 1,
                                            isEnabled: !1,
                                            thickness: 1
                                        }],
                                        shadows: [],
                                        miterLimit: 10,
                                        do_objectID: "E03C63FD-7452-4111-AAA7-337B47A8A7B0",
                                        windingRule: 0,
                                        innerShadows: [],
                                        borderOptions: {
                                            _class: "borderOptions",
                                            isEnabled: !0,
                                            dashPattern: [],
                                            lineCapStyle: 0,
                                            lineJoinStyle: 0
                                        },
                                        colorControls: {
                                            hue: 0,
                                            _class: "colorControls",
                                            contrast: 1,
                                            isEnabled: !1,
                                            brightness: 0,
                                            saturation: 1
                                        },
                                        endMarkerType: 0,
                                        contextSettings: {
                                            _class: "graphicsContextSettings",
                                            opacity: 1,
                                            blendMode: 0
                                        },
                                        startMarkerType: 0
                                    },
                                    _class: "shapeGroup",
                                    layers: [{
                                        name: "Path",
                                        frame: {
                                            x: 0,
                                            y: 0,
                                            top: 0,
                                            left: 0,
                                            right: 0,
                                            width: 22.95302013422814,
                                            _class: "rect",
                                            bottom: 0,
                                            height: 22.8,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 10,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .592,
                                                    blue: .592,
                                                    alpha: 1,
                                                    green: .592,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                gradient: {
                                                    to: [.5, 1],
                                                    from: [.5, 0],
                                                    stops: [{
                                                        color: {
                                                            red: 1,
                                                            blue: 1,
                                                            alpha: 1,
                                                            green: 1,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 0
                                                    }, {
                                                        color: {
                                                            red: 0,
                                                            blue: 0,
                                                            alpha: 1,
                                                            green: 0,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 1
                                                    }],
                                                    _class: "gradient",
                                                    elipseLength: 0,
                                                    gradientType: 0
                                                },
                                                isEnabled: !0,
                                                noiseIndex: 0,
                                                noiseIntensity: 0,
                                                contextSettings: {
                                                    _class: "graphicsContextSettings",
                                                    opacity: 1,
                                                    blendMode: 0
                                                },
                                                patternFillType: 1,
                                                patternTileScale: 1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "2C1C74C0-AA39-4AC1-B010-4327FC873465",
                                            windingRule: 0,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [.7333333333333333, 0],
                                            _class: "curvePoint",
                                            curveTo: [.7333333333333333, 0],
                                            curveFrom: [.8788559817014477, 0],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.9999493930203305, .26317331098517976],
                                            _class: "curvePoint",
                                            curveTo: [.997153411506875, .11734733287737015],
                                            curveFrom: [.9999493930203305, .26317331098517976],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [1, .26845637583892556],
                                            _class: "curvePoint",
                                            curveTo: [1, .26845637583892556],
                                            curveFrom: [1, .26845637583892556],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [1, .7315436241610744],
                                            _class: "curvePoint",
                                            curveTo: [1, .7315436241610744],
                                            curveFrom: [1, .8780429345987733],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.7385811777547204, .9999490533761711],
                                            _class: "curvePoint",
                                            curveTo: [.8834349826751454, .9971343068861157],
                                            curveFrom: [.7385811777547204, .9999490533761711],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.7333333333333333, 1],
                                            _class: "curvePoint",
                                            curveTo: [.7333333333333333, 1],
                                            curveFrom: [.7333333333333333, 1],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.26666666666666666, 1],
                                            _class: "curvePoint",
                                            curveTo: [.26666666666666666, 1],
                                            curveFrom: [.12114401829855267, 1],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [5060697966901747e-20, .7368266890148212],
                                            _class: "curvePoint",
                                            curveTo: [.0028465884931250683, .8826526671226308],
                                            curveFrom: [5060697966901747e-20, .7368266890148212],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [0, .7315436241610744],
                                            _class: "curvePoint",
                                            curveTo: [0, .7315436241610744],
                                            curveFrom: [0, .7315436241610744],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [0, .26845637583892556],
                                            _class: "curvePoint",
                                            curveTo: [0, .26845637583892556],
                                            curveFrom: [0, .12195706540122678],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.2614188222452787, 5094662382691607e-20],
                                            _class: "curvePoint",
                                            curveTo: [.11656501732485462, .0028656931138842903],
                                            curveFrom: [.2614188222452787, 5094662382691607e-20],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.26666666666666666, 0],
                                            _class: "curvePoint",
                                            curveTo: [.26666666666666666, 0],
                                            curveFrom: [.26666666666666666, 0],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }],
                                        isClosed: !0,
                                        isLocked: !1,
                                        rotation: 0,
                                        isVisible: !0,
                                        do_objectID: "619957C4-A56B-4634-B6A4-546F15D42A93",
                                        nameIsFixed: !1,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: -1,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 0
                                    }, {
                                        name: "Path",
                                        frame: {
                                            x: 2.295302013422813,
                                            y: 2.295302013422814,
                                            top: 2.295302013422814,
                                            left: 2.295302013422813,
                                            right: 2.295302013422816,
                                            width: 18.36241610738251,
                                            _class: "rect",
                                            bottom: 2.295302013422809,
                                            height: 18.20939597315438,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 10,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .592,
                                                    blue: .592,
                                                    alpha: 1,
                                                    green: .592,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                gradient: {
                                                    to: [.5, 1],
                                                    from: [.5, 0],
                                                    stops: [{
                                                        color: {
                                                            red: 1,
                                                            blue: 1,
                                                            alpha: 1,
                                                            green: 1,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 0
                                                    }, {
                                                        color: {
                                                            red: 0,
                                                            blue: 0,
                                                            alpha: 1,
                                                            green: 0,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 1
                                                    }],
                                                    _class: "gradient",
                                                    elipseLength: 0,
                                                    gradientType: 0
                                                },
                                                isEnabled: !0,
                                                noiseIndex: 0,
                                                noiseIntensity: 0,
                                                contextSettings: {
                                                    _class: "graphicsContextSettings",
                                                    opacity: 1,
                                                    blendMode: 0
                                                },
                                                patternFillType: 1,
                                                patternTileScale: 1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "003F8451-5D41-4775-B39D-750EB1C94414",
                                            windingRule: 0,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [.7916666666666666, 0],
                                            _class: "curvePoint",
                                            curveTo: [.7916666666666666, 0],
                                            curveFrom: [.7916666666666666, 0],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.20833333333333334, 0],
                                            _class: "curvePoint",
                                            curveTo: [.20833333333333334, 0],
                                            curveFrom: [.09522416846685704, 0],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [8003744857987272e-20, .204203679748201],
                                            _class: "curvePoint",
                                            curveTo: [.003168089211650719, .09089644514212876],
                                            curveFrom: [8003744857987272e-20, .204203679748201],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [0, .21008403361344477],
                                            _class: "curvePoint",
                                            curveTo: [0, .21008403361344477],
                                            curveFrom: [0, .21008403361344477],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [0, .7899159663865553],
                                            _class: "curvePoint",
                                            curveTo: [0, .7899159663865553],
                                            curveFrom: [0, .9039756284367837],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.20250198241696596, .9999192899678186],
                                            _class: "curvePoint",
                                            curveTo: [.09013897476594461, .9968052881899333],
                                            curveFrom: [.20250198241696596, .9999192899678186],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.20833333333333334, 1],
                                            _class: "curvePoint",
                                            curveTo: [.20833333333333334, 1],
                                            curveFrom: [.20833333333333334, 1],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.7916666666666666, 1],
                                            _class: "curvePoint",
                                            curveTo: [.7916666666666666, 1],
                                            curveFrom: [.904775831533143, 1],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.9999199625514196, .7957963202518002],
                                            _class: "curvePoint",
                                            curveTo: [.9968319107883499, .9091035548578712],
                                            curveFrom: [.9999199625514196, .7957963202518002],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [1, .7899159663865553],
                                            _class: "curvePoint",
                                            curveTo: [1, .7899159663865553],
                                            curveFrom: [1, .7899159663865553],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [1, .21008403361344477],
                                            _class: "curvePoint",
                                            curveTo: [1, .21008403361344477],
                                            curveFrom: [1, .0960243715632175],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.7974980175830341, 8071003218138403e-20],
                                            _class: "curvePoint",
                                            curveTo: [.9098610252340554, .0031947118100679427],
                                            curveFrom: [.7974980175830341, 8071003218138403e-20],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }],
                                        isClosed: !0,
                                        isLocked: !1,
                                        rotation: 0,
                                        isVisible: !0,
                                        do_objectID: "706B98A2-C3CB-4881-A4B8-01F4A6D09D62",
                                        nameIsFixed: !1,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: -1,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 0
                                    }, {
                                        name: "Path",
                                        frame: {
                                            x: 5.593009331288744,
                                            y: 5.516499264174677,
                                            top: 5.516499264174677,
                                            left: 5.593009331288744,
                                            right: 5.593009331288748,
                                            width: 11.76700147165065,
                                            _class: "rect",
                                            bottom: 5.516499264174673,
                                            height: 11.76700147165065,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 10,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .592,
                                                    blue: .592,
                                                    alpha: 1,
                                                    green: .592,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                gradient: {
                                                    to: [.5, 1],
                                                    from: [.5, 0],
                                                    stops: [{
                                                        color: {
                                                            red: 1,
                                                            blue: 1,
                                                            alpha: 1,
                                                            green: 1,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 0
                                                    }, {
                                                        color: {
                                                            red: 0,
                                                            blue: 0,
                                                            alpha: 1,
                                                            green: 0,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 1
                                                    }],
                                                    _class: "gradient",
                                                    elipseLength: 0,
                                                    gradientType: 0
                                                },
                                                isEnabled: !0,
                                                noiseIndex: 0,
                                                noiseIntensity: 0,
                                                contextSettings: {
                                                    _class: "graphicsContextSettings",
                                                    opacity: 1,
                                                    blendMode: 0
                                                },
                                                patternFillType: 1,
                                                patternTileScale: 1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "9AC16A62-8D21-40C3-A6B9-3E3FD4E05E19",
                                            windingRule: 0,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [.5, 0],
                                            _class: "curvePoint",
                                            curveTo: [.2238576250969966, 0],
                                            curveFrom: [.7761423749030034, 0],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [1, .5],
                                            _class: "curvePoint",
                                            curveTo: [1, .2238576250969966],
                                            curveFrom: [1, .7761423749030034],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.5, 1],
                                            _class: "curvePoint",
                                            curveTo: [.7761423749030034, 1],
                                            curveFrom: [.2238576250969966, 1],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [0, .5],
                                            _class: "curvePoint",
                                            curveTo: [0, .7761423749030034],
                                            curveFrom: [0, .2238576250969966],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }],
                                        isClosed: !0,
                                        isLocked: !1,
                                        rotation: 0,
                                        isVisible: !0,
                                        do_objectID: "15F11875-D7D1-46BF-88A4-46D81ED693CE",
                                        nameIsFixed: !1,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: -1,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 0
                                    }, {
                                        name: "Path",
                                        frame: {
                                            x: 7.888311344711557,
                                            y: 7.811801277597491,
                                            top: 7.811801277597491,
                                            left: 7.888311344711557,
                                            right: 7.8883113447115605,
                                            width: 7.176397444805023,
                                            _class: "rect",
                                            bottom: 7.811801277597489,
                                            height: 7.176397444805021,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 10,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .592,
                                                    blue: .592,
                                                    alpha: 1,
                                                    green: .592,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                gradient: {
                                                    to: [.5, 1],
                                                    from: [.5, 0],
                                                    stops: [{
                                                        color: {
                                                            red: 1,
                                                            blue: 1,
                                                            alpha: 1,
                                                            green: 1,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 0
                                                    }, {
                                                        color: {
                                                            red: 0,
                                                            blue: 0,
                                                            alpha: 1,
                                                            green: 0,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 1
                                                    }],
                                                    _class: "gradient",
                                                    elipseLength: 0,
                                                    gradientType: 0
                                                },
                                                isEnabled: !0,
                                                noiseIndex: 0,
                                                noiseIntensity: 0,
                                                contextSettings: {
                                                    _class: "graphicsContextSettings",
                                                    opacity: 1,
                                                    blendMode: 0
                                                },
                                                patternFillType: 1,
                                                patternTileScale: 1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "A9D271FF-E897-46BC-B565-9EAE9C71396E",
                                            windingRule: 0,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [.5, 0],
                                            _class: "curvePoint",
                                            curveTo: [.7761423748950753, 0],
                                            curveFrom: [.22385762510492468, 0],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [0, .5],
                                            _class: "curvePoint",
                                            curveTo: [0, .22385762510492924],
                                            curveFrom: [0, .7761423748950769],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.5, 1],
                                            _class: "curvePoint",
                                            curveTo: [.22385762510492468, 1],
                                            curveFrom: [.7761423748950753, 1],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [1, .5],
                                            _class: "curvePoint",
                                            curveTo: [1, .7761423748950769],
                                            curveFrom: [1, .22385762510492924],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }],
                                        isClosed: !0,
                                        isLocked: !1,
                                        rotation: 0,
                                        isVisible: !0,
                                        do_objectID: "B990D0BE-60E6-4734-9657-4FB89D0D5C04",
                                        nameIsFixed: !1,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: -1,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 0
                                    }, {
                                        name: "Path",
                                        frame: {
                                            x: 16.01430182638896,
                                            y: 4.001780322497287,
                                            top: 4.001780322497287,
                                            left: 16.01430182638896,
                                            right: 4.0782903896114036,
                                            width: 2.860427918227778,
                                            _class: "rect",
                                            bottom: 15.937791759274937,
                                            height: 2.860427918227776,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 10,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .592,
                                                    blue: .592,
                                                    alpha: 1,
                                                    green: .592,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                gradient: {
                                                    to: [.5, 1],
                                                    from: [.5, 0],
                                                    stops: [{
                                                        color: {
                                                            red: 1,
                                                            blue: 1,
                                                            alpha: 1,
                                                            green: 1,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 0
                                                    }, {
                                                        color: {
                                                            red: 0,
                                                            blue: 0,
                                                            alpha: 1,
                                                            green: 0,
                                                            _class: "color"
                                                        },
                                                        _class: "gradientStop",
                                                        position: 1
                                                    }],
                                                    _class: "gradient",
                                                    elipseLength: 0,
                                                    gradientType: 0
                                                },
                                                isEnabled: !0,
                                                noiseIndex: 0,
                                                noiseIntensity: 0,
                                                contextSettings: {
                                                    _class: "graphicsContextSettings",
                                                    opacity: 1,
                                                    blendMode: 0
                                                },
                                                patternFillType: 1,
                                                patternTileScale: 1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "E048D073-E130-4F53-976F-03A6A49843E3",
                                            windingRule: 0,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [.5, 0],
                                            _class: "curvePoint",
                                            curveTo: [.22385762510000162, 0],
                                            curveFrom: [.7761423748999984, 0],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [1, .5],
                                            _class: "curvePoint",
                                            curveTo: [1, .22385762510000162],
                                            curveFrom: [1, .7761423748999984],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.5, 1],
                                            _class: "curvePoint",
                                            curveTo: [.7761423748999984, 1],
                                            curveFrom: [.22385762510000162, 1],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [0, .5],
                                            _class: "curvePoint",
                                            curveTo: [0, .7761423748999984],
                                            curveFrom: [0, .22385762510000162],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }],
                                        isClosed: !0,
                                        isLocked: !1,
                                        rotation: 0,
                                        isVisible: !0,
                                        do_objectID: "4C55A98F-38A1-47BC-A29B-200FBD240C2C",
                                        nameIsFixed: !1,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: -1,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 0
                                    }],
                                    isLocked: !1,
                                    rotation: 0,
                                    isVisible: !0,
                                    do_objectID: "9978C166-EE69-4E5E-977F-9122D54491CF",
                                    groupLayout: {
                                        _class: "MSImmutableFreeformGroupLayout"
                                    },
                                    nameIsFixed: !0,
                                    windingRule: 0,
                                    resizingType: 0,
                                    exportOptions: {
                                        _class: "exportOptions",
                                        shouldTrim: !1,
                                        layerOptions: 0,
                                        exportFormats: [],
                                        includedLayerIds: []
                                    },
                                    hasClickThrough: !1,
                                    hasClippingMask: !1,
                                    booleanOperation: -1,
                                    clippingMaskMode: 0,
                                    isFixedToViewport: !1,
                                    isFlippedVertical: !1,
                                    resizingConstraint: 63,
                                    isFlippedHorizontal: !1,
                                    shouldBreakMaskChain: !1,
                                    layerListExpandedType: 1
                                },
                                style: {
                                    blur: {
                                        type: 0,
                                        _class: "blur",
                                        center: [.5, .5],
                                        radius: 10,
                                        isEnabled: !1,
                                        saturation: 1,
                                        motionAngle: 0
                                    },
                                    fills: [{
                                        color: {
                                            red: 1,
                                            blue: 1,
                                            alpha: 1,
                                            green: 1,
                                            _class: "color"
                                        },
                                        _class: "fill",
                                        opacity: 1,
                                        fillType: 0,
                                        gradient: {
                                            to: [.5, 1],
                                            from: [.5, 0],
                                            stops: [{
                                                color: {
                                                    red: 1,
                                                    blue: 1,
                                                    alpha: 1,
                                                    green: 1,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 0
                                            }, {
                                                color: {
                                                    red: 0,
                                                    blue: 0,
                                                    alpha: 1,
                                                    green: 0,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 1
                                            }],
                                            _class: "gradient",
                                            elipseLength: 0,
                                            gradientType: 0
                                        },
                                        blendMode: "normal",
                                        isEnabled: !0,
                                        noiseIndex: 0,
                                        noiseIntensity: 0,
                                        patternFillType: 1,
                                        patternTileScale: 1
                                    }],
                                    _class: "style",
                                    borders: [{
                                        color: {
                                            red: .533,
                                            blue: .533,
                                            alpha: 1,
                                            green: .533,
                                            _class: "color"
                                        },
                                        _class: "border",
                                        fillType: 0,
                                        position: 1,
                                        isEnabled: !1,
                                        thickness: 1
                                    }],
                                    opacity: 1,
                                    shadows: [],
                                    blendMode: "normal",
                                    miterLimit: 10,
                                    do_objectID: "E03C63FD-7452-4111-AAA7-337B47A8A7B0",
                                    windingRule: 0,
                                    innerShadows: [],
                                    borderOptions: {
                                        _class: "borderOptions",
                                        isEnabled: !0,
                                        dashPattern: [],
                                        lineCapStyle: 0,
                                        lineJoinStyle: 0
                                    },
                                    colorControls: {
                                        hue: 0,
                                        _class: "colorControls",
                                        contrast: 1,
                                        isEnabled: !1,
                                        brightness: 0,
                                        saturation: 1
                                    },
                                    endMarkerType: 0,
                                    startMarkerType: 0
                                },
                                maskId: null,
                                hasClippingMask: !1
                            },
                            top: .5398507700456285,
                            blur: {
                                type: 0,
                                _class: "blur",
                                center: [.5, .5],
                                radius: 10,
                                isEnabled: !1,
                                saturation: 1,
                                motionAngle: 0
                            },
                            href: "https://www.instagram.com/famous_industries/",
                            left: 76.64219793054053,
                            fills: [{
                                color: {
                                    red: .8,
                                    blue: .8,
                                    alpha: 1,
                                    green: .8,
                                    _class: "color"
                                },
                                _class: "fill",
                                fillType: 0,
                                isEnabled: !1
                            }],
                            right: 76.35780206945947,
                            width: 49,
                            _class: "style",
                            bottom: 33.46014922995437,
                            height: 49,
                            borders: [{
                                color: {
                                    red: .533,
                                    blue: .533,
                                    alpha: 1,
                                    green: .533,
                                    _class: "color"
                                },
                                _class: "border",
                                fillType: 0,
                                position: 1,
                                isEnabled: !1,
                                thickness: 1
                            }],
                            opacity: 1,
                            shadows: [],
                            blendMode: "normal",
                            miterLimit: 10,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            do_objectID: "4D8877D0-2F01-4E0F-9481-BBD5221F8D04",
                            targetBlank: !0,
                            windingRule: 1,
                            innerShadows: [],
                            borderOptions: {
                                _class: "borderOptions",
                                isEnabled: !0,
                                dashPattern: [],
                                lineCapStyle: 0,
                                lineJoinStyle: 0
                            },
                            colorControls: {
                                hue: 0,
                                _class: "colorControls",
                                contrast: 1,
                                isEnabled: !1,
                                brightness: 0,
                                saturation: 1
                            },
                            endMarkerType: 0,
                            startMarkerType: 0
                        },
                        name: "Insta",
                        type: t[21],
                        frame: {
                            x: 75,
                            y: 0,
                            top: 0,
                            left: 75,
                            right: 76,
                            width: 49,
                            bottom: 32,
                            height: 49,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        isLink: "external",
                        maskId: null,
                        content: [{
                            $id: "C5B4529C-0D62-4C75-8688-F1DBB637BC35",
                            name: "Oval",
                            type: t[27],
                            frame: {
                                top: 0,
                                left: .5,
                                right: .5,
                                width: 49,
                                bottom: 0,
                                height: 49,
                                scaleX: 1,
                                scaleY: 1,
                                responsive: {
                                    x: {
                                        pos: "left",
                                        left: "vw",
                                        right: "vw"
                                    },
                                    y: {
                                        pos: "top",
                                        top: "vw",
                                        bottom: "vw"
                                    },
                                    scaling: "vw"
                                },
                                rotateAngle: 0
                            },
                            maskId: null,
                            hasClippingMask: !1
                        }, {
                            $id: "9978C166-EE69-4E5E-977F-9122D54491CF",
                            name: "icon",
                            type: t[27],
                            frame: {
                                top: 12,
                                left: 13.5,
                                right: 13.5,
                                width: 23,
                                bottom: 14,
                                height: 23,
                                scaleX: 1,
                                scaleY: 1,
                                responsive: {
                                    x: {
                                        pos: "left",
                                        left: "vw",
                                        right: "vw"
                                    },
                                    y: {
                                        pos: "top",
                                        top: "vw",
                                        bottom: "vw"
                                    },
                                    scaling: "vw"
                                },
                                rotateAngle: 0
                            },
                            maskId: null,
                            hasClippingMask: !1
                        }],
                        hasClippingMask: !1
                    }, {
                        $id: "67C72AA1-43DB-4183-B252-57C013EFFE4B",
                        link: {
                            0: {
                                $id: "F13A0E9F-4CCA-47E5-8F2C-A384AABC082D",
                                name: "Oval",
                                type: "svg",
                                frame: {
                                    top: -3.552713678800501e-15,
                                    left: 0,
                                    right: .47857459937358726,
                                    width: 48.52142540062641,
                                    bottom: .4785745993734736,
                                    height: 48.52142540062653,
                                    responsive: {
                                        x: {
                                            pos: "left",
                                            left: "vw",
                                            right: "vw"
                                        },
                                        y: {
                                            pos: "top",
                                            top: "vw",
                                            bottom: "vw"
                                        },
                                        scaling: "vw"
                                    }
                                },
                                layer: {
                                    name: "Oval",
                                    frame: {
                                        x: 0,
                                        y: -3.552713678800501e-15,
                                        top: -3.552713678800501e-15,
                                        left: 0,
                                        right: .47857459937358726,
                                        width: 48.52142540062641,
                                        _class: "rect",
                                        bottom: .4785745993734736,
                                        height: 48.52142540062653,
                                        constrainProportions: !0
                                    },
                                    style: {
                                        blur: {
                                            type: 0,
                                            _class: "blur",
                                            center: [.5, .5],
                                            radius: 10,
                                            isEnabled: !1,
                                            saturation: 1,
                                            motionAngle: 0
                                        },
                                        fills: [{
                                            color: {
                                                red: 1,
                                                blue: 1,
                                                alpha: 1,
                                                green: 1,
                                                _class: "color"
                                            },
                                            _class: "fill",
                                            opacity: 1,
                                            fillType: 0,
                                            gradient: {
                                                to: [.5, 1],
                                                from: [.5, 0],
                                                stops: [{
                                                    color: {
                                                        red: 1,
                                                        blue: 1,
                                                        alpha: 1,
                                                        green: 1,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 0
                                                }, {
                                                    color: {
                                                        red: 0,
                                                        blue: 0,
                                                        alpha: 1,
                                                        green: 0,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 1
                                                }],
                                                _class: "gradient",
                                                elipseLength: 0,
                                                gradientType: 0
                                            },
                                            blendMode: "normal",
                                            isEnabled: !0,
                                            noiseIndex: 0,
                                            noiseIntensity: 0,
                                            patternFillType: 1,
                                            patternTileScale: 1
                                        }],
                                        _class: "style",
                                        borders: [{
                                            color: {
                                                red: .592,
                                                blue: .592,
                                                alpha: 1,
                                                green: .592,
                                                _class: "color"
                                            },
                                            _class: "border",
                                            fillType: 0,
                                            gradient: {
                                                to: [.5, 1],
                                                from: [.5, 0],
                                                stops: [{
                                                    color: {
                                                        red: 1,
                                                        blue: 1,
                                                        alpha: 1,
                                                        green: 1,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 0
                                                }, {
                                                    color: {
                                                        red: 0,
                                                        blue: 0,
                                                        alpha: 1,
                                                        green: 0,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 1
                                                }],
                                                _class: "gradient",
                                                elipseLength: 0,
                                                gradientType: 0
                                            },
                                            position: 0,
                                            isEnabled: !1,
                                            thickness: 1,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            }
                                        }],
                                        shadows: [],
                                        miterLimit: 10,
                                        do_objectID: "F1555363-8384-44BA-8EF0-5D60EA4EAC9D",
                                        windingRule: 0,
                                        innerShadows: [],
                                        borderOptions: {
                                            _class: "borderOptions",
                                            isEnabled: !0,
                                            dashPattern: [],
                                            lineCapStyle: 0,
                                            lineJoinStyle: 0
                                        },
                                        colorControls: {
                                            hue: 0,
                                            _class: "colorControls",
                                            contrast: 1,
                                            isEnabled: !1,
                                            brightness: 0,
                                            saturation: 1
                                        },
                                        endMarkerType: 0,
                                        contextSettings: {
                                            _class: "graphicsContextSettings",
                                            opacity: .2,
                                            blendMode: 0
                                        },
                                        startMarkerType: 0
                                    },
                                    _class: "oval",
                                    edited: !1,
                                    points: [{
                                        point: [.5, 1],
                                        _class: "curvePoint",
                                        curveTo: [.2238576251, 1],
                                        curveFrom: [.7761423749, 1],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }, {
                                        point: [1, .5],
                                        _class: "curvePoint",
                                        curveTo: [1, .7761423749],
                                        curveFrom: [1, .2238576251],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }, {
                                        point: [.5, 0],
                                        _class: "curvePoint",
                                        curveTo: [.7761423749, 0],
                                        curveFrom: [.2238576251, 0],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }, {
                                        point: [0, .5],
                                        _class: "curvePoint",
                                        curveTo: [0, .2238576251],
                                        curveFrom: [0, .7761423749],
                                        curveMode: 2,
                                        hasCurveTo: !0,
                                        cornerRadius: 0,
                                        hasCurveFrom: !0
                                    }],
                                    isClosed: !0,
                                    isLocked: !1,
                                    rotation: 0,
                                    isVisible: !0,
                                    do_objectID: "F13A0E9F-4CCA-47E5-8F2C-A384AABC082D",
                                    nameIsFixed: !1,
                                    resizingType: 0,
                                    exportOptions: {
                                        _class: "exportOptions",
                                        shouldTrim: !1,
                                        layerOptions: 0,
                                        exportFormats: [],
                                        includedLayerIds: []
                                    },
                                    hasClippingMask: !1,
                                    booleanOperation: -1,
                                    clippingMaskMode: 0,
                                    isFixedToViewport: !1,
                                    isFlippedVertical: !1,
                                    resizingConstraint: 63,
                                    isFlippedHorizontal: !1,
                                    pointRadiusBehaviour: 1,
                                    shouldBreakMaskChain: !1,
                                    layerListExpandedType: 0
                                },
                                style: {
                                    blur: {
                                        type: 0,
                                        _class: "blur",
                                        center: [.5, .5],
                                        radius: 10,
                                        isEnabled: !1,
                                        saturation: 1,
                                        motionAngle: 0
                                    },
                                    fills: [{
                                        color: {
                                            red: 1,
                                            blue: 1,
                                            alpha: 1,
                                            green: 1,
                                            _class: "color"
                                        },
                                        _class: "fill",
                                        opacity: 1,
                                        fillType: 0,
                                        gradient: {
                                            to: [.5, 1],
                                            from: [.5, 0],
                                            stops: [{
                                                color: {
                                                    red: 1,
                                                    blue: 1,
                                                    alpha: 1,
                                                    green: 1,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 0
                                            }, {
                                                color: {
                                                    red: 0,
                                                    blue: 0,
                                                    alpha: 1,
                                                    green: 0,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 1
                                            }],
                                            _class: "gradient",
                                            elipseLength: 0,
                                            gradientType: 0
                                        },
                                        blendMode: "normal",
                                        isEnabled: !0,
                                        noiseIndex: 0,
                                        noiseIntensity: 0,
                                        patternFillType: 1,
                                        patternTileScale: 1
                                    }],
                                    _class: "style",
                                    borders: [{
                                        color: {
                                            red: .592,
                                            blue: .592,
                                            alpha: 1,
                                            green: .592,
                                            _class: "color"
                                        },
                                        _class: "border",
                                        fillType: 0,
                                        gradient: {
                                            to: [.5, 1],
                                            from: [.5, 0],
                                            stops: [{
                                                color: {
                                                    red: 1,
                                                    blue: 1,
                                                    alpha: 1,
                                                    green: 1,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 0
                                            }, {
                                                color: {
                                                    red: 0,
                                                    blue: 0,
                                                    alpha: 1,
                                                    green: 0,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 1
                                            }],
                                            _class: "gradient",
                                            elipseLength: 0,
                                            gradientType: 0
                                        },
                                        position: 0,
                                        isEnabled: !1,
                                        thickness: 1,
                                        contextSettings: {
                                            _class: "graphicsContextSettings",
                                            opacity: 1,
                                            blendMode: 0
                                        }
                                    }],
                                    opacity: .2,
                                    shadows: [],
                                    blendMode: "normal",
                                    miterLimit: 10,
                                    do_objectID: "F1555363-8384-44BA-8EF0-5D60EA4EAC9D",
                                    windingRule: 0,
                                    innerShadows: [],
                                    borderOptions: {
                                        _class: "borderOptions",
                                        isEnabled: !0,
                                        dashPattern: [],
                                        lineCapStyle: 0,
                                        lineJoinStyle: 0
                                    },
                                    colorControls: {
                                        hue: 0,
                                        _class: "colorControls",
                                        contrast: 1,
                                        isEnabled: !1,
                                        brightness: 0,
                                        saturation: 1
                                    },
                                    endMarkerType: 0,
                                    startMarkerType: 0
                                },
                                maskId: null,
                                hasClippingMask: !1
                            },
                            1: {
                                $id: "05D28152-8045-464E-ADEC-628E1152FBF9",
                                name: "in",
                                type: "svg",
                                frame: {
                                    top: 13.66071270031333,
                                    left: 15.64370575627884,
                                    right: 16.13392287906793,
                                    width: 17.22237136465323,
                                    bottom: 18.1392872996867,
                                    height: 17.19999999999997,
                                    responsive: {
                                        x: {
                                            pos: "left",
                                            left: "vw",
                                            right: "vw"
                                        },
                                        y: {
                                            pos: "top",
                                            top: "vw",
                                            bottom: "vw"
                                        },
                                        scaling: "vw"
                                    }
                                },
                                layer: {
                                    name: "in",
                                    frame: {
                                        x: 15.64370575627884,
                                        y: 13.66071270031333,
                                        top: 13.66071270031333,
                                        left: 15.64370575627884,
                                        right: 16.13392287906793,
                                        width: 17.22237136465323,
                                        _class: "rect",
                                        bottom: 18.1392872996867,
                                        height: 17.19999999999997,
                                        constrainProportions: !1
                                    },
                                    style: {
                                        blur: {
                                            type: 0,
                                            _class: "blur",
                                            center: [.5, .5],
                                            radius: 13,
                                            isEnabled: !1,
                                            saturation: 1,
                                            motionAngle: 0
                                        },
                                        fills: [{
                                            color: {
                                                red: 1,
                                                blue: 1,
                                                alpha: 1,
                                                green: 1,
                                                _class: "color"
                                            },
                                            _class: "fill",
                                            opacity: 1,
                                            fillType: 0,
                                            gradient: {
                                                to: [.5, 1],
                                                from: [.5, 0],
                                                stops: [{
                                                    color: {
                                                        red: 1,
                                                        blue: 1,
                                                        alpha: 1,
                                                        green: 1,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 0
                                                }, {
                                                    color: {
                                                        red: 0,
                                                        blue: 0,
                                                        alpha: 1,
                                                        green: 0,
                                                        _class: "color"
                                                    },
                                                    _class: "gradientStop",
                                                    position: 1
                                                }],
                                                _class: "gradient",
                                                elipseLength: 0,
                                                gradientType: 0
                                            },
                                            blendMode: "normal",
                                            isEnabled: !0,
                                            noiseIndex: 0,
                                            noiseIntensity: 0,
                                            patternFillType: 1,
                                            patternTileScale: 1
                                        }],
                                        _class: "style",
                                        borders: [{
                                            color: {
                                                red: .533,
                                                blue: .533,
                                                alpha: 1,
                                                green: .533,
                                                _class: "color"
                                            },
                                            _class: "border",
                                            fillType: 0,
                                            position: 1,
                                            isEnabled: !1,
                                            thickness: 1
                                        }],
                                        shadows: [],
                                        miterLimit: 10,
                                        do_objectID: "AB5F06BA-A852-4B23-B60A-1D72B527044E",
                                        windingRule: 1,
                                        innerShadows: [],
                                        borderOptions: {
                                            _class: "borderOptions",
                                            isEnabled: !0,
                                            dashPattern: [],
                                            lineCapStyle: 0,
                                            lineJoinStyle: 0
                                        },
                                        colorControls: {
                                            hue: 0,
                                            _class: "colorControls",
                                            contrast: 1,
                                            isEnabled: !1,
                                            brightness: 0,
                                            saturation: 1
                                        },
                                        endMarkerType: 0,
                                        contextSettings: {
                                            _class: "graphicsContextSettings",
                                            opacity: 1,
                                            blendMode: 0
                                        },
                                        startMarkerType: 0
                                    },
                                    _class: "shapeGroup",
                                    layers: [{
                                        name: "Path",
                                        frame: {
                                            x: 5.948993288590327,
                                            y: 5.404026845637615,
                                            top: 5.404026845637615,
                                            left: 5.948993288590327,
                                            right: 2.7355895326763857e-13,
                                            width: 11.27337807606263,
                                            _class: "rect",
                                            bottom: -6.394884621840902e-14,
                                            height: 11.79597315436242,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 13,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .8,
                                                    blue: .8,
                                                    alpha: 1,
                                                    green: .8,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                isEnabled: !1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "030A7201-35B3-48EF-8CF9-3BE726CF9A3E",
                                            windingRule: 1,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [1, 1],
                                            _class: "curvePoint",
                                            curveTo: [1, 1],
                                            curveFrom: [1, 1],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.6724083187807589, 1],
                                            _class: "curvePoint",
                                            curveTo: [.6724083187807589, 1],
                                            curveFrom: [.6724083187807589, 1],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.6724083187807589, .46009710210893645],
                                            _class: "curvePoint",
                                            curveTo: [.6724083187807589, .46009710210893645],
                                            curveFrom: [.6724083187807589, .33348505537854645],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.5063502143197333, .24707935062964637],
                                            _class: "curvePoint",
                                            curveTo: [.6249404667407524, .24707935062964637],
                                            curveFrom: [.4160184156215273, .24707935062964637],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.3384664232417845, .3616294947655893],
                                            _class: "curvePoint",
                                            curveTo: [.3620415939037942, .3053406159915035],
                                            curveFrom: [.3296555008731545, .3817326657563343],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.3275916812192412, .43802154453042025],
                                            _class: "curvePoint",
                                            curveTo: [.3275916812192412, .4098771051433773],
                                            curveFrom: [.3275916812192412, .43802154453042025],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.3275916812192412, 1],
                                            _class: "curvePoint",
                                            curveTo: [.3275916812192412, 1],
                                            curveFrom: [.3275916812192412, 1],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [0, 1],
                                            _class: "curvePoint",
                                            curveTo: [0, 1],
                                            curveFrom: [0, 1],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [0, .02207555757851619],
                                            _class: "curvePoint",
                                            curveTo: [.004365772344816615, .11037778789258068],
                                            curveFrom: [0, .02207555757851619],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.3275916812192412, .02207555757851619],
                                            _class: "curvePoint",
                                            curveTo: [.3275916812192412, .02207555757851619],
                                            curveFrom: [.3275916812192412, .02207555757851619],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [.3275916812192412, .15559095736610518],
                                            _class: "curvePoint",
                                            curveTo: [.3275916812192412, .15559095736610518],
                                            curveFrom: [.3710112716304176, .09141253224093449],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.6227972694078426, 0],
                                            _class: "curvePoint",
                                            curveTo: [.4487220193681537, 0],
                                            curveFrom: [.8383076678837912, 0],
                                            curveMode: 3,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [1, .4239113943255955],
                                            _class: "curvePoint",
                                            curveTo: [1, .13457745410408128],
                                            curveFrom: [1, .4239113943255955],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }],
                                        isClosed: !0,
                                        isLocked: !1,
                                        rotation: 0,
                                        isVisible: !0,
                                        do_objectID: "4ED4F12F-2144-427F-9C01-D27BE61497F7",
                                        nameIsFixed: !1,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: -1,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 0
                                    }, {
                                        name: "Path",
                                        frame: {
                                            x: -2.767981145662201e-13,
                                            y: 6.919952864155505e-14,
                                            top: 6.919952864155505e-14,
                                            left: -2.767981145662201e-13,
                                            right: 13.10246085011212,
                                            width: 4.119910514541387,
                                            _class: "rect",
                                            bottom: 13.079194630872397,
                                            height: 4.120805369127505,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 13,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .8,
                                                    blue: .8,
                                                    alpha: 1,
                                                    green: .8,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                isEnabled: !1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "05BA6686-D71D-4E3C-9F00-A24A89AE0954",
                                            windingRule: 1,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [.5002172024326668, 1],
                                            _class: "curvePoint",
                                            curveTo: [.5002172024326668, 1],
                                            curveFrom: [.22393570807993027, 1],
                                            curveMode: 4,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [0, .49989142236699224],
                                            _class: "curvePoint",
                                            curveTo: [0, .7761129207383282],
                                            curveFrom: [0, .2238870792616718],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.5002172024326668, 0],
                                            _class: "curvePoint",
                                            curveTo: [.22393570807993027, 0],
                                            curveFrom: [.7762814943527364, 0],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [1, .49989142236699224],
                                            _class: "curvePoint",
                                            curveTo: [1, .2238870792616718],
                                            curveFrom: [1, .7761129207383282],
                                            curveMode: 2,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !0
                                        }, {
                                            point: [.5002172024326668, 1],
                                            _class: "curvePoint",
                                            curveTo: [.7762814943527364, 1],
                                            curveFrom: [.5002172024326668, 1],
                                            curveMode: 4,
                                            hasCurveTo: !0,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }],
                                        isClosed: !0,
                                        isLocked: !1,
                                        rotation: 0,
                                        isVisible: !0,
                                        do_objectID: "70247EE5-F7AE-4B6E-99A8-8D40F502C12B",
                                        nameIsFixed: !1,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: -1,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 0
                                    }, {
                                        name: "Path",
                                        frame: {
                                            x: .2129753914987214,
                                            y: 5.664429530201351,
                                            top: 5.664429530201351,
                                            left: .2129753914987214,
                                            right: 13.318120805369269,
                                            width: 3.691275167785241,
                                            _class: "rect",
                                            bottom: -3.907985046680551e-14,
                                            height: 11.53557046979866,
                                            constrainProportions: !1
                                        },
                                        style: {
                                            blur: {
                                                type: 0,
                                                _class: "blur",
                                                center: [.5, .5],
                                                radius: 13,
                                                isEnabled: !1,
                                                saturation: 1,
                                                motionAngle: 0
                                            },
                                            fills: [{
                                                color: {
                                                    red: .8,
                                                    blue: .8,
                                                    alpha: 1,
                                                    green: .8,
                                                    _class: "color"
                                                },
                                                _class: "fill",
                                                fillType: 0,
                                                isEnabled: !1
                                            }],
                                            _class: "style",
                                            borders: [{
                                                color: {
                                                    red: .533,
                                                    blue: .533,
                                                    alpha: 1,
                                                    green: .533,
                                                    _class: "color"
                                                },
                                                _class: "border",
                                                fillType: 0,
                                                position: 1,
                                                isEnabled: !1,
                                                thickness: 1
                                            }],
                                            shadows: [],
                                            miterLimit: 10,
                                            do_objectID: "B6A58804-068F-46A8-B46D-8D3C86572057",
                                            windingRule: 1,
                                            innerShadows: [],
                                            borderOptions: {
                                                _class: "borderOptions",
                                                isEnabled: !0,
                                                dashPattern: [],
                                                lineCapStyle: 0,
                                                lineJoinStyle: 0
                                            },
                                            colorControls: {
                                                hue: 0,
                                                _class: "colorControls",
                                                contrast: 1,
                                                isEnabled: !1,
                                                brightness: 0,
                                                saturation: 1
                                            },
                                            endMarkerType: 0,
                                            contextSettings: {
                                                _class: "graphicsContextSettings",
                                                opacity: 1,
                                                blendMode: 0
                                            },
                                            startMarkerType: 0
                                        },
                                        _class: "shapePath",
                                        edited: !0,
                                        points: [{
                                            point: [0, 1],
                                            _class: "curvePoint",
                                            curveTo: [0, 1],
                                            curveFrom: [0, 1],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [1, 1],
                                            _class: "curvePoint",
                                            curveTo: [1, 1],
                                            curveFrom: [1, 1],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [1, 0],
                                            _class: "curvePoint",
                                            curveTo: [1, 0],
                                            curveFrom: [1, 0],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }, {
                                            point: [0, 0],
                                            _class: "curvePoint",
                                            curveTo: [0, 0],
                                            curveFrom: [0, 0],
                                            curveMode: 1,
                                            hasCurveTo: !1,
                                            cornerRadius: 0,
                                            hasCurveFrom: !1
                                        }],
                                        isClosed: !0,
                                        isLocked: !1,
                                        rotation: 0,
                                        isVisible: !0,
                                        do_objectID: "069E610B-634B-4BD8-AC02-F69A2D35CA25",
                                        nameIsFixed: !1,
                                        resizingType: 0,
                                        exportOptions: {
                                            _class: "exportOptions",
                                            shouldTrim: !1,
                                            layerOptions: 0,
                                            exportFormats: [],
                                            includedLayerIds: []
                                        },
                                        hasClippingMask: !1,
                                        booleanOperation: -1,
                                        clippingMaskMode: 0,
                                        isFixedToViewport: !1,
                                        isFlippedVertical: !1,
                                        resizingConstraint: 63,
                                        isFlippedHorizontal: !1,
                                        pointRadiusBehaviour: 1,
                                        shouldBreakMaskChain: !1,
                                        layerListExpandedType: 0
                                    }],
                                    isLocked: !1,
                                    rotation: 0,
                                    isVisible: !0,
                                    do_objectID: "05D28152-8045-464E-ADEC-628E1152FBF9",
                                    groupLayout: {
                                        _class: "MSImmutableFreeformGroupLayout"
                                    },
                                    nameIsFixed: !0,
                                    windingRule: 1,
                                    resizingType: 0,
                                    exportOptions: {
                                        _class: "exportOptions",
                                        shouldTrim: !1,
                                        layerOptions: 0,
                                        exportFormats: [],
                                        includedLayerIds: []
                                    },
                                    hasClickThrough: !1,
                                    hasClippingMask: !1,
                                    booleanOperation: -1,
                                    clippingMaskMode: 0,
                                    isFixedToViewport: !1,
                                    isFlippedVertical: !1,
                                    resizingConstraint: 63,
                                    isFlippedHorizontal: !1,
                                    shouldBreakMaskChain: !1,
                                    layerListExpandedType: 1
                                },
                                style: {
                                    blur: {
                                        type: 0,
                                        _class: "blur",
                                        center: [.5, .5],
                                        radius: 13,
                                        isEnabled: !1,
                                        saturation: 1,
                                        motionAngle: 0
                                    },
                                    fills: [{
                                        color: {
                                            red: 1,
                                            blue: 1,
                                            alpha: 1,
                                            green: 1,
                                            _class: "color"
                                        },
                                        _class: "fill",
                                        opacity: 1,
                                        fillType: 0,
                                        gradient: {
                                            to: [.5, 1],
                                            from: [.5, 0],
                                            stops: [{
                                                color: {
                                                    red: 1,
                                                    blue: 1,
                                                    alpha: 1,
                                                    green: 1,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 0
                                            }, {
                                                color: {
                                                    red: 0,
                                                    blue: 0,
                                                    alpha: 1,
                                                    green: 0,
                                                    _class: "color"
                                                },
                                                _class: "gradientStop",
                                                position: 1
                                            }],
                                            _class: "gradient",
                                            elipseLength: 0,
                                            gradientType: 0
                                        },
                                        blendMode: "normal",
                                        isEnabled: !0,
                                        noiseIndex: 0,
                                        noiseIntensity: 0,
                                        patternFillType: 1,
                                        patternTileScale: 1
                                    }],
                                    _class: "style",
                                    borders: [{
                                        color: {
                                            red: .533,
                                            blue: .533,
                                            alpha: 1,
                                            green: .533,
                                            _class: "color"
                                        },
                                        _class: "border",
                                        fillType: 0,
                                        position: 1,
                                        isEnabled: !1,
                                        thickness: 1
                                    }],
                                    opacity: 1,
                                    shadows: [],
                                    blendMode: "normal",
                                    miterLimit: 10,
                                    do_objectID: "AB5F06BA-A852-4B23-B60A-1D72B527044E",
                                    windingRule: 1,
                                    innerShadows: [],
                                    borderOptions: {
                                        _class: "borderOptions",
                                        isEnabled: !0,
                                        dashPattern: [],
                                        lineCapStyle: 0,
                                        lineJoinStyle: 0
                                    },
                                    colorControls: {
                                        hue: 0,
                                        _class: "colorControls",
                                        contrast: 1,
                                        isEnabled: !1,
                                        brightness: 0,
                                        saturation: 1
                                    },
                                    endMarkerType: 0,
                                    startMarkerType: 0
                                },
                                maskId: null,
                                hasClippingMask: !1
                            },
                            top: .5398507700456285,
                            blur: {
                                type: 0,
                                _class: "blur",
                                center: [.5, .5],
                                radius: 10,
                                isEnabled: !1,
                                saturation: 1,
                                motionAngle: 0
                            },
                            href: "https://www.linkedin.com/company/famo-us/",
                            left: 152.5136233311671,
                            fills: [{
                                color: {
                                    red: .8,
                                    blue: .8,
                                    alpha: 1,
                                    green: .8,
                                    _class: "color"
                                },
                                _class: "fill",
                                fillType: 0,
                                isEnabled: !1
                            }],
                            right: .48637666883288944,
                            width: 49,
                            _class: "style",
                            bottom: 33.46014922995437,
                            height: 49,
                            borders: [{
                                color: {
                                    red: .533,
                                    blue: .533,
                                    alpha: 1,
                                    green: .533,
                                    _class: "color"
                                },
                                _class: "border",
                                fillType: 0,
                                position: 1,
                                isEnabled: !1,
                                thickness: 1
                            }],
                            opacity: 1,
                            shadows: [],
                            blendMode: "normal",
                            miterLimit: 10,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            do_objectID: "96DB165A-CD10-4147-842C-646AD31BB494",
                            targetBlank: !0,
                            windingRule: 1,
                            innerShadows: [],
                            borderOptions: {
                                _class: "borderOptions",
                                isEnabled: !0,
                                dashPattern: [],
                                lineCapStyle: 0,
                                lineJoinStyle: 0
                            },
                            colorControls: {
                                hue: 0,
                                _class: "colorControls",
                                contrast: 1,
                                isEnabled: !1,
                                brightness: 0,
                                saturation: 1
                            },
                            endMarkerType: 0,
                            startMarkerType: 0
                        },
                        name: "LinkedIn",
                        type: t[21],
                        frame: {
                            x: 151,
                            y: 0,
                            top: 0,
                            left: 151,
                            right: 0,
                            width: 49,
                            bottom: 32,
                            height: 49,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            }
                        },
                        isLink: "external",
                        maskId: null,
                        content: [{
                            $id: "F13A0E9F-4CCA-47E5-8F2C-A384AABC082D",
                            name: "Oval",
                            type: t[27],
                            frame: {
                                top: 0,
                                left: 0,
                                right: 0,
                                width: 49,
                                bottom: 0,
                                height: 49,
                                responsive: {
                                    x: {
                                        pos: "left",
                                        left: "vw",
                                        right: "vw"
                                    },
                                    y: {
                                        pos: "top",
                                        top: "vw",
                                        bottom: "vw"
                                    },
                                    scaling: "vw"
                                }
                            },
                            maskId: null,
                            hasClippingMask: !1
                        }, {
                            $id: "05D28152-8045-464E-ADEC-628E1152FBF9",
                            name: "in",
                            type: t[27],
                            frame: {
                                top: 14,
                                left: 16,
                                right: 15,
                                width: 18,
                                bottom: 17,
                                height: 18,
                                scaleX: 1,
                                scaleY: 1,
                                responsive: {
                                    x: {
                                        pos: "left",
                                        left: "vw",
                                        right: "vw"
                                    },
                                    y: {
                                        pos: "top",
                                        top: "vw",
                                        bottom: "vw"
                                    },
                                    scaling: "vw"
                                },
                                rotateAngle: 0
                            },
                            maskId: null,
                            hasClippingMask: !1
                        }],
                        hasClippingMask: !1
                    }],
                    inAnimation: {
                        edge: "right",
                        type: t[18],
                        curve: t[19],
                        delay: .5,
                        pinTo: "card",
                        scale: 1,
                        title: "Slide In",
                        opacity: 1,
                        styleId: "ebd8bf35-80f2-4182-a442-4a0e5bc2fdb0",
                        duration: 300,
                        blurRadius: 0,
                        translateX: 0,
                        translateY: 0,
                        animationBasis: "position"
                    },
                    hasClippingMask: !1
                }],
                gestures: {
                    swipeLeft: {
                        edge: "right",
                        target: "0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55",
                        timestamp: 1584456158464,
                        transition: {
                            type: t[22],
                            curve: t[19],
                            title: "Parallax",
                            margin: 1,
                            duration: 500
                        }
                    },
                    swipeRight: {
                        edge: "left",
                        target: "E4D65595-1DA8-41B2-B0D7-B2E3586404A9",
                        timestamp: 2,
                        transition: {
                            type: t[23],
                            curve: t[24],
                            title: "3D Cube",
                            duration: 1200
                        }
                    }
                },
                $ordering: 5,
                scrolling: !1,
                hasClippingMask: !1
            }, {
                $id: "75b32d68-c1a3-40fe-a2f8-7d6435026c9c",
                name: "Login",
                type: t[11],
                frame: {
                    x: 451,
                    y: 734,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 667,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    },
                    originalWidth: 375,
                    originalHeight: 667
                },
                content: [{
                    $id: "DC62FF00-5089-49B9-BDE8-E1F384A0CF35",
                    name: "form",
                    type: t[21],
                    frame: {
                        x: 55.5,
                        y: 307.5,
                        top: 307.5,
                        left: 55.5,
                        right: 55.5,
                        width: 264,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 280,
                        height: 79.5,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        originalWidth: 264,
                        originalHeight: 79.5,
                        constrainProportions: !1
                    },
                    index: 0,
                    content: [{
                        $id: "728C2289-5670-487D-A98E-2D02F962F04F",
                        name: "Rectangle",
                        type: t[27],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 0,
                            width: 264,
                            bottom: 27.5,
                            height: 52,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        }
                    }, {
                        $id: "0C02D32A-F053-4445-AB40-73C861E1D879",
                        name: "",
                        type: t[20],
                        frame: {
                            x: 118,
                            y: 420,
                            top: 74.5,
                            left: 7.5,
                            right: 6.5,
                            width: 249,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 0,
                            height: 24,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0,
                        isLink: "internal",
                        gestures: {
                            tap: {
                                edge: "down",
                                target: "b058e5cd-e6a5-42a8-880d-1cce50687810",
                                transition: {
                                    type: t[22],
                                    curve: t[19],
                                    title: "Parallax",
                                    margin: 1,
                                    duration: 500
                                }
                            }
                        }
                    }, {
                        $id: "7B017DE4-2820-4819-8266-F347C361E1E7",
                        name: "",
                        type: t[20],
                        frame: {
                            x: 88.46280822633149,
                            y: 332.3237894352451,
                            top: 16.5,
                            left: 14.5,
                            right: 11.5,
                            width: 238,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 30,
                            height: 24,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0
                    }]
                }, {
                    $id: "a7b885c4-e8c8-4e22-9e92-bd6a1ce73256",
                    src: "../../assets/images/09c2efe0c30eb5f0fd614c2cb82e3865.png",
                    name: "email.png",
                    type: t[12],
                    frame: {
                        x: 138.5,
                        y: 112.5,
                        top: 116,
                        left: 112,
                        right: 112,
                        width: 150,
                        alignX: "left",
                        alignY: "top",
                        bottom: 437,
                        height: 113,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 150,
                        originalHeight: 113,
                        constrainProportions: !0
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "76907f0e-5af8-4593-9191-8286888c4268",
                    src: "../../assets/images/c016e6d972f5390619d276622e8fecd9.png",
                    name: "close.png",
                    type: t[12],
                    frame: {
                        x: 181.02777777777794,
                        y: 34.72222222222206,
                        top: 34,
                        left: 175,
                        right: 175,
                        width: 24,
                        alignX: "left",
                        alignY: "top",
                        bottom: 608,
                        height: 24,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 24,
                        originalHeight: 24,
                        constrainProportions: !0
                    },
                    isLink: "internal",
                    gestures: {
                        tap: {
                            edge: "left",
                            target: "0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55",
                            transition: {
                                type: t[22],
                                curve: t[19],
                                title: "Parallax",
                                margin: 1,
                                duration: 500
                            }
                        }
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "BDAC0717-B745-48F6-AE12-CE9D66A5DB60",
                    name: "",
                    type: t[20],
                    frame: {
                        x: 57.83024388048697,
                        y: 366.3212870424539,
                        top: 557,
                        left: 57,
                        right: 53,
                        width: 265,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: -21,
                        height: 72,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 100,
                        originalHeight: 14,
                        constrainProportions: !1
                    },
                    index: 0
                }],
                gestures: {},
                $ordering: 6,
                scrolling: !1,
                scrollReset: !1
            }, {
                $id: "b058e5cd-e6a5-42a8-880d-1cce50687810",
                name: "Verificar",
                type: t[11],
                frame: {
                    x: 451,
                    y: 1481,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 667,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    },
                    originalWidth: 375,
                    originalHeight: 667
                },
                content: [{
                    $id: "cbeffed7-61c5-4d85-a2be-2f1a6687b212",
                    src: "../../assets/images/3d92316a7d6fc72e211c3e3ddd1df7e4.png",
                    name: "paper-plane.png copy",
                    type: t[12],
                    frame: {
                        x: 117,
                        y: 122,
                        top: 93,
                        left: 123,
                        right: 124,
                        width: 128,
                        alignX: "left",
                        alignY: "top",
                        bottom: 446,
                        height: 128,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 128,
                        originalHeight: 128,
                        constrainProportions: !0
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "d41774fe-abb7-478f-a2ad-1254eae744d9",
                    name: "formB",
                    type: t[21],
                    frame: {
                        x: 55.5,
                        y: 307.5,
                        top: 307,
                        left: 55,
                        right: 56,
                        width: 264,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 281,
                        height: 79,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 264,
                        originalHeight: 79.5,
                        constrainProportions: !1
                    },
                    index: 0,
                    content: [{
                        $id: "4bd6e643-4783-4b3f-9be8-ab2d765aa262",
                        name: "Rectangle",
                        type: t[27],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 0,
                            width: 264,
                            bottom: 27.5,
                            height: 52,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        parentIndex: 0
                    }, {
                        $id: "e334d4f3-07ec-4215-b5b5-610af3a5f6a4",
                        name: "",
                        type: t[20],
                        frame: {
                            x: 118,
                            y: 420,
                            top: 74.5,
                            left: 7.5,
                            right: 6.5,
                            width: 249,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 0,
                            height: 22,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0,
                        isLink: "internal",
                        gestures: {
                            tap: {
                                edge: "right",
                                target: "ab1ad13b-4d31-4f40-9d67-cc50287ee465",
                                transition: {
                                    type: t[22],
                                    curve: t[19],
                                    title: "Parallax",
                                    margin: 1,
                                    duration: 500
                                }
                            }
                        },
                        parentIndex: 0
                    }, {
                        $id: "2ad65a2d-fb97-4637-bab9-70b0196be206",
                        name: "",
                        type: t[20],
                        frame: {
                            x: 88.46280822633149,
                            y: 332.3237894352451,
                            top: 16.5,
                            left: 14.5,
                            right: 11.5,
                            width: 238,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 30,
                            height: 24,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0,
                        parentIndex: 0
                    }]
                }, {
                    $id: "763b47bd-222c-4a15-9648-51220ef32128",
                    name: "Revisa tu email\nEncontrarás un token de seguridad único\ncopialo y pegalo en la pantalla siguiente\npara acceder al perfil de tus eventos copy",
                    type: t[20],
                    frame: {
                        x: 57.83024388048697,
                        y: 366.3212870424539,
                        top: 557,
                        left: 57,
                        right: 53,
                        width: 265,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 42,
                        height: 72,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 100,
                        originalHeight: 14,
                        constrainProportions: !1
                    },
                    index: 0
                }],
                gestures: {},
                $ordering: 7,
                scrolling: !1,
                scrollReset: !1
            }, {
                $id: "ab1ad13b-4d31-4f40-9d67-cc50287ee465",
                name: "Compay",
                type: t[11],
                frame: {
                    x: 915,
                    y: 734,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 760,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    },
                    originalWidth: 375,
                    originalHeight: 667
                },
                content: [{
                    $id: "3e1ec960-ca18-46ab-bbeb-04e734c86915",
                    src: "../../assets/images/7e967f22c9701f07f47e344b93a2107c.png",
                    name: "300px_sasaicon.png",
                    type: t[12],
                    frame: {
                        x: 88.69614950797757,
                        y: 100.20451752297072,
                        top: 51,
                        left: 85,
                        right: 85,
                        width: 205,
                        alignX: "left",
                        alignY: "top",
                        bottom: -240,
                        height: 215,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 300,
                        originalHeight: 314,
                        constrainProportions: !0
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "97e739c6-23bf-4b7d-88c9-a01f24457b8a",
                    name: "Revisa tu email\nEncontrarás un token de seguridad único\ncopialo y pegalo en la pantalla siguiente\npara acceder al perfil de tus eventos copy",
                    type: t[20],
                    frame: {
                        x: 57.83024388048697,
                        y: 366.3212870424539,
                        top: 283,
                        left: 55,
                        right: 55,
                        width: 265,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 403,
                        height: 72,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 100,
                        originalHeight: 14,
                        constrainProportions: !1
                    },
                    index: 0
                }, {
                    $id: "30057435-0317-4597-95CD-3B766C66DE12",
                    name: "Contactos",
                    type: t[21],
                    frame: {
                        x: 55,
                        y: 364,
                        top: 364,
                        left: 55,
                        right: 57,
                        width: 263,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 304,
                        height: 92,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        originalWidth: 258,
                        originalHeight: 813,
                        constrainProportions: !1
                    },
                    index: 0,
                    content: [{
                        $id: "6A7A0597-AFDF-4352-AFE6-D97A1CAD0476",
                        name: "Line",
                        type: t[27],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 0,
                            width: 263,
                            bottom: 91,
                            height: 1,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "center",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "center",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        }
                    }, {
                        $id: "97F55116-D9E3-4535-A8A1-129E53611888",
                        name: "",
                        type: t[20],
                        frame: {
                            x: 65.85994949166968,
                            y: 373.4027565895376,
                            top: 9,
                            left: 21,
                            right: 1,
                            width: 241,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 66,
                            height: 17,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0
                    }, {
                        $id: "a00382aa-6b5e-43cc-8639-30eae6f74079",
                        name: "+57 319.577.5617 copy",
                        type: t[20],
                        frame: {
                            x: 65.85994949166968,
                            y: 373.4027565895376,
                            top: 31,
                            left: 21,
                            right: 1,
                            width: 241,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 42,
                            height: 19,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0
                    }, {
                        $id: "f67cad34-5bb4-4301-b3d6-15095da323d4",
                        name: "+57 319.577.5617 copy 2",
                        type: t[20],
                        frame: {
                            x: 65.85994949166968,
                            y: 373.4027565895376,
                            top: 53,
                            left: 21,
                            right: 1,
                            width: 241,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 20,
                            height: 19,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0
                    }, {
                        $id: "08207910-1168-43bb-b4eb-190c0153e5fa",
                        name: "+57 319.577.5617 copy 3",
                        type: t[20],
                        frame: {
                            x: 65.85994949166968,
                            y: 373.4027565895376,
                            top: 73,
                            left: 21,
                            right: 1,
                            width: 241,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 0,
                            height: 19,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0
                    }, {
                        $id: "5bc1c9c7-b91b-450d-bc18-68eeaac58f9f",
                        src: "../../assets/images/99a258a3065941c965be181959fde22d.png",
                        name: "phone_inv.png",
                        type: t[12],
                        frame: {
                            x: 59.29004437855701,
                            y: 360.13362241045365,
                            top: 14,
                            left: 5,
                            right: 249,
                            width: 9,
                            alignX: "left",
                            alignY: "top",
                            bottom: 69,
                            height: 9,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 24,
                            originalHeight: 24,
                            constrainProportions: !0
                        },
                        imageFit: "cover",
                        imageFocalPoint: "xMidYMid"
                    }, {
                        $id: "18525d17-aa54-4383-8518-64c86f1b080f",
                        src: "../../assets/images/866a110656a5b1dfaedc543bf5f74813.png",
                        name: "mail_inv.png",
                        type: t[12],
                        frame: {
                            x: 67.0285593003955,
                            y: 383.75187582885246,
                            top: 37,
                            left: 5,
                            right: 249,
                            width: 9,
                            alignX: "left",
                            alignY: "top",
                            bottom: 46,
                            height: 9,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 24,
                            originalHeight: 24,
                            constrainProportions: !0
                        },
                        imageFit: "cover",
                        imageFocalPoint: "xMidYMid"
                    }, {
                        $id: "f5b891c5-c3b5-4f60-9932-623b99377c42",
                        src: "../../assets/images/6ade112b9daea159985bffa545a969b3.png",
                        name: "map_inv.png",
                        type: t[12],
                        frame: {
                            x: 69.0767422659369,
                            y: 417.06920034124687,
                            top: 59,
                            left: 5,
                            right: 249,
                            width: 9,
                            alignX: "left",
                            alignY: "top",
                            bottom: 24,
                            height: 9,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 24,
                            originalHeight: 24,
                            constrainProportions: !0
                        },
                        imageFit: "cover",
                        imageFocalPoint: "xMidYMid"
                    }, {
                        $id: "bb9a605d-a839-4b41-9423-1f6bc6d99d8f",
                        src: "../../assets/images/21dc1bc4bde9287ce82fba8f4f5b30aa.png",
                        name: "web_inv.png",
                        type: t[12],
                        frame: {
                            x: 73.14433753249943,
                            y: 435.95446407885856,
                            top: 79,
                            left: 5,
                            right: 249,
                            width: 9,
                            alignX: "left",
                            alignY: "top",
                            bottom: 4,
                            height: 9,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 24,
                            originalHeight: 24,
                            constrainProportions: !0
                        },
                        imageFit: "cover",
                        imageFocalPoint: "xMidYMid"
                    }]
                }, {
                    $id: "701751A7-6E4A-4501-8383-DFC9FB94771A",
                    name: "Evento_1",
                    type: t[21],
                    frame: {
                        x: 37,
                        y: 488,
                        top: 488,
                        left: 37,
                        right: 35,
                        width: 302,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 206,
                        height: 65,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 301.35896704945293,
                        originalHeight: 83.10440969871274,
                        constrainProportions: !1
                    },
                    index: 0,
                    isLink: "internal",
                    content: [{
                        $id: "D0FDC425-4D8E-4988-AC6B-ABC77669F84D",
                        name: "Rectangle",
                        type: t[27],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 0,
                            width: 302,
                            bottom: 0,
                            height: 65,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        parentIndex: 0
                    }, {
                        $id: "F63C1D16-16CA-46CD-A35F-FB73CCE27F31",
                        name: "Oval",
                        type: t[27],
                        frame: {
                            top: 9,
                            left: 19,
                            right: 238,
                            width: 45,
                            bottom: 11,
                            height: 45,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        parentIndex: 0
                    }, {
                        $id: "34b36616-c8d4-4f55-8aa3-78f5e3421a90",
                        name: "Revisa tu email\nEncontrarás un token de seguridad único\ncopialo y pegalo en la pantalla siguiente\npara acceder al perfil de tus eventos copy 2",
                        type: t[20],
                        frame: {
                            x: 57.83024388048697,
                            y: 366.3212870424539,
                            top: 4,
                            left: 70,
                            right: 27,
                            width: 205,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 5,
                            height: 56,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0,
                        parentIndex: 0
                    }],
                    gestures: {
                        tap: {
                            edge: "right",
                            target: "d7130849-99d5-4f4f-b876-7f8bc78ef16c",
                            transition: {
                                type: t[22],
                                curve: t[19],
                                title: "Parallax",
                                margin: 1,
                                duration: 500
                            }
                        }
                    }
                }, {
                    $id: "a5a9edf4-3be0-406a-80e0-0c8cea9bfada",
                    name: "Evento_1 copy",
                    type: t[21],
                    frame: {
                        x: 37,
                        y: 568,
                        top: 568,
                        left: 37,
                        right: 36,
                        width: 302,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 126,
                        height: 65,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 301.35896704945293,
                        originalHeight: 83.10440969871274,
                        constrainProportions: !1
                    },
                    index: 0,
                    content: [{
                        $id: "6d3fe0f8-013b-4ed5-9e05-79a87a871e22",
                        name: "Rectangle",
                        type: t[27],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 0,
                            width: 302,
                            bottom: 0,
                            height: 65,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        parentIndex: 0
                    }, {
                        $id: "40c97664-621b-4221-bcd4-7f057c68226e",
                        name: "Oval",
                        type: t[27],
                        frame: {
                            top: 9,
                            left: 19,
                            right: 238,
                            width: 45,
                            bottom: 11,
                            height: 45,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        parentIndex: 0
                    }, {
                        $id: "135b6f51-f680-4c98-9afc-93f6eead0774",
                        name: "Revisa tu email\nEncontrarás un token de seguridad único\ncopialo y pegalo en la pantalla siguiente\npara acceder al perfil de tus eventos copy 2",
                        type: t[20],
                        frame: {
                            x: 57.83024388048697,
                            y: 366.3212870424539,
                            top: 4,
                            left: 70,
                            right: 27,
                            width: 205,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 5,
                            height: 56,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0,
                        parentIndex: 0
                    }]
                }, {
                    $id: "e4e4ac92-2983-4703-bbf0-503f6cd55297",
                    name: "Evento_1 copy 2",
                    type: t[21],
                    frame: {
                        x: 37,
                        y: 648,
                        top: 648,
                        left: 37,
                        right: 36,
                        width: 302,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 46,
                        height: 65,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 301.35896704945293,
                        originalHeight: 83.10440969871274,
                        constrainProportions: !1
                    },
                    index: 0,
                    content: [{
                        $id: "ff7f9184-d229-4211-9154-80e658552cfa",
                        name: "Rectangle",
                        type: t[27],
                        frame: {
                            top: 0,
                            left: 0,
                            right: 0,
                            width: 302,
                            bottom: 0,
                            height: 65,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        parentIndex: 0
                    }, {
                        $id: "72d8f390-54fc-41dc-b1c2-56508bf0bfca",
                        name: "Oval",
                        type: t[27],
                        frame: {
                            top: 9,
                            left: 19,
                            right: 238,
                            width: 45,
                            bottom: 11,
                            height: 45,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0
                        },
                        parentIndex: 0
                    }, {
                        $id: "adc05a86-4d44-4fcf-93ee-fea87beaa114",
                        name: "Revisa tu email\nEncontrarás un token de seguridad único\ncopialo y pegalo en la pantalla siguiente\npara acceder al perfil de tus eventos copy 2",
                        type: t[20],
                        frame: {
                            x: 57.83024388048697,
                            y: 366.3212870424539,
                            top: 4,
                            left: 70,
                            right: 27,
                            width: 205,
                            _class: "rect",
                            alignX: "left",
                            alignY: "top",
                            bottom: 5,
                            height: 56,
                            scaleX: 1,
                            scaleY: 1,
                            responsive: {
                                x: {
                                    pos: "left",
                                    left: "vw",
                                    right: "vw"
                                },
                                y: {
                                    pos: "top",
                                    top: "vw",
                                    bottom: "vw"
                                },
                                scaling: "vw"
                            },
                            rotateAngle: 0,
                            originalWidth: 100,
                            originalHeight: 14,
                            constrainProportions: !1
                        },
                        index: 0,
                        parentIndex: 0
                    }]
                }, {
                    $id: "49f0ba87-c355-402c-aef2-1c890e703c92",
                    src: "../../assets/images/66731d571ce72bb14d8cd722ce38a6ba.png",
                    name: "check.png",
                    type: t[12],
                    frame: {
                        x: 311.1481714163076,
                        y: 477.95617638877957,
                        top: 482,
                        left: 310,
                        right: 48,
                        width: 16,
                        alignX: "left",
                        alignY: "top",
                        bottom: -472,
                        height: 16,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 16,
                        originalHeight: 16,
                        constrainProportions: !0
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }],
                gestures: {},
                $ordering: 8,
                scrolling: !0,
                scrollReset: !1
            }, {
                $id: "d7130849-99d5-4f4f-b876-7f8bc78ef16c",
                name: "Evento",
                type: t[11],
                frame: {
                    x: 1379,
                    y: 734,
                    top: 0,
                    left: 0,
                    right: 0,
                    width: 375,
                    bottom: 0,
                    height: 1334,
                    responsive: {
                        x: {
                            pos: "left",
                            left: "vw",
                            right: "vw"
                        },
                        y: {
                            pos: "top",
                            top: "vw",
                            bottom: "vw"
                        },
                        scaling: "vw"
                    },
                    originalWidth: 375,
                    originalHeight: 667
                },
                content: [{
                    $id: "9dff98e3-fd48-48c4-8123-8c66eedeef5a",
                    src: "../../assets/images/ee701954e9e30843a572431c21270748.jpg",
                    name: "maxresdefault.jpg",
                    type: t[12],
                    frame: {
                        x: 29.82694858227387,
                        y: 9.771276201035562,
                        top: 0,
                        left: 0,
                        right: -1,
                        width: 375,
                        alignX: "left",
                        alignY: "top",
                        bottom: 1121,
                        height: 212,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 600,
                        originalHeight: 338,
                        constrainProportions: !0
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "A763D189-0ED7-426A-88AF-23ABFC34868D",
                    name: "Rectangle",
                    type: t[27],
                    frame: {
                        top: 212,
                        left: 0,
                        right: 0,
                        width: 375,
                        bottom: 1082,
                        height: 36,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    }
                }, {
                    $id: "83263073-B4D3-4FB2-B509-4587942F80AA",
                    name: "",
                    type: t[20],
                    frame: {
                        x: 37.605543587099234,
                        y: 310.4049858263219,
                        top: 220,
                        left: 9,
                        right: 7,
                        width: 356,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 1101,
                        height: 23,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 100,
                        originalHeight: 14,
                        constrainProportions: !1
                    },
                    index: 0
                }, {
                    $id: "866f19e5-cd4f-4d1d-82fd-b3b661703af3",
                    name: "Revisa tu email\nEncontrarás un token de seguridad único\ncopialo y pegalo en la pantalla siguiente\npara acceder al perfil de tus eventos copy",
                    type: t[20],
                    frame: {
                        x: 57.83024388048697,
                        y: 366.3212870424539,
                        top: 264,
                        left: 17,
                        right: 92,
                        width: 265,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 997,
                        height: 48,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 100,
                        originalHeight: 14,
                        constrainProportions: !1
                    },
                    index: 0
                }, {
                    $id: "1ad85705-5594-4c81-8c98-d5c5c861e588",
                    src: "../../assets/images/7e967f22c9701f07f47e344b93a2107c.png",
                    name: "300px_sasaicon.png copy",
                    type: t[12],
                    frame: {
                        x: 88.69614950797757,
                        y: 100.20451752297072,
                        top: 263,
                        left: 310,
                        right: 18,
                        width: 46,
                        alignX: "left",
                        alignY: "top",
                        bottom: 1022,
                        height: 48,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 300,
                        originalHeight: 314,
                        constrainProportions: !0
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "06eef167-959f-4eda-a519-67dca5c06cb8",
                    name: "Revisa tu email\nEncontrarás un token de seguridad único\ncopialo y pegalo en la pantalla siguiente\npara acceder al perfil de tus eventos copy 2",
                    type: t[20],
                    frame: {
                        x: 57.83024388048697,
                        y: 366.3212870424539,
                        top: 724,
                        left: 55,
                        right: 55,
                        width: 265,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 558,
                        height: 320,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 100,
                        originalHeight: 14,
                        constrainProportions: !1
                    },
                    index: 0
                }, {
                    $id: "c07e9060-6fb4-4cd6-b485-1894f98e0c8f",
                    name: "Revisa tu email\nEncontrarás un token de seguridad único\ncopialo y pegalo en la pantalla siguiente\npara acceder al perfil de tus eventos copy 3",
                    type: t[20],
                    frame: {
                        x: 57.83024388048697,
                        y: 366.3212870424539,
                        top: 631,
                        left: 55,
                        right: 55,
                        width: 265,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 616,
                        height: 85,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 100,
                        originalHeight: 14,
                        constrainProportions: !1
                    },
                    index: 0
                }, {
                    $id: "6c437825-b5f6-4f1e-a688-d8bd26c8561b",
                    src: "../../assets/images/a4d5662f8a95c8ec61e900249ab22535.jpg",
                    name: "tydemo.jpg",
                    type: t[12],
                    frame: {
                        x: 69.48367940979597,
                        y: 373.3454018330824,
                        top: 394,
                        left: 0,
                        right: -3,
                        width: 378,
                        alignX: "left",
                        alignY: "top",
                        bottom: 60,
                        height: 212,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 550,
                        originalHeight: 309,
                        constrainProportions: !0
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "7D60A956-A365-471C-A5CF-6910A4E60136",
                    name: "Rectangle",
                    type: t[27],
                    frame: {
                        top: 333,
                        left: 41,
                        right: 41,
                        width: 292,
                        bottom: 964,
                        height: 37,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0
                    }
                }, {
                    $id: "1e9bd735-dbaa-4879-944a-7cfd5250cb16",
                    name: "Revisa tu email\nEncontrarás un token de seguridad único\ncopialo y pegalo en la pantalla siguiente\npara acceder al perfil de tus eventos copy 4",
                    type: t[20],
                    frame: {
                        x: 57.83024388048697,
                        y: 366.3212870424539,
                        top: 340,
                        left: 48,
                        right: 111,
                        width: 215,
                        _class: "rect",
                        alignX: "left",
                        alignY: "top",
                        bottom: 938,
                        height: 23,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 100,
                        originalHeight: 14,
                        constrainProportions: !1
                    },
                    index: 0
                }, {
                    $id: "7eb3c7b0-ac01-43e7-93bf-2eaa359151ce",
                    src: "../../assets/images/2c78d0906bd95ceb04e9d1c0c0a53c36.png",
                    name: "bellinv.png",
                    type: t[12],
                    frame: {
                        x: 303.8993637142721,
                        y: 324.83394324294386,
                        top: 340,
                        left: 299,
                        right: 51,
                        width: 24,
                        alignX: "left",
                        alignY: "top",
                        bottom: 302,
                        height: 24,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 24,
                        originalHeight: 24,
                        constrainProportions: !0
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }, {
                    $id: "742b496c-efcd-4d56-a332-0606a889525e",
                    src: "../../assets/images/c016e6d972f5390619d276622e8fecd9.png",
                    name: "close.png",
                    type: t[12],
                    frame: {
                        x: 325.4125473312963,
                        y: 18.0373285605026,
                        top: 10,
                        left: 346,
                        right: 13,
                        width: 15,
                        alignX: "left",
                        alignY: "top",
                        bottom: 641,
                        height: 15,
                        scaleX: 1,
                        scaleY: 1,
                        responsive: {
                            x: {
                                pos: "center",
                                left: "vw",
                                right: "vw"
                            },
                            y: {
                                pos: "center",
                                top: "vw",
                                bottom: "vw"
                            },
                            scaling: "vw"
                        },
                        rotateAngle: 0,
                        originalWidth: 24,
                        originalHeight: 24,
                        constrainProportions: !0
                    },
                    isLink: "internal",
                    gestures: {
                        tap: {
                            edge: "left",
                            target: "ab1ad13b-4d31-4f40-9d67-cc50287ee465",
                            transition: {
                                type: t[22],
                                curve: t[19],
                                title: "Parallax",
                                margin: 1,
                                duration: 500
                            }
                        }
                    },
                    imageFit: "cover",
                    imageFocalPoint: "xMidYMid"
                }],
                gestures: {},
                $ordering: 9,
                scrolling: !0,
                scrollReset: !1
            }],
            device: "mobile",
            styles: '.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]{background-color:rgba(255,255,255,1);}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper{position:relative;width:100%;height:100%;}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="55020CDE-798A-4F23-BAD8-4356C48CAEA8"]{position:absolute;width:100vw;height:calc(var(--ihpx) * 1);top:0;left:0;right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="2c04ebba-9dbd-4139-93d5-d23cb76c8745"]{position:absolute;width:41.07vw;height:10.13vw;top:calc((var(--ihpx) * 0.8861) - (10.13vw / 2));left:calc((100vw * 0.4987) - (41.07vw / 2));right:auto;bottom:auto;filter:unset;visibility:inherit;mix-blend-mode:screen;transform:translate3d(0, 0, 0);}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="618E76A3-CBDF-43FA-B064-D8D074D4BDF1"]{position:absolute;width:52.27vw;height:5.07vw;top:calc((var(--ihpx) * 0.7144) - (5.07vw / 2));left:calc((100vw * 0.4933) - (52.27vw / 2));right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="618E76A3-CBDF-43FA-B064-D8D074D4BDF1"] [data-text-paragraph-id="618E76A3-CBDF-43FA-B064-D8D074D4BDF1-0"]{white-space:pre-wrap;text-align:center;line-height:4vw;}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="618E76A3-CBDF-43FA-B064-D8D074D4BDF1"] [data-text-span-id="618E76A3-CBDF-43FA-B064-D8D074D4BDF1-0"]{color:rgba(255, 255, 255, 1);font-family:PlayfairDisplay-Regular;font-size:2.67vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="a46d7a25-f6e1-4efc-99b0-2af268166478"]{position:absolute;width:52.27vw;height:4.53vw;top:calc((var(--ihpx) * 0.9438) - (4.53vw / 2));left:calc((100vw * 0.4933) - (52.27vw / 2));right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="a46d7a25-f6e1-4efc-99b0-2af268166478"] [data-text-paragraph-id="a46d7a25-f6e1-4efc-99b0-2af268166478-0"]{white-space:pre-wrap;text-align:center;line-height:4vw;}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="a46d7a25-f6e1-4efc-99b0-2af268166478"] [data-text-span-id="a46d7a25-f6e1-4efc-99b0-2af268166478-0"]{color:rgba(255, 255, 255, 1);font-family:PlayfairDisplay-Regular;font-size:2.67vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="4E105924-DF73-490D-9CF8-84A2E4C59216"]{position:absolute;width:65.87vw;height:59.73vw;top:calc((var(--ihpx) * 0.2879) - (59.73vw / 2));left:calc((100vw * 0.5) - (65.87vw / 2));right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="4E105924-DF73-490D-9CF8-84A2E4C59216"]>[data-id="73456F0F-FB13-4517-ABAC-900B76213135"]{position:absolute;width:65.87vw;height:18.13vw;top:39.73vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="4E105924-DF73-490D-9CF8-84A2E4C59216"]>[data-id="73456F0F-FB13-4517-ABAC-900B76213135"] [data-text-paragraph-id="73456F0F-FB13-4517-ABAC-900B76213135-0"]{white-space:pre-wrap;text-align:center;line-height:15.47vw;}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="4E105924-DF73-490D-9CF8-84A2E4C59216"]>[data-id="73456F0F-FB13-4517-ABAC-900B76213135"] [data-text-span-id="73456F0F-FB13-4517-ABAC-900B76213135-0"]{color:rgba(255, 255, 255, 1);font-family:Montserrat-Regular;font-size:10.4vw;word-wrap:break-word;letter-spacing:0.11vw;}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="4E105924-DF73-490D-9CF8-84A2E4C59216"]>[data-id="0a8daedd-d6fd-4a98-9d7d-157171e46b82"]{position:absolute;width:40vw;height:40vw;top:calc((var(--ihpx) * 0.1124) - (40vw / 2));left:calc((100vw * 0.3333) - (40vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="4E105924-DF73-490D-9CF8-84A2E4C59216"]>[data-id="D143C30C-E842-41F5-AA34-5282482C2B14"]{position:absolute;width:56.53vw;height:5.07vw;top:55.47vw;left:4.53vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="4E105924-DF73-490D-9CF8-84A2E4C59216"]>[data-id="D143C30C-E842-41F5-AA34-5282482C2B14"] [data-text-paragraph-id="D143C30C-E842-41F5-AA34-5282482C2B14-0"]{white-space:pre-wrap;text-align:center;line-height:5.07vw;}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="4E105924-DF73-490D-9CF8-84A2E4C59216"]>[data-id="D143C30C-E842-41F5-AA34-5282482C2B14"] [data-text-span-id="D143C30C-E842-41F5-AA34-5282482C2B14-0"]{color:rgba(255, 255, 255, 1);font-family:Montserrat-Medium;font-size:3.47vw;word-wrap:break-word;letter-spacing:0.46vw;}.card[data-id="0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55"]>.wrapper>[data-id="ce049ab9-29e8-4e8f-a6a9-27bb1a02b4a7"]{position:absolute;width:21.6vw;height:21.6vw;top:calc((var(--ihpx) * 0.6229) - (21.6vw / 2));left:calc((100vw * 0.5) - (21.6vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}@font-face{font-family:PlayfairDisplay-Regular;src:url("https://fonts.gstatic.com/s/playfairdisplay/v18/nuFiD-vYSZviVYUb_rj3ij__anPXDTzYgEM86xQ.woff2");}@font-face{font-family:Montserrat-Regular;src:url("https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WlhyyTh89Y.woff2");}@font-face{font-family:Montserrat-Medium;src:url("https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_ZpC3gnD_vx3rCs.woff2");}@font-face{font-family:PlayfairDisplay-Italic;src:url("https://fonts.gstatic.com/s/playfairdisplay/v18/nuFkD-vYSZviVYUb_rj3ij__anPXDTnogkk7yRZrPA.woff2");}@font-face{font-family:NunitoSans-Regular;src:url("https://fonts.gstatic.com/s/nunitosans/v5/pe0qMImSLYBIv1o4X1M8cce9I9tAcVwo.woff2");}:root{--ih:100vh;}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]{background-color:rgba(255,255,255,1);}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper{position:relative;width:100%;height:100%;}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="D8C08B00-E465-412F-8BEB-BE791BBC8FB5"]{position:absolute;width:272.8vw;height:calc(var(--ihpx) * 1.0225);top:calc((var(--ihpx) * 0.4888) - (calc(var(--ihpx) * 1.0225) / 2));left:calc((100vw * 0.172) - (272.8vw / 2));right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:multiply;transform:translate3d(0, 0, 0);}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="21EACC7C-6912-4E47-B0AB-08DF8B6CC27C"]{position:absolute;width:253.61vw;height:calc(var(--ihpx) * 0.9925);top:calc((var(--ihpx) * 0.3433) - (calc(var(--ihpx) * 0.9925) / 2));left:calc((100vw * 0.7587) - (253.61vw / 2));right:auto;bottom:auto;filter:unset;opacity:1;visibility:inherit;mix-blend-mode:multiply;transform:translate3d(0, 0, 0);}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]{position:absolute;width:81.07vw;height:41.87vw;top:calc((var(--ihpx) * 0.7819) - (41.87vw / 2));left:calc((100vw * 0.464) - (81.07vw / 2));right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="180A8DF0-11A9-4DB3-80B3-8F61C85DC1B1"]{position:absolute;width:81.07vw;height:12.8vw;top:29.07vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="180A8DF0-11A9-4DB3-80B3-8F61C85DC1B1"] [data-text-paragraph-id="180A8DF0-11A9-4DB3-80B3-8F61C85DC1B1-0"]{white-space:pre-wrap;text-align:left;line-height:5.33vw;}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="180A8DF0-11A9-4DB3-80B3-8F61C85DC1B1"] [data-text-span-id="180A8DF0-11A9-4DB3-80B3-8F61C85DC1B1-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Regular;font-size:3.73vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="22DF118B-294F-4627-A3E0-FC0F5E34055B"]{position:absolute;width:32.27vw;height:5.07vw;top:22.93vw;left:0vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="22DF118B-294F-4627-A3E0-FC0F5E34055B"] [data-text-paragraph-id="22DF118B-294F-4627-A3E0-FC0F5E34055B-0"]{white-space:pre-wrap;text-align:left;line-height:5.6vw;}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="22DF118B-294F-4627-A3E0-FC0F5E34055B"] [data-text-span-id="22DF118B-294F-4627-A3E0-FC0F5E34055B-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Italic;font-size:3.73vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="78D6DDB9-0A89-4812-B558-DC4F8B5C8617"]{position:absolute;width:44.27vw;height:5.07vw;top:12.8vw;left:0vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="78D6DDB9-0A89-4812-B558-DC4F8B5C8617"] [data-text-paragraph-id="78D6DDB9-0A89-4812-B558-DC4F8B5C8617-0"]{white-space:pre-wrap;text-align:left;line-height:5.6vw;}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="78D6DDB9-0A89-4812-B558-DC4F8B5C8617"] [data-text-span-id="78D6DDB9-0A89-4812-B558-DC4F8B5C8617-0"]{color:rgba(83, 83, 83, 1);font-family:PlayfairDisplay-Regular;font-size:3.73vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="97A529DC-5C64-40AC-AE13-13698678ABAB"]{position:absolute;width:71.47vw;height:12.8vw;top:0vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="97A529DC-5C64-40AC-AE13-13698678ABAB"] [data-text-paragraph-id="97A529DC-5C64-40AC-AE13-13698678ABAB-0"]{white-space:pre-wrap;text-align:left;line-height:14.4vw;}.card[data-id="8A730960-47ED-4804-9598-E351DDBE3E6B"]>.wrapper>[data-id="99ADF95D-8239-4A5A-8D66-29CDAD95AB6D"]>[data-id="97A529DC-5C64-40AC-AE13-13698678ABAB"] [data-text-span-id="97A529DC-5C64-40AC-AE13-13698678ABAB-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Regular;font-size:9.6vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]{background-color:rgba(255,255,255,1);}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper{position:relative;width:100%;height:100%;}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="F0402241-92B6-4842-97C3-9EC5FD9FFA5C"]{position:absolute;width:178.31vw;height:calc(var(--ihpx) * 1.5037);top:calc((var(--ihpx) * 0.56) - (calc(var(--ihpx) * 1.5037) / 2));left:calc((100vw * 0.5387) - (178.31vw / 2));right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="00F8606D-FE24-4A5F-A9DB-5F3DC85FE2CE"]{position:absolute;width:178.13vw;height:calc(var(--ihpx) * 1.2521);top:calc((var(--ihpx) * 0.3739) - (calc(var(--ihpx) * 1.2521) / 2));left:calc((100vw * 0.5387) - (178.13vw / 2));right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:screen;transform:translate3d(0, 0, 0);}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]{position:absolute;width:81.07vw;height:47.73vw;top:calc((var(--ihpx) * 0.2196) - (47.73vw / 2));left:calc((100vw * 0.5067) - (81.07vw / 2));right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="92B8D1BB-ABF6-4934-A5A5-DD7C86F569CA"]{position:absolute;width:81.07vw;height:18.67vw;top:29.07vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="92B8D1BB-ABF6-4934-A5A5-DD7C86F569CA"] [data-text-paragraph-id="92B8D1BB-ABF6-4934-A5A5-DD7C86F569CA-0"]{white-space:pre-wrap;text-align:left;line-height:5.33vw;}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="92B8D1BB-ABF6-4934-A5A5-DD7C86F569CA"] [data-text-span-id="92B8D1BB-ABF6-4934-A5A5-DD7C86F569CA-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Regular;font-size:3.73vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="CAF8920B-94C8-4D2C-977E-FB4B60333D8C"]{position:absolute;width:32.27vw;height:5.07vw;top:22.93vw;left:0vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="CAF8920B-94C8-4D2C-977E-FB4B60333D8C"] [data-text-paragraph-id="CAF8920B-94C8-4D2C-977E-FB4B60333D8C-0"]{white-space:pre-wrap;text-align:left;line-height:5.6vw;}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="CAF8920B-94C8-4D2C-977E-FB4B60333D8C"] [data-text-span-id="CAF8920B-94C8-4D2C-977E-FB4B60333D8C-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Italic;font-size:3.73vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="D687001B-C47C-4C6E-9AD0-30A05B91B664"]{position:absolute;width:44.27vw;height:5.07vw;top:12.8vw;left:0vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="D687001B-C47C-4C6E-9AD0-30A05B91B664"] [data-text-paragraph-id="D687001B-C47C-4C6E-9AD0-30A05B91B664-0"]{white-space:pre-wrap;text-align:left;line-height:5.6vw;}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="D687001B-C47C-4C6E-9AD0-30A05B91B664"] [data-text-span-id="D687001B-C47C-4C6E-9AD0-30A05B91B664-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Regular;font-size:3.73vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="E755211E-80A8-4189-990D-8642B47AC26F"]{position:absolute;width:69.33vw;height:12.8vw;top:0vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="E755211E-80A8-4189-990D-8642B47AC26F"] [data-text-paragraph-id="E755211E-80A8-4189-990D-8642B47AC26F-0"]{white-space:pre-wrap;text-align:left;line-height:14.4vw;}.card[data-id="4875BC62-1759-4F94-87F8-249946AC7555"]>.wrapper>[data-id="3232B0D6-C712-428E-9E86-CC1F247693B0"]>[data-id="E755211E-80A8-4189-990D-8642B47AC26F"] [data-text-span-id="E755211E-80A8-4189-990D-8642B47AC26F-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Regular;font-size:9.6vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]{background-color:rgba(255,255,255,1);}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper{position:relative;width:100%;height:100%;}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="1547FC76-E0E7-478F-9F3A-00D7DF95C911"]{position:absolute;width:363.73vw;height:calc(var(--ihpx) * 1.2414);top:calc((var(--ihpx) * 0.5082) - (calc(var(--ihpx) * 1.2414) / 2));left:calc((100vw * 0.5787) - (363.73vw / 2));right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="4ED541B6-4A7F-465F-BE69-5C783B28DBD0"]{position:absolute;width:283.4vw;height:calc(var(--ihpx) * 1.018);top:calc((var(--ihpx) * 0.5) - (calc(var(--ihpx) * 1.018) / 2));left:calc((100vw * 0.5) - (283.4vw / 2));right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:overlay;transform:translate3d(0, 0, 0);}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]{position:absolute;width:73.33vw;height:41.33vw;top:calc((var(--ihpx) * 0.7864) - (41.33vw / 2));left:calc((100vw * 0.468) - (73.33vw / 2));right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="CB626D74-4FCF-4586-96E9-090DD7F67E33"]{position:absolute;width:60.27vw;height:12.8vw;top:0vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="CB626D74-4FCF-4586-96E9-090DD7F67E33"] [data-text-paragraph-id="CB626D74-4FCF-4586-96E9-090DD7F67E33-0"]{white-space:pre-wrap;text-align:left;line-height:14.4vw;}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="CB626D74-4FCF-4586-96E9-090DD7F67E33"] [data-text-span-id="CB626D74-4FCF-4586-96E9-090DD7F67E33-0"]{color:rgba(255, 255, 255, 1);font-family:PlayfairDisplay-Regular;font-size:9.6vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="37554E6A-A620-4C46-BFA5-5E65E1F8AB75"]{position:absolute;width:44.27vw;height:5.07vw;top:12.8vw;left:0vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="37554E6A-A620-4C46-BFA5-5E65E1F8AB75"] [data-text-paragraph-id="37554E6A-A620-4C46-BFA5-5E65E1F8AB75-0"]{white-space:pre-wrap;text-align:left;line-height:5.6vw;}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="37554E6A-A620-4C46-BFA5-5E65E1F8AB75"] [data-text-span-id="37554E6A-A620-4C46-BFA5-5E65E1F8AB75-0"]{color:rgba(255, 255, 255, 1);font-family:PlayfairDisplay-Regular;font-size:3.73vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="C1F648B8-EE63-4FC6-9D55-CB4C423B3DD5"]{position:absolute;width:32.27vw;height:5.07vw;top:22.93vw;left:0vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="C1F648B8-EE63-4FC6-9D55-CB4C423B3DD5"] [data-text-paragraph-id="C1F648B8-EE63-4FC6-9D55-CB4C423B3DD5-0"]{white-space:pre-wrap;text-align:left;line-height:5.6vw;}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="C1F648B8-EE63-4FC6-9D55-CB4C423B3DD5"] [data-text-span-id="C1F648B8-EE63-4FC6-9D55-CB4C423B3DD5-0"]{color:rgba(255, 255, 255, 1);font-family:PlayfairDisplay-Italic;font-size:3.73vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="03B98670-B85C-443B-8552-83F32958DA7A"]{position:absolute;width:73.33vw;height:12.8vw;top:28.53vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="03B98670-B85C-443B-8552-83F32958DA7A"] [data-text-paragraph-id="03B98670-B85C-443B-8552-83F32958DA7A-0"]{white-space:pre-wrap;text-align:left;line-height:5.33vw;}.card[data-id="78AF3047-7E04-4101-98E0-A71DDC18E506"]>.wrapper>[data-id="804DE8A9-8968-4990-9FBD-D18EE6AFC107"]>[data-id="03B98670-B85C-443B-8552-83F32958DA7A"] [data-text-span-id="03B98670-B85C-443B-8552-83F32958DA7A-0"]{color:rgba(255, 255, 255, 1);font-family:PlayfairDisplay-Regular;font-size:4vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]{background-color:rgba(255,255,255,1);}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper{position:relative;width:100%;height:100%;}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="BE2C2E72-8EE7-429D-A307-57FE1CB76642"]{position:absolute;width:118.71vw;height:calc(var(--ihpx) * 1);top:0;left:calc((100vw * 0.5) - (118.71vw / 2));right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="c05eeab9-f1bc-4698-9ee4-83962853879c"]{position:absolute;width:156.53vw;height:calc(var(--ihpx) * 1.2504);top:calc((var(--ihpx) * 0.5217) - (calc(var(--ihpx) * 1.2504) / 2));left:calc((100vw * 0.436) - (156.53vw / 2));right:auto;bottom:auto;filter:unset;mix-blend-mode:screen;transform:translate3d(0, 0, 0);}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]{position:absolute;width:74.13vw;height:41.33vw;top:calc((var(--ihpx) * 0.2016) - (41.33vw / 2));left:calc((100vw * 0.472) - (74.13vw / 2));right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="06BDAA54-F03F-48D4-9302-CAD8344B07F7"]{position:absolute;width:74.13vw;height:12.8vw;top:28.53vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="06BDAA54-F03F-48D4-9302-CAD8344B07F7"] [data-text-paragraph-id="06BDAA54-F03F-48D4-9302-CAD8344B07F7-0"]{white-space:pre-wrap;text-align:left;line-height:5.33vw;}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="06BDAA54-F03F-48D4-9302-CAD8344B07F7"] [data-text-span-id="06BDAA54-F03F-48D4-9302-CAD8344B07F7-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Regular;font-size:4vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="20A5E2AA-26F3-4F56-B13E-C1A50D374D78"]{position:absolute;width:32.27vw;height:5.07vw;top:22.93vw;left:0vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="20A5E2AA-26F3-4F56-B13E-C1A50D374D78"] [data-text-paragraph-id="20A5E2AA-26F3-4F56-B13E-C1A50D374D78-0"]{white-space:pre-wrap;text-align:left;line-height:5.6vw;}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="20A5E2AA-26F3-4F56-B13E-C1A50D374D78"] [data-text-span-id="20A5E2AA-26F3-4F56-B13E-C1A50D374D78-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Italic;font-size:3.73vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="66579892-F7FB-4387-9638-F25380FD9647"]{position:absolute;width:44.27vw;height:5.07vw;top:12.8vw;left:0vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="66579892-F7FB-4387-9638-F25380FD9647"] [data-text-paragraph-id="66579892-F7FB-4387-9638-F25380FD9647-0"]{white-space:pre-wrap;text-align:left;line-height:5.6vw;}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="66579892-F7FB-4387-9638-F25380FD9647"] [data-text-span-id="66579892-F7FB-4387-9638-F25380FD9647-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Regular;font-size:3.73vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="6C681A76-99BA-45B9-AD64-563FAA623013"]{position:absolute;width:66.67vw;height:12.8vw;top:0vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="6C681A76-99BA-45B9-AD64-563FAA623013"] [data-text-paragraph-id="6C681A76-99BA-45B9-AD64-563FAA623013-0"]{white-space:pre-wrap;text-align:left;line-height:14.4vw;}.card[data-id="E4D65595-1DA8-41B2-B0D7-B2E3586404A9"]>.wrapper>[data-id="084A0385-8768-4CF2-AE02-703268C01C43"]>[data-id="6C681A76-99BA-45B9-AD64-563FAA623013"] [data-text-span-id="6C681A76-99BA-45B9-AD64-563FAA623013-0"]{color:rgba(84, 84, 84, 1);font-family:PlayfairDisplay-Regular;font-size:9.6vw;word-wrap:break-word;letter-spacing:0vw;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]{background-color:rgba(255,255,255,1);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper{position:relative;width:100%;height:100%;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="C0666904-6E33-46CC-AC18-4F2D73EDD541"]{position:absolute;width:100vw;height:calc(var(--ihpx) * 1);top:0;left:0;right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="e2ec743b-44f6-4ef4-ab5e-9afe78246b94"]{position:absolute;width:65.87vw;height:60.53vw;top:calc((var(--ihpx) * 0.2901) - (60.53vw / 2));left:calc((100vw * 0.5) - (65.87vw / 2));right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="e2ec743b-44f6-4ef4-ab5e-9afe78246b94"]>[data-id="9c21ac66-77f1-4688-ab7c-7cf48e2fc779"]{position:absolute;width:65.87vw;height:18.13vw;top:39.73vw;left:0vw;right:auto;bottom:auto;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="e2ec743b-44f6-4ef4-ab5e-9afe78246b94"]>[data-id="9c21ac66-77f1-4688-ab7c-7cf48e2fc779"] [data-text-paragraph-id="9c21ac66-77f1-4688-ab7c-7cf48e2fc779-0"]{white-space:pre-wrap;text-align:center;line-height:15.47vw;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="e2ec743b-44f6-4ef4-ab5e-9afe78246b94"]>[data-id="9c21ac66-77f1-4688-ab7c-7cf48e2fc779"] [data-text-span-id="9c21ac66-77f1-4688-ab7c-7cf48e2fc779-0"]{color:rgba(255, 255, 255, 1);font-family:Montserrat-Regular;font-size:10.4vw;word-wrap:break-word;letter-spacing:0.11vw;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="e2ec743b-44f6-4ef4-ab5e-9afe78246b94"]>[data-id="efcc2c92-aa2b-4924-900f-fe32b99fe630"]{position:absolute;width:40vw;height:40vw;top:calc((var(--ihpx) * 0.1124) - (40vw / 2));left:calc((100vw * 0.3333) - (40vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="e2ec743b-44f6-4ef4-ab5e-9afe78246b94"]>[data-id="44231c55-0c80-4b54-9a0d-a3f98d54b853"]{position:absolute;width:56.53vw;height:5.07vw;top:55.47vw;left:4.53vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="e2ec743b-44f6-4ef4-ab5e-9afe78246b94"]>[data-id="44231c55-0c80-4b54-9a0d-a3f98d54b853"] [data-text-paragraph-id="44231c55-0c80-4b54-9a0d-a3f98d54b853-0"]{white-space:pre-wrap;text-align:center;line-height:5.07vw;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="e2ec743b-44f6-4ef4-ab5e-9afe78246b94"]>[data-id="44231c55-0c80-4b54-9a0d-a3f98d54b853"] [data-text-span-id="44231c55-0c80-4b54-9a0d-a3f98d54b853-0"]{color:rgba(255, 255, 255, 1);font-family:Montserrat-Medium;font-size:3.47vw;word-wrap:break-word;letter-spacing:0.46vw;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]{position:absolute;width:53.33vw;height:21.6vw;top:calc((var(--ihpx) * 0.7354) - (21.6vw / 2));left:calc((100vw * 0.4987) - (53.33vw / 2));right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="44BF805F-10F4-43AD-A72A-6E966CAB765F"]{position:absolute;width:45.87vw;height:4.27vw;top:17.33vw;left:3.47vw;right:auto;bottom:auto;opacity:0.7;mix-blend-mode:normal;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="44BF805F-10F4-43AD-A72A-6E966CAB765F"] [data-text-paragraph-id="44BF805F-10F4-43AD-A72A-6E966CAB765F-0"]{white-space:pre-wrap;text-align:center;line-height:5.07vw;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="44BF805F-10F4-43AD-A72A-6E966CAB765F"] [data-text-span-id="44BF805F-10F4-43AD-A72A-6E966CAB765F-0"]{color:rgba(255, 255, 255, 1);font-family:Montserrat-Medium;font-size:3.47vw;word-wrap:break-word;letter-spacing:0.46vw;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="6C9FFB58-30C4-4D7F-AE90-CE8E9C238305"]{position:absolute;width:13.07vw;height:13.07vw;top:0vw;left:0vw;right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="6C9FFB58-30C4-4D7F-AE90-CE8E9C238305"]>[data-id="B3CA3D2A-923F-43C2-B134-14D713016B49"]{position:absolute;width:13.07vw;height:13.07vw;top:0vw;left:0vw;right:auto;bottom:auto;filter:unset;opacity:0.2021716889880952;mix-blend-mode:normal;transform:translate3d(0, 0, 0);overflow:visible;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="6C9FFB58-30C4-4D7F-AE90-CE8E9C238305"]>[data-id="936CAE08-3BDC-451B-81CB-BA99B478549D"]{position:absolute;width:5.87vw;height:4.8vw;top:4.27vw;left:3.73vw;right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);overflow:visible;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="A37778BE-B939-4C94-A371-F29D96EBDCD3"]{position:absolute;width:13.07vw;height:13.07vw;top:0vw;left:20vw;right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="A37778BE-B939-4C94-A371-F29D96EBDCD3"]>[data-id="C5B4529C-0D62-4C75-8688-F1DBB637BC35"]{position:absolute;width:13.07vw;height:13.07vw;top:0vw;left:0.13vw;right:auto;bottom:auto;filter:unset;opacity:0.2;mix-blend-mode:normal;transform:translate3d(0, 0, 0);overflow:visible;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="A37778BE-B939-4C94-A371-F29D96EBDCD3"]>[data-id="9978C166-EE69-4E5E-977F-9122D54491CF"]{position:absolute;width:6.13vw;height:6.13vw;top:3.2vw;left:3.6vw;right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);overflow:visible;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="67C72AA1-43DB-4183-B252-57C013EFFE4B"]{position:absolute;width:13.07vw;height:13.07vw;top:0vw;left:40.27vw;right:auto;bottom:auto;opacity:1;transform:translate3d(0, 0, 0);}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="67C72AA1-43DB-4183-B252-57C013EFFE4B"]>[data-id="F13A0E9F-4CCA-47E5-8F2C-A384AABC082D"]{position:absolute;width:13.07vw;height:13.07vw;top:0vw;left:0vw;right:auto;bottom:auto;filter:unset;opacity:0.2;mix-blend-mode:normal;transform:translate3d(0, 0, 0);overflow:visible;}.card[data-id="2EA69AE2-03D2-48F6-B667-7CAE3D2C4100"]>.wrapper>[data-id="F93A6F64-E8B8-4E31-B29F-463B573DDEEC"]>[data-id="67C72AA1-43DB-4183-B252-57C013EFFE4B"]>[data-id="05D28152-8045-464E-ADEC-628E1152FBF9"]{position:absolute;width:4.8vw;height:4.8vw;top:3.73vw;left:4.27vw;right:auto;bottom:auto;filter:unset;opacity:1;mix-blend-mode:normal;transform:translate3d(0, 0, 0);overflow:visible;}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]{background-color:rgba(255,255,255,1);}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper{position:relative;width:100%;height:100%;}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="DC62FF00-5089-49B9-BDE8-E1F384A0CF35"]{position:absolute;width:70.4vw;height:21.2vw;top:calc((var(--ihpx) * 0.5206) - (21.2vw / 2));left:calc((100vw * 0.5) - (70.4vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="DC62FF00-5089-49B9-BDE8-E1F384A0CF35"]>[data-id="728C2289-5670-487D-A98E-2D02F962F04F"]{position:absolute;width:70.4vw;height:13.87vw;top:0vw;left:0vw;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="DC62FF00-5089-49B9-BDE8-E1F384A0CF35"]>[data-id="0C02D32A-F053-4445-AB40-73C861E1D879"]{position:absolute;width:66.4vw;height:6.4vw;top:19.87vw;left:2vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="DC62FF00-5089-49B9-BDE8-E1F384A0CF35"]>[data-id="0C02D32A-F053-4445-AB40-73C861E1D879"] [data-text-paragraph-id="0C02D32A-F053-4445-AB40-73C861E1D879-0"]{white-space:pre-wrap;text-align:center;}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="DC62FF00-5089-49B9-BDE8-E1F384A0CF35"]>[data-id="0C02D32A-F053-4445-AB40-73C861E1D879"] [data-text-span-id="0C02D32A-F053-4445-AB40-73C861E1D879-0"]{color:rgba(0, 0, 0, 1);font-family:NunitoSans-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="DC62FF00-5089-49B9-BDE8-E1F384A0CF35"]>[data-id="7B017DE4-2820-4819-8266-F347C361E1E7"]{position:absolute;width:63.47vw;height:6.4vw;top:4.4vw;left:3.87vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="DC62FF00-5089-49B9-BDE8-E1F384A0CF35"]>[data-id="7B017DE4-2820-4819-8266-F347C361E1E7"] [data-text-paragraph-id="7B017DE4-2820-4819-8266-F347C361E1E7-0"]{white-space:pre-wrap;text-align:center;}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="DC62FF00-5089-49B9-BDE8-E1F384A0CF35"]>[data-id="7B017DE4-2820-4819-8266-F347C361E1E7"] [data-text-span-id="7B017DE4-2820-4819-8266-F347C361E1E7-0"]{color:rgba(169, 169, 169, 1);font-family:PlayfairDisplay-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="a7b885c4-e8c8-4e22-9e92-bd6a1ce73256"]{position:absolute;width:40vw;height:30.13vw;top:calc((var(--ihpx) * 0.2586) - (30.13vw / 2));left:calc((100vw * 0.4987) - (40vw / 2));right:auto;bottom:auto;filter:unset;opacity:0.5;transform:translate3d(0, 0, 0);}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="76907f0e-5af8-4593-9191-8286888c4268"]{position:absolute;width:6.4vw;height:6.4vw;top:calc((var(--ihpx) * 0.069) - (6.4vw / 2));left:calc((100vw * 0.4987) - (6.4vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="BDAC0717-B745-48F6-AE12-CE9D66A5DB60"]{position:absolute;width:70.67vw;height:19.2vw;top:calc((var(--ihpx) * 0.8891) - (19.2vw / 2));left:calc((100vw * 0.5053) - (70.67vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="BDAC0717-B745-48F6-AE12-CE9D66A5DB60"] [data-text-paragraph-id="BDAC0717-B745-48F6-AE12-CE9D66A5DB60-0"]{white-space:pre-wrap;text-align:left;}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="BDAC0717-B745-48F6-AE12-CE9D66A5DB60"] [data-text-span-id="BDAC0717-B745-48F6-AE12-CE9D66A5DB60-0"]{color:rgba(68, 68, 68, 1);font-family:PlayfairDisplay-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="75b32d68-c1a3-40fe-a2f8-7d6435026c9c"]>.wrapper>[data-id="BDAC0717-B745-48F6-AE12-CE9D66A5DB60"] [data-text-span-id="BDAC0717-B745-48F6-AE12-CE9D66A5DB60-1"]{color:rgba(68, 68, 68, 1);font-family:PlayfairDisplay-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]{background-color:rgba(255,255,255,1);}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper{position:relative;width:100%;height:100%;}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="cbeffed7-61c5-4d85-a2be-2f1a6687b212"]{position:absolute;width:34.13vw;height:34.13vw;top:calc((var(--ihpx) * 0.2354) - (34.13vw / 2));left:calc((100vw * 0.4987) - (34.13vw / 2));right:auto;bottom:auto;filter:unset;opacity:0.51;transform:translate3d(0, 0, 0);}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="d41774fe-abb7-478f-a2ad-1254eae744d9"]{position:absolute;width:70.4vw;height:21.07vw;top:calc((var(--ihpx) * 0.5195) - (21.07vw / 2));left:calc((100vw * 0.4987) - (70.4vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="d41774fe-abb7-478f-a2ad-1254eae744d9"]>[data-id="4bd6e643-4783-4b3f-9be8-ab2d765aa262"]{position:absolute;width:70.4vw;height:13.87vw;top:0vw;left:0vw;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="d41774fe-abb7-478f-a2ad-1254eae744d9"]>[data-id="e334d4f3-07ec-4215-b5b5-610af3a5f6a4"]{position:absolute;width:66.4vw;height:5.87vw;top:19.87vw;left:2vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="d41774fe-abb7-478f-a2ad-1254eae744d9"]>[data-id="e334d4f3-07ec-4215-b5b5-610af3a5f6a4"] [data-text-paragraph-id="e334d4f3-07ec-4215-b5b5-610af3a5f6a4-0"]{white-space:pre-wrap;text-align:center;}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="d41774fe-abb7-478f-a2ad-1254eae744d9"]>[data-id="e334d4f3-07ec-4215-b5b5-610af3a5f6a4"] [data-text-span-id="e334d4f3-07ec-4215-b5b5-610af3a5f6a4-0"]{color:rgba(0, 0, 0, 1);font-family:NunitoSans-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="d41774fe-abb7-478f-a2ad-1254eae744d9"]>[data-id="2ad65a2d-fb97-4637-bab9-70b0196be206"]{position:absolute;width:63.47vw;height:6.4vw;top:4.4vw;left:3.87vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="d41774fe-abb7-478f-a2ad-1254eae744d9"]>[data-id="2ad65a2d-fb97-4637-bab9-70b0196be206"] [data-text-paragraph-id="2ad65a2d-fb97-4637-bab9-70b0196be206-0"]{white-space:pre-wrap;text-align:center;}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="d41774fe-abb7-478f-a2ad-1254eae744d9"]>[data-id="2ad65a2d-fb97-4637-bab9-70b0196be206"] [data-text-span-id="2ad65a2d-fb97-4637-bab9-70b0196be206-0"]{color:rgba(169, 169, 169, 1);font-family:PlayfairDisplay-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="763b47bd-222c-4a15-9648-51220ef32128"]{position:absolute;width:70.67vw;height:19.2vw;top:calc((var(--ihpx) * 0.8891) - (19.2vw / 2));left:calc((100vw * 0.5053) - (70.67vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="763b47bd-222c-4a15-9648-51220ef32128"] [data-text-paragraph-id="763b47bd-222c-4a15-9648-51220ef32128-0"]{white-space:pre-wrap;text-align:left;}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="763b47bd-222c-4a15-9648-51220ef32128"] [data-text-span-id="763b47bd-222c-4a15-9648-51220ef32128-0"]{color:rgba(68, 68, 68, 1);font-family:PlayfairDisplay-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card[data-id="b058e5cd-e6a5-42a8-880d-1cce50687810"]>.wrapper>[data-id="763b47bd-222c-4a15-9648-51220ef32128"] [data-text-span-id="763b47bd-222c-4a15-9648-51220ef32128-1"]{color:rgba(68, 68, 68, 1);font-family:PlayfairDisplay-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]{background-color:rgba(50,50,50,1);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper{position:relative;width:100%;min-height:203vw;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="3e1ec960-ca18-46ab-bbeb-04e734c86915"]{position:absolute;width:54.67vw;height:57.33vw;top:calc((var(--ihpx) * 0.2376) - (57.33vw / 2));left:calc((100vw * 0.5) - (54.67vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="97e739c6-23bf-4b7d-88c9-a01f24457b8a"]{position:absolute;width:70.67vw;height:19.2vw;top:calc((var(--ihpx) * 0.4783) - (19.2vw / 2));left:calc((100vw * 0.5) - (70.67vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="97e739c6-23bf-4b7d-88c9-a01f24457b8a"] [data-text-paragraph-id="97e739c6-23bf-4b7d-88c9-a01f24457b8a-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="97e739c6-23bf-4b7d-88c9-a01f24457b8a"] [data-text-span-id="97e739c6-23bf-4b7d-88c9-a01f24457b8a-0"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:5.33vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="97e739c6-23bf-4b7d-88c9-a01f24457b8a"] [data-text-span-id="97e739c6-23bf-4b7d-88c9-a01f24457b8a-1"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]{position:absolute;width:70.13vw;height:24.53vw;top:calc((var(--ihpx) * 0.6147) - (24.53vw / 2));left:calc((100vw * 0.4973) - (70.13vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="6A7A0597-AFDF-4352-AFE6-D97A1CAD0476"]{position:absolute;width:70.13vw;height:0.27vw;top:calc((var(--ihpx) * 0.0007) - (0.27vw / 2));left:0;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="97F55116-D9E3-4535-A8A1-129E53611888"]{position:absolute;width:64.27vw;height:4.53vw;top:2.4vw;left:5.6vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="97F55116-D9E3-4535-A8A1-129E53611888"] [data-text-paragraph-id="97F55116-D9E3-4535-A8A1-129E53611888-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="97F55116-D9E3-4535-A8A1-129E53611888"] [data-text-span-id="97F55116-D9E3-4535-A8A1-129E53611888-0"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="a00382aa-6b5e-43cc-8639-30eae6f74079"]{position:absolute;width:64.27vw;height:5.07vw;top:8.27vw;left:5.6vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="a00382aa-6b5e-43cc-8639-30eae6f74079"] [data-text-paragraph-id="a00382aa-6b5e-43cc-8639-30eae6f74079-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="a00382aa-6b5e-43cc-8639-30eae6f74079"] [data-text-span-id="a00382aa-6b5e-43cc-8639-30eae6f74079-0"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="f67cad34-5bb4-4301-b3d6-15095da323d4"]{position:absolute;width:64.27vw;height:5.07vw;top:14.13vw;left:5.6vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="f67cad34-5bb4-4301-b3d6-15095da323d4"] [data-text-paragraph-id="f67cad34-5bb4-4301-b3d6-15095da323d4-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="f67cad34-5bb4-4301-b3d6-15095da323d4"] [data-text-span-id="f67cad34-5bb4-4301-b3d6-15095da323d4-0"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="08207910-1168-43bb-b4eb-190c0153e5fa"]{position:absolute;width:64.27vw;height:5.07vw;top:19.47vw;left:5.6vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="08207910-1168-43bb-b4eb-190c0153e5fa"] [data-text-paragraph-id="08207910-1168-43bb-b4eb-190c0153e5fa-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="08207910-1168-43bb-b4eb-190c0153e5fa"] [data-text-span-id="08207910-1168-43bb-b4eb-190c0153e5fa-0"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="5bc1c9c7-b91b-450d-bc18-68eeaac58f9f"]{position:absolute;width:2.4vw;height:2.4vw;top:3.73vw;left:1.33vw;right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="18525d17-aa54-4383-8518-64c86f1b080f"]{position:absolute;width:2.4vw;height:2.4vw;top:9.87vw;left:1.33vw;right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="f5b891c5-c3b5-4f60-9932-623b99377c42"]{position:absolute;width:2.4vw;height:2.4vw;top:15.73vw;left:1.33vw;right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="30057435-0317-4597-95CD-3B766C66DE12"]>[data-id="bb9a605d-a839-4b41-9423-1f6bc6d99d8f"]{position:absolute;width:2.4vw;height:2.4vw;top:21.07vw;left:1.33vw;right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="701751A7-6E4A-4501-8383-DFC9FB94771A"]{position:absolute;width:80.53vw;height:17.33vw;top:calc((var(--ihpx) * 0.7804) - (17.33vw / 2));left:calc((100vw * 0.5013) - (80.53vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="701751A7-6E4A-4501-8383-DFC9FB94771A"]>[data-id="D0FDC425-4D8E-4988-AC6B-ABC77669F84D"]{position:absolute;width:80.53vw;height:17.33vw;top:0vw;left:0vw;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="701751A7-6E4A-4501-8383-DFC9FB94771A"]>[data-id="F63C1D16-16CA-46CD-A35F-FB73CCE27F31"]{position:absolute;width:12vw;height:12vw;top:2.4vw;left:5.07vw;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="701751A7-6E4A-4501-8383-DFC9FB94771A"]>[data-id="34b36616-c8d4-4f55-8aa3-78f5e3421a90"]{position:absolute;width:54.67vw;height:14.93vw;top:1.07vw;left:18.67vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="701751A7-6E4A-4501-8383-DFC9FB94771A"]>[data-id="34b36616-c8d4-4f55-8aa3-78f5e3421a90"] [data-text-paragraph-id="34b36616-c8d4-4f55-8aa3-78f5e3421a90-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="701751A7-6E4A-4501-8383-DFC9FB94771A"]>[data-id="34b36616-c8d4-4f55-8aa3-78f5e3421a90"] [data-text-span-id="34b36616-c8d4-4f55-8aa3-78f5e3421a90-0"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="701751A7-6E4A-4501-8383-DFC9FB94771A"]>[data-id="34b36616-c8d4-4f55-8aa3-78f5e3421a90"] [data-text-span-id="34b36616-c8d4-4f55-8aa3-78f5e3421a90-1"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="701751A7-6E4A-4501-8383-DFC9FB94771A"]>[data-id="34b36616-c8d4-4f55-8aa3-78f5e3421a90"] [data-text-span-id="34b36616-c8d4-4f55-8aa3-78f5e3421a90-2"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:2.67vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="a5a9edf4-3be0-406a-80e0-0c8cea9bfada"]{position:absolute;width:80.53vw;height:17.33vw;top:calc((var(--ihpx) * 0.9003) - (17.33vw / 2));left:calc((100vw * 0.5013) - (80.53vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="a5a9edf4-3be0-406a-80e0-0c8cea9bfada"]>[data-id="6d3fe0f8-013b-4ed5-9e05-79a87a871e22"]{position:absolute;width:80.53vw;height:17.33vw;top:0vw;left:0vw;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="a5a9edf4-3be0-406a-80e0-0c8cea9bfada"]>[data-id="40c97664-621b-4221-bcd4-7f057c68226e"]{position:absolute;width:12vw;height:12vw;top:2.4vw;left:5.07vw;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="a5a9edf4-3be0-406a-80e0-0c8cea9bfada"]>[data-id="135b6f51-f680-4c98-9afc-93f6eead0774"]{position:absolute;width:54.67vw;height:14.93vw;top:1.07vw;left:18.67vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="a5a9edf4-3be0-406a-80e0-0c8cea9bfada"]>[data-id="135b6f51-f680-4c98-9afc-93f6eead0774"] [data-text-paragraph-id="135b6f51-f680-4c98-9afc-93f6eead0774-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="a5a9edf4-3be0-406a-80e0-0c8cea9bfada"]>[data-id="135b6f51-f680-4c98-9afc-93f6eead0774"] [data-text-span-id="135b6f51-f680-4c98-9afc-93f6eead0774-0"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="a5a9edf4-3be0-406a-80e0-0c8cea9bfada"]>[data-id="135b6f51-f680-4c98-9afc-93f6eead0774"] [data-text-span-id="135b6f51-f680-4c98-9afc-93f6eead0774-1"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="a5a9edf4-3be0-406a-80e0-0c8cea9bfada"]>[data-id="135b6f51-f680-4c98-9afc-93f6eead0774"] [data-text-span-id="135b6f51-f680-4c98-9afc-93f6eead0774-2"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:2.67vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="e4e4ac92-2983-4703-bbf0-503f6cd55297"]{position:absolute;width:80.53vw;height:17.33vw;top:calc((var(--ihpx) * 1.0202) - (17.33vw / 2));left:calc((100vw * 0.5013) - (80.53vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="e4e4ac92-2983-4703-bbf0-503f6cd55297"]>[data-id="ff7f9184-d229-4211-9154-80e658552cfa"]{position:absolute;width:80.53vw;height:17.33vw;top:0vw;left:0vw;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="e4e4ac92-2983-4703-bbf0-503f6cd55297"]>[data-id="72d8f390-54fc-41dc-b1c2-56508bf0bfca"]{position:absolute;width:12vw;height:12vw;top:2.4vw;left:5.07vw;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="e4e4ac92-2983-4703-bbf0-503f6cd55297"]>[data-id="adc05a86-4d44-4fcf-93ee-fea87beaa114"]{position:absolute;width:54.67vw;height:14.93vw;top:1.07vw;left:18.67vw;right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="e4e4ac92-2983-4703-bbf0-503f6cd55297"]>[data-id="adc05a86-4d44-4fcf-93ee-fea87beaa114"] [data-text-paragraph-id="adc05a86-4d44-4fcf-93ee-fea87beaa114-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="e4e4ac92-2983-4703-bbf0-503f6cd55297"]>[data-id="adc05a86-4d44-4fcf-93ee-fea87beaa114"] [data-text-span-id="adc05a86-4d44-4fcf-93ee-fea87beaa114-0"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="e4e4ac92-2983-4703-bbf0-503f6cd55297"]>[data-id="adc05a86-4d44-4fcf-93ee-fea87beaa114"] [data-text-span-id="adc05a86-4d44-4fcf-93ee-fea87beaa114-1"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="e4e4ac92-2983-4703-bbf0-503f6cd55297"]>[data-id="adc05a86-4d44-4fcf-93ee-fea87beaa114"] [data-text-span-id="adc05a86-4d44-4fcf-93ee-fea87beaa114-2"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:2.67vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="ab1ad13b-4d31-4f40-9d67-cc50287ee465"]>.wrapper>[data-id="49f0ba87-c355-402c-aef2-1c890e703c92"]{position:absolute;width:4.27vw;height:4.27vw;top:calc((var(--ihpx) * 0.7346) - (4.27vw / 2));left:calc((100vw * 0.848) - (4.27vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]{background-color:rgba(255,255,255,1);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper{position:relative;width:100%;min-height:356vw;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="9dff98e3-fd48-48c4-8123-8c66eedeef5a"]{position:absolute;width:100vw;height:56.53vw;top:calc((var(--ihpx) * 0.1589) - (56.53vw / 2));left:calc((100vw * 0.5) - (100vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="A763D189-0ED7-426A-88AF-23ABFC34868D"]{position:absolute;width:100vw;height:9.6vw;top:calc((var(--ihpx) * 0.3448) - (9.6vw / 2));left:0;right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="83263073-B4D3-4FB2-B509-4587942F80AA"]{position:absolute;width:94.93vw;height:6.13vw;top:calc((var(--ihpx) * 0.3471) - (6.13vw / 2));left:calc((100vw * 0.4987) - (94.93vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="83263073-B4D3-4FB2-B509-4587942F80AA"] [data-text-paragraph-id="83263073-B4D3-4FB2-B509-4587942F80AA-0"]{white-space:pre-wrap;text-align:center;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="83263073-B4D3-4FB2-B509-4587942F80AA"] [data-text-span-id="83263073-B4D3-4FB2-B509-4587942F80AA-0"]{color:rgba(241, 241, 241, 1);font-family:Montserrat-Regular;font-size:5.33vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="866f19e5-cd4f-4d1d-82fd-b3b661703af3"]{position:absolute;width:70.67vw;height:12.8vw;top:calc((var(--ihpx) * 0.4318) - (12.8vw / 2));left:calc((100vw * 0.3987) - (70.67vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="866f19e5-cd4f-4d1d-82fd-b3b661703af3"] [data-text-paragraph-id="866f19e5-cd4f-4d1d-82fd-b3b661703af3-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="866f19e5-cd4f-4d1d-82fd-b3b661703af3"] [data-text-span-id="866f19e5-cd4f-4d1d-82fd-b3b661703af3-0"]{color:rgba(65, 65, 65, 1);font-family:Montserrat-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="1ad85705-5594-4c81-8c98-d5c5c861e588"]{position:absolute;width:12.27vw;height:12.8vw;top:calc((var(--ihpx) * 0.4303) - (12.8vw / 2));left:calc((100vw * 0.888) - (12.27vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="06eef167-959f-4eda-a519-67dca5c06cb8"]{position:absolute;width:70.67vw;height:85.33vw;top:calc((var(--ihpx) * 1.3253) - (85.33vw / 2));left:calc((100vw * 0.5) - (70.67vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="06eef167-959f-4eda-a519-67dca5c06cb8"] [data-text-paragraph-id="06eef167-959f-4eda-a519-67dca5c06cb8-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="06eef167-959f-4eda-a519-67dca5c06cb8"] [data-text-span-id="06eef167-959f-4eda-a519-67dca5c06cb8-0"]{color:rgba(65, 65, 65, 1);font-family:Montserrat-Regular;font-size:3.2vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="c07e9060-6fb4-4cd6-b485-1894f98e0c8f"]{position:absolute;width:70.67vw;height:22.67vw;top:calc((var(--ihpx) * 1.0097) - (22.67vw / 2));left:calc((100vw * 0.5) - (70.67vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="c07e9060-6fb4-4cd6-b485-1894f98e0c8f"] [data-text-paragraph-id="c07e9060-6fb4-4cd6-b485-1894f98e0c8f-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="c07e9060-6fb4-4cd6-b485-1894f98e0c8f"] [data-text-span-id="c07e9060-6fb4-4cd6-b485-1894f98e0c8f-0"]{color:rgba(65, 65, 65, 1);font-family:Montserrat-Regular;font-size:4.27vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="6c437825-b5f6-4f1e-a688-d8bd26c8561b"]{position:absolute;width:100.8vw;height:56.53vw;top:calc((var(--ihpx) * 0.7496) - (56.53vw / 2));left:calc((100vw * 0.504) - (100.8vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="7D60A956-A365-471C-A5CF-6910A4E60136"]{position:absolute;width:77.87vw;height:9.87vw;top:calc((var(--ihpx) * 0.527) - (9.87vw / 2));left:calc((100vw * 0.4987) - (77.87vw / 2));right:auto;bottom:auto;filter:unset;opacity:1;transform:translate3d(0, 0, 0);overflow:visible;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="1e9bd735-dbaa-4879-944a-7cfd5250cb16"]{position:absolute;width:57.33vw;height:6.13vw;top:calc((var(--ihpx) * 0.527) - (6.13vw / 2));left:calc((100vw * 0.4147) - (57.33vw / 2));right:auto;bottom:auto;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="1e9bd735-dbaa-4879-944a-7cfd5250cb16"] [data-text-paragraph-id="1e9bd735-dbaa-4879-944a-7cfd5250cb16-0"]{white-space:pre-wrap;text-align:left;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="1e9bd735-dbaa-4879-944a-7cfd5250cb16"] [data-text-span-id="1e9bd735-dbaa-4879-944a-7cfd5250cb16-0"]{color:rgba(255, 255, 255, 1);font-family:Montserrat-Regular;font-size:5.33vw;word-wrap:break-word;letter-spacing:0;}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="7eb3c7b0-ac01-43e7-93bf-2eaa359151ce"]{position:absolute;width:6.4vw;height:6.4vw;top:calc((var(--ihpx) * 0.5277) - (6.4vw / 2));left:calc((100vw * 0.8293) - (6.4vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}.card.scrolling[data-id="d7130849-99d5-4f4f-b876-7f8bc78ef16c"]>.wrapper>[data-id="742b496c-efcd-4d56-a332-0606a889525e"]{position:absolute;width:4vw;height:4vw;top:calc((var(--ihpx) * 0.0262) - (4vw / 2));left:calc((100vw * 0.9427) - (4vw / 2));right:auto;bottom:auto;filter:unset;transform:translate3d(0, 0, 0);}',
            isBlank: !0,
            modified: !1,
            settings: {
                splashCardId: "0378A657-A7D0-4AA9-88D9-8E2B7C2F3B55",
                backgroundColor: "rgba(53,55,53,1)"
            },
            dataVersion: 4,
            mediaQueries: {
                mobile: {
                    width: 375,
                    height: 667,
                    mediaType: "all",
                    mediaFeatures: {}
                }
            },
            dataVersionInitial: 4,
            fonts: [{
                postscriptName: "PlayfairDisplay-Regular",
                src: "https://fonts.gstatic.com/s/playfairdisplay/v18/nuFiD-vYSZviVYUb_rj3ij__anPXDTzYgEM86xQ.woff2"
            }, {
                postscriptName: "Montserrat-Regular",
                src: "https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WlhyyTh89Y.woff2"
            }, {
                postscriptName: "Montserrat-Medium",
                src: "https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_ZpC3gnD_vx3rCs.woff2"
            }, {
                postscriptName: "PlayfairDisplay-Italic",
                src: "https://fonts.gstatic.com/s/playfairdisplay/v18/nuFkD-vYSZviVYUb_rj3ij__anPXDTnogkk7yRZrPA.woff2"
            }, {
                postscriptName: "NunitoSans-Regular",
                src: "https://fonts.gstatic.com/s/nunitosans/v5/pe0qMImSLYBIv1o4X1M8cce9I9tAcVwo.woff2"
            }]
        }
    }, function(t, e, i) {
        var a = t[1];
    
        function o(t, e) {
            a.call(this), this.q = null, this.Tt = e, this.Rt = t, this.Bt = 0, this.Ft = this.Gt.bind(this), this.Ht = this.Mt.bind(this), this.Qt = {
                keyLeft: "left",
                keyRight: "right",
                keyUp: "up",
                keyDown: "down"
            }
        }
        for (var s in a.prototype) o.prototype[s] = a.prototype[s];
        o.prototype.attach = function(t) {
            this.q = t, this.q.addEventListener("keydown", this.Ft), this.q.addEventListener("keyup", this.Ht)
        }, o.prototype.detach = function() {
            this.q.removeEventListener("keydown", this.Ft), this.q.removeEventLIstener("keyup", this.Ht), this.q = null
        }, o.prototype.Gt = function(t) {
            if (!this.Bt) {
                var e, i = this.Rt.getState(),
                    a = this.Rt.getTargetState(),
                    o = this.Rt.getProgress(),
                    s = this.Tt.get(i),
                    r = null;
                if (37 === t.keyCode) r = "keyLeft";
                else if (32 === t.keyCode) r = "keyRight";
                else if (39 === t.keyCode) r = "keyRight";
                else if (38 === t.keyCode) r = "keyUp";
                else {
                    if (40 !== t.keyCode) return;
                    r = "keyDown"
                }
                this.Bt = t.keyCode, a !== (e = s.getTargetId(r)) && e && this.trigger("stateChange", {
                    state: i,
                    target: e,
                    progress: o,
                    settleOnTarget: 1,
                    edge: this.Qt[r],
                    gesture: r,
                    keyNavigation: !0
                })
            }
        }, o.prototype.Mt = function(t) {
            t.keyCode === this.Bt && (this.Bt = 0)
        }, i.exports = o
    }, function(t, e, i) {
        var a = t[1];
    
        function o(t, e) {
            a.call(this), this.Rt = t, this.Tt = e, this.q = null, this.Vt = this.Xt.bind(this), this.Yt = this.Zt.bind(this), this.$t = this.zi.bind(this), this.Di = 1, this.y = "", this.St = "", this.Nt = 0, this.Ci = 0, this.Bi = 0, this.Gi = 0, this.Hi = 0, this.Ii = 2, this.Ji = null, this.Ki = !1, this.scrolling = !1, this.inTransition = !1, this.Qt = {
                wheelUp: "up",
                wheelDown: "down"
            }
        }
        for (var s in a.prototype) o.prototype[s] = a.prototype[s];
        o.prototype.attach = function(t) {
            this.q = t, this.q.addEventListener("wheel", this.Yt), window.addEventListener("resize", this.$t), this.$t()
        }, o.prototype.detach = function() {
            this.q.removeEventListener("wheel", this.Yt), window.removeEventListener("resize", this.$t), this.q = null
        }, o.prototype.Li = function() {
            var t = {
                state: this.y,
                target: this.St,
                progress: 0,
                settleOnTarget: 1,
                edge: this.Qt[this.Ji],
                gesture: this.Ji
            };
            this.trigger("stateChange", t)
        }, o.prototype.Ni = function(t) {
            this.y = this.Rt.getState(), this.St = this.Rt.getTargetState(), this.Nt = this.Rt.getProgress(), this.Gi = 0, this.Hi = 0, this.Ji = null, this.scrolling = !1, this.Ki = !1
        }, o.prototype.zi = function() {
            this.Di = window.innerHeight
        }, o.prototype.Pi = function() {
            this.Ki = !0
        }, o.prototype.Zt = function(t) {
            this.Bi = this.Ci, this.Ci = t.timeStamp;
            var e = Math.abs(this.Ci - this.Bi) > 50;
            if (!this.dt && e && this.Ni(), !(this.dt || this.Hi > this.Ii || this.Ci === this.Bi || this.scrolling)) {
                this.Gi = -t.deltaY, this.Hi++, window.safari && this.q.addEventListener("gesturechange", this.Pi(), {
                    once: !0
                });
                var i = this.Gi;
                0 === t.deltaMode && i < -40 && (i = -40), 0 === t.deltaMode && i > 40 && (i = 40), 1 === t.deltaMode ? i *= 40 : 2 === t.deltaMode && (i *= this.Di);
                var a = this.y;
                if (this.St === a) {
                    var o = i < 0,
                        s = this.Tt.get(this.y);
                    this.Ji = o ? "wheelDown" : "wheelUp";
                    var r = s.getTargetId(this.Ji);
                    if (!r && 0 !== r) return;
                    this.St = r
                }
                this.Ji && (this.Hi > this.Ii || this.Ki) && this.Vt()
            }
        }, o.prototype.Xt = function() {
            this.inTransition = !0, this.Li()
        }, i.exports = o
    }, function(t, e, i) {
        var a = t[1];
    
        function o(t, e) {
            let p = t || Object;
            a.call(this), this.vs = 50, this.ms = null, this.ds = !1, this.Xs = null, this.Ys = 0, this.Es = 0, this.ei = 0, this.gs = 0, this.Ds = 0, this.ws = 0, this.qs = 0, this.bs = 0, e || (e = {}), e.only && (this.ms = e.only);
            var i = this.js.bind(this),
                o = this.I.bind(this);
                console.log(t);
            p.addEventListener("touchstart", this.ks.bind(this)), p.addEventListener("touchmove", this.xs.bind(this)), t.addEventListener("touchend", i), p.addEventListener("touchcancel", i), t.addEventListener("mousedown", this.x.bind(this)), p.addEventListener("mousemove", this.ys.bind(this)), p.addEventListener("mouseup", o), p.addEventListener("mouseleave", o)
        }
        for (var s in a.prototype) o.prototype[s] = a.prototype[s];
        o.prototype.ss = function(t, e) {
            this.Ys = this.gs = t.screenX, this.Es = this.Ds = t.screenY, this.ei = Date.now(), this.ws = this.ei, this.qs = 0, this.bs = 0, this.emit("start", {
                originalEvent: e,
                clientX: t.clientX,
                clientY: t.clientY
            })
        }, o.prototype.zs = function(t, e) {
            var i = Date.now(),
                a = i - this.ws,
                o = t.screenX - this.gs,
                s = t.screenY - this.Ds;
            a < 2 && (a = 2), this.qs = (t.screenX - this.gs) / a, this.bs = (t.screenY - this.Ds) / a, this.gs = t.screenX, this.Ds = t.screenY, this.ws = i, this.emit("drag", {
                originalEvent: e,
                deltaX: this.gs - this.Ys,
                deltaY: this.Ds - this.Es,
                frameDeltaX: o,
                frameDeltaY: s,
                veloX: this.qs,
                veloY: this.bs
            })
        }, o.prototype.us = function(t, e) {
            var i = this.gs - this.Ys,
                a = this.Ds - this.Es,
                o = Date.now() - this.ei;
            !this.ms || this.ms(i, a, o) ? this.emit("swipe", {
                originalEvent: e,
                deltaX: i,
                deltaY: a,
                veloX: this.qs,
                veloY: this.bs,
                time: o
            }) : this.emit("cancel", {
                originalEvent: e,
                deltaX: i,
                deltaY: a,
                veloX: this.qs,
                veloY: this.bs,
                time: o
            }), this.Xs = null, this.Ys = this.gs = 0, this.Es = this.Ds = 0, this.qs = 0, this.bs = 0
        }, o.prototype.x = function(t) {
            this.ds || this.Xs || (this.Xs = -1, this.ss(t, t))
        }, o.prototype.ys = function(t) {
            -1 === this.Xs && this.zs(t, t)
        }, o.prototype.I = function(t) {
            -1 === this.Xs && this.us(t, t)
        }, o.prototype.ks = function(t) {
            this.ds = !0;
            var e = t.targetTouches[0];
            this.Xs = e.identifier, this.ss(e, t)
        }, o.prototype.xs = function(t) {
            for (var e = 0; e < t.changedTouches.length; e++)
                if (t.changedTouches[e].identifier === this.Xs) return this.zs(t.changedTouches[e], t)
        }, o.prototype.js = function(t) {
            for (var e = 0; e < t.changedTouches.length; e++)
                if (t.changedTouches[e].identifier === this.Xs) return this.us(t.changedTouches[e], t)
        }, i.exports = o
    }, function(t, e, i) {
        var a = t[1],
            o = t[31];
    
        function s(t, e) {
            a.call(this), this.q = null, this.Rt = t, this.Tt = e, this.Nt = 0, this.Ri = -1, this.Qi = !0, this.Ji = null, this.Vi = null, this.Wi = 1, this.Di = 1, this.dt = !1, this.Xi = !1, this.Yi = null, this.y = this.Rt.getState(), this.St = this.Rt.getTargetState(), this.isDesktop = !1, this.Zi = !1, this._t = !1, this.ts = this.ss.bind(this), this.hs = this.ns.bind(this), this.es = this.us.bind(this)
        }
        for (var r in a.prototype) s.prototype[r] = a.prototype[r];
        s.prototype.attach = function(t) {
            this.q = t, this.rs = new o(this.q), this.rs.on("start", this.ts), this.rs.on("drag", this.hs), this.rs.on("swipe", this.es)
        }, s.prototype.detach = function() {
            this.q && (this.rs.off("start", this.ts), this.rs.off("drag", this.hs), this.rs.off("swipe", this.es), this.rs = null, this.q = null)
        }, s.prototype.resize = function(t, e) {
            this.Wi = t, this.Di = e
        }, s.prototype.ls = function() {
            if (this.St === this.y && this.Ji || this.St < 0) {
                var t = this.Tt.get(this.y);
                this.fs(t)
            }
        }, s.prototype.as = function(t) {
            var e;
            if (this.Nt > 1 && (this.y = this.St, e = this.Tt.get(this.y), this.fs(e), this.Nt = 1, e)) {
                const t = e.getTargetId(this.Ji);
                t && "Empty" !== t ? this.Nt -= 1 : this.Nt = 1
            }
            this.Nt < 0 && (e = this.Tt.get(this.y), this.Ji = null, this.os(t), this.fs(e), this.Nt = Math.abs(this.Nt))
        }, s.prototype.fs = function(t) {
            this.St = t ? t.getTargetId(this.Ji) : null, "Empty" === this.St && (this.St = null), this.li = t ? t.getGestureEdge(this.Ji) : null
        }, s.prototype.os = function(t) {
            this.Ji || (Math.abs(t.deltaX) > Math.abs(t.deltaY) && !this.Ji ? (t.veloX < 0 && (this.Ji = "swipeLeft"), t.veloX > 0 && (this.Ji = "swipeRight")) : Math.abs(t.deltaY) > Math.abs(t.deltaX) && !this.Ji && (t.veloY < 0 && (this.Ji = "swipeUp"), t.veloY > 0 && (this.Ji = "swipeDown")), this.Ji && (this.ls(), this.Ji.includes("Left") || this.Ji.includes("Up") ? this.Qi = !0 : this.Qi = !1))
        }, s.prototype.ps = function(t) {
            for (var e = t; e && e !== document.body;) {
                if ("scroll" === window.getComputedStyle(e).overflowY && e.scrollHeight > e.clientHeight) return e;
                e = e.parentNode
            }
            return null
        }, s.prototype.cs = function(t) {
            if (this.Yi && !this.Xi) {
                var e = !1,
                    i = this.Yi.scrollTop,
                    a = (this.Yi.scrollHeight || this.Yi.firstElementChild.clientHeight) - this.Yi.clientHeight;
                e |= i <= 0 && t.frameDeltaY < 0, e |= i >= 1 && i < a, e |= i >= a && t.frameDeltaY > 0, this.Xi = e
            }
        }, s.prototype.ss = function(t) {
            this.y = this.Rt.getState(), this.St = this.Rt.getTargetState(), this.Nt = this.Rt.getProgress(), this.Ri = -1, this.dt = this.Nt > 0, this.isDesktop || (this.Yi = this.ps(t.originalEvent.target)), this.Xi = !1, this.dt && this.y !== this.St || (this.Ji = null), this.Vi = t.clientX && t.clientX < this.Wi / 3 ? "tapLeft" : "tapRight", this.Li()
        }, s.prototype.ns = function(t) {
            if (this.os(t), this.Ji && (this.Zi = this.Ji.includes("Left") || this.Ji.includes("Right"), this._t = this.Ji.includes("Up") || this.Ji.includes("Down"), this.cs(t), !this.Xi || !this._t)) {
                var e = this.Zi ? t.frameDeltaX : t.frameDeltaY,
                    i = this.Zi ? this.Di : this.Wi;
                this.Nt += (this.Qi ? -e : e) / i, this.as(t), this.St && t.originalEvent.preventDefault(), this.Li()
            }
        }, s.prototype.us = function(t) {
            if (this.cs(t), !this.Xi || !this._t) {
                var e = "swipeLeft" === this.Ji || "swipeRight" === this.Ji ? t.deltaX : t.deltaY,
                    i = "swipeLeft" === this.Ji || "swipeRight" === this.Ji ? t.veloX : t.veloY,
                    a = e * (this.Qi ? -1 : 1),
                    o = i * (this.Qi ? -1 : 1),
                    s = t.veloX * t.veloX + t.veloY * t.veloY;
                a > 50 || o > .5 || t.time < 300 && s < .5 && this.dt ? this.Ri = 1 : 0 === e && 0 === i && 0 === this.Nt ? (this.Ri = 1, this.Ji = this.Vi, this.ls()) : this.Ri = 0, this.St || (this.Ri = 0), this.Li()
            }
        }, s.prototype.Li = function() {
            var t = {
                progress: this.Nt,
                settleOnTarget: this.Ri,
                gesture: this.Ji,
                state: this.y,
                target: this.St,
                edge: this.li
            };
            this.trigger("stateChange", t)
        }, i.exports = s
    }, function(t, e, i) {
        var a = t[1];
    
        function o() {
            a.call(this), this.tn = {}, this.in = {}
        }
        for (var s in a.prototype) o.prototype[s] = a.prototype[s];
        o.prototype.get = function(t) {
            return this.tn[t]
        }, o.prototype.forEach = function(t) {
            for (var e in this.tn) t(this.tn[e], e)
        }, o.prototype.has = function(t) {
            return t in this.tn
        }, o.prototype.add = function(t, e) {
            return this.replace(t, e), e
        }, o.prototype.replace = function(t, e, i) {
            var a = this.tn[t];
            return e && (this.tn[t] = e), this.emit("replace", {
                id: t,
                value: e
            }), a
        }, o.prototype.remove = function(t) {
            var e = this.tn[t];
            return delete this.tn[t], delete this.in[t], this.emit("remove", {
                id: t,
                value: e
            }), e
        }, i.exports = o
    }, function(t, e, i) {
        var a = t[0],
            o = t[5];
    
        function s(t) {
            if (!t.cardSet) return !1;
            if (this.Tt = t.cardSet, this.ie = this.Tt.get(t.elementIds.start), this.te = this.Tt.get(t.elementIds.end), !this.te || this.te === this.ie) return !1;
            this.ee = 1;
            var e = this.te.getTargetTransition(this.Ji);
            this.ie && (e = this.ie.getTargetTransition(t.gesture)), t.overrideTransition && (e = t.overrideTransition);
            var i = null,
                a = null;
            e && e.type && (i = new(o(e.type))(e), a = new(o(e.type))(e)), this.ie && this.ie.setTransition({
                engine: i,
                appDirection: t.appDirection,
                orientation: t.orientation,
                elementIds: t.elementIds || {},
                role: "start"
            }), this.te && this.te.setTransition({
                engine: a,
                appDirection: t.appDirection,
                orientation: t.orientation,
                elementIds: t.elementIds || {},
                role: "end"
            }), this.ie || this.te.setTransition({
                engine: null,
                appDirection: 1,
                orientation: 0,
                elementIds: t.elementIds || {},
                role: "end"
            })
        }
        s.prototype.getProgress = function() {
            return this.te ? this.te.getProgress() : this.ie ? 1 - this.ie.getProgress() : 0
        }, s.prototype.setProgress = function(t, e) {
            this.ee = t;
            var i = 1 - t;
            return a.all([this.ie.setProgress(i, e), this.te.setProgress(t, e)])
        }, s.prototype.terminate = function(t) {
            return t || 0 === t || (t = this.ee), this.setProgress(t), this.ie.resetAnimations(), this.te.resetAnimations(), !0
        }, i.exports = s
    }, function(t, e, i) {
        var a = t[14],
            o = t[15];
    
        function s(t) {
            a.call(this, t), this.qi = isNaN(parseFloat(t.opacity)) ? 1 : parseFloat(t.opacity), this.Fs = t.offset || 0, this.appDirection = 1
        }
        s.prototype = Object.create(a.prototype), s.ORIENTATION_X = 0, s.ORIENTATION_Y = 1, s.prototype.step = function() {
            a.prototype.step.call(this);
            var t = 100 * this.appDirection * (1 - this.Oi);
            Math.abs(t) < 1e-5 && (t = 0);
            var e = t * this.Fs,
                i = "";
            if (this.appDirection < 0) {
                var r = this.qi;
                return this.element.style.opacity = this.Oi * (1 - r) + r, this.orientation === s.ORIENTATION_X ? i = "translate3d(" + e + "%,0,-0.001px)" : this.orientation === s.ORIENTATION_Y && (i = "translate3d(0," + e + "%,-0.001px)"), this.element.style[o.transform] = i, void("-1" !== this.element.style.zIndex && (this.element.style.zIndex = "-1"))
            }
            this.orientation === s.ORIENTATION_X ? i = "translate3d(" + t + "%,0,0)" : this.orientation === s.ORIENTATION_Y && (i = "translate3d(0," + t + "%,0)"), this.element.style[o.transform] = i, "" !== this.element.style.zIndex && (this.element.style.zIndex = "")
        }, i.exports = s
    }, function(t, e, i) {
        var a = t[0],
            o = t[34],
            s = t[1];
    
        function r(e) {
            s.call(this), e || (e = {}), this.Tt = e.cardSet, this.y = e.startState || 0, this.Is = this.y, this.Ps = null, this.Ss = !1, this.Ji = null, this.Ct = t[35], this.pt = {}, this.Cs = this.Ls.bind(this), this.Ts = {
                left: [-1, 0],
                right: [1, 0],
                up: [-1, 1],
                down: [1, 1]
            }
        }
        for (var n in s.prototype) r.prototype[n] = s.prototype[n];
        r.prototype.getState = function() {
            return this.y
        }, r.prototype.getTargetState = function() {
            return this.Is
        }, r.prototype.resetTo = function(t) {
            if (this.Ps) {
                var e = this.Ps.getProgress();
                this.Ps.terminate(e > .5 ? 1 : 0), this.Ps = null
            }
            this.Is === this.y && (this.Is = t), this.y = t
        }, r.prototype.isActive = function() {
            return !this.Ps && this.y !== this.Is
        }, r.prototype.Ls = function() {
            if (this.Ss && this.Ps) {
                var t = this.Ps.getProgress();
                return t >= 1 ? (this.Ps.terminate(1) && (this.y = this.Is), this.Ps = null, this.trigger("settle", {
                    state: this.y
                }), this.Ls()) : t <= 0 ? (this.Ps.terminate(0) && (this.Is = this.y), this.Ps = null, this.trigger("settle", {
                    state: this.y
                }), this.Ls()) : this.Ps.setProgress(1, !0).then(this.Cs)
            }
        }, r.prototype.setProgress = function(t, e) {
            if (this.Ss = !1, this.Ps) {
                if (this.Is === t) return this.Ps.setProgress(e);
                this.Ps.terminate(0)
            }
            return (t || 0 === t) && (this.Is = t, this.Ps = this.getProgressor(this.y, t), this.Ps && this.Ps.setProgress(e)), this.Ps
        }, r.prototype.release = function(t) {
            return this.Ss = !0, this.Ps ? (t || 0 === t || (t = this.Ps.getProgress() < .5 ? 0 : 1), this.Ps.setProgress(t, !0).then(this.Cs)) : a.resolve()
        }, r.prototype.getProgress = function() {
            return this.Ps ? this.Ps.getProgress() : 0
        }, r.prototype.getProgressor = function(t, e) {
            var i = this.Ts[this.li][0],
                a = this.Ts[this.li][1],
                s = {
                    start: t,
                    end: e
                };
            return new o({
                cardSet: this.Tt,
                gesture: this.Ji,
                overrideTransition: this.qt,
                elementIds: s,
                appDirection: i,
                orientation: a
            })
        }, r.prototype.updateState = function(t) {
            var e = this.getState(),
                i = this.getTargetState();
            this.Ji = t.gesture || "swipeLeft", this.li = t.edge || "right", this.qt = t.transition, t.state === e && t.target === i || this.resetTo(t.state), t.target !== e && this.setProgress(t.target, t.progress), t.settleOnTarget >= 0 && this.release(t.settleOnTarget)
        }, i.exports = r
    }, function(t, e, i) {
        var a = t[1],
            o = t[29],
            s = t[30],
            r = t[32],
            n = t[4],
            l = t[5],
            d = t[33],
            c = t[36];
    
        function h(e) {
            a.call(this), this.q = null, this.gt = [], this.Ct = t[35], this.pt = {}, this.qt = !1, this.Ot = window.innerWidth > 768, this.y = null, this.St = null, this.It = 0, this.Nt = 0, this.At = null, this.Tt = new d, this.jt = new c({
                cardSet: this.Tt,
                startState: -1
            }), this.Et = new o(this.jt, this.Tt), this.Jt = new r(this.jt, this.Tt), this.Kt = this.Lt.bind(this), this.Pt = this.emit.bind(this, "stateChange"), this.Ut = this.Wt.bind(this), this.jt.on("settle", this.Ut), this.Jt.on("stateChange", this.Kt), this.Jt.on("stateChange", this.Pt), this.Et.on("stateChange", this.Kt), this.Et.on("stateChange", this.Pt), this.Tt.on("replace", function(t) {
                t.value.on("stateChange", this.Kt)
            }.bind(this)), this.Ot && this.ae(), e && this.setOptions(e)
        }
        for (var p in a.prototype) h.prototype[p] = a.prototype[p];
        h.prototype.ae = function() {
            this.kt = new s(this.jt, this.Tt), this.kt.on("stateChange", this.Kt), this.kt.on("stateChange", this.Pt), this.Jt.isDesktop = !0
        }, h.prototype.attach = function(t) {
            this.q = t, this.Jt.attach(t), this.Et.attach(window), this.Ot && this.kt.attach(window)
        }, h.prototype.detach = function() {
            this.Jt.detach(this.q), this.Et.detach(window), this.Ot && this.kt.detach(window), this.q = null
        }, h.prototype.Lt = function(t) {
            this.qt || ((t.keyNavigation || t.transition) && (this.Jt.detach(this.q), this.qt = !0), this.jt.updateState({
                state: t.state ? t.state : this.jt.getState(),
                target: t.target ? t.target : this.jt.getState(),
                progress: t.progress,
                settleOnTarget: t.settleOnTarget,
                edge: t.edge,
                gesture: t.gesture,
                transition: t.transition
            }))
        }, h.prototype.Wt = function(t) {
            this.qt && (this.Jt.attach(this.q), this.qt = !1), this.trigger("sendAnalytics", {
                type: "screen"
            }), this.kt && (this.kt.inTransition = !1);
            var e = this.jt.getState();
            e && this.Tt.get(e).checkTimed()
        }, h.prototype.resize = function(t, e) {
            this.Jt.resize(t, e)
        }, h.prototype.setSplashCardId = function(t) {
            this.At && this.Tt.get(this.At).setSplash(!1), this.Tt.get(t).setSplash(!0), this.At = t
        }, h.prototype.start = function() {
            this.goToStartCard(), this.Tt.forEach(t => {
                t.start()
            })
        }, h.prototype.goToStartCard = function() {
            this.jt.getState() !== this.At && this.goTo(this.At)
        }, h.prototype.goTo = function(t, e) {
            var i = this.jt.getState(),
                a = {
                    state: !i || i < 1 ? t : i,
                    target: t,
                    progress: 0,
                    gesture: "timed",
                    settleOnTarget: 1,
                    transition: {
                        title: "Instant",
                        type: "transitions/_Instant",
                        curve: "curves/easeOut",
                        duration: 500
                    }
                };
            e && (a = Object.assign(a, e)), this.Lt(a)
        }, h.prototype.edit = n.pathMin(1, function(t, e, i) {
            var a = -1,
                o = "",
                s = null;
            if (this.Tt.has(t[0]) ? o = t[0] : "-" !== (a = "-" === t[0] ? "-" : parseInt(t[0])) && (o = this.gt[a]), s = this.Tt.get(o)) {
                var r = i ? t.slice() : t.slice(1);
                s.edit(r, e, i)
            } else {
                if (!e.type) return;
                (s = new l(e.type)(e)).on("sendAnalytics", function(t) {
                    this.trigger("sendAnalytics", t)
                }.bind(this)), s.on("timed", function(t) {
                    this.Lt(t)
                }.bind(this));
                var n = s.scrollCheck();
                this.Ot && n && s.on("scrolling", function(t) {
                    this.kt.scrolling = t
                }.bind(this)), o = e.$id, "-" === a ? this.gt.push(o) : a >= 0 && (this.gt[a] = o), this.Tt.add(o, s)
            }
        }), h.prototype.domUpdate = function(t) {
            const e = Object.keys(t);
            e.forEach(e => {
                (t[e].value.hasClippingMask || t[e].value.maskId) && this.domGenerator.zt.xt.addMaskContent(t[e].value)
            }), e.forEach(e => {
                const i = t[e].path.split("/");
                i.splice(0, 2), this.edit(i, t[e].value, "replace")
            })
        }, h.prototype.setMediaQuery = function(t) {
            this.Tt.forEach(e => {
                e.setMediaQuery(t)
            })
        }, i.exports = h
    }, function(t, e, i) {
        var a = t[37],
            o = t[1],
            s = t[4];
    
        function r(t) {
            o.call(this), this.ki = t, this.deck = new a, this.deck.attach(this.ki), this.setInnerHeightVar = this.setInnerHeightVar.bind(this), this.pollHeight = this.pollHeight.bind(this), this.Si = null, this.Ui = 0, this.$i = 0
        }
        for (var n in o.prototype) r.prototype[n] = o.prototype[n];
        r.prototype.resize = function(t, e) {
            this.deck.resize(t, e), "safari" === this.ji ? window.setTimeout(function() {
                this.setInnerHeightVar(window.innerHeight)
            }.bind(this), 300) : this.setInnerHeightVar(window.innerHeight)
        }, r.prototype.edit = s.route({
            settings: s.expand(function(t, e) {
                t && (t.backgroundColor && (document.body.style.backgroundColor = t.backgroundColor), t.splashCardId && this.deck.setSplashCardId(t.splashCardId))
            }),
            cards: function(t, e, i) {
                this.deck.edit(t, e, i)
            },
            domUpdate: function(t, e, i) {
                this.deck.domUpdate(e)
            },
            mediaQueries: function(t, e, i) {
                const a = e,
                    o = Object.keys(a);
                this.deck.setMediaQuery(a[o[0]])
            },
            styles: function(t, e, i) {
                const a = document.body.lastChild,
                    o = document.createElement("style");
                o.innerHTML = e, o.classList.add("live-style"), document.body.appendChild(o), "STYLE" === a.nodeName && a.classList.contains("live-style") && a.remove()
            }
        }), r.prototype.goTo = function(t) {
            if ("cards" === t[1]) return this.deck.goTo(parseInt(t[2]))
        }, r.prototype.start = function() {
            this.ji = this.getUserAgentException(), this.ki.classList.remove("hidden-template"), this.deck.start(), this.deck.resize(window.innerWidth, window.innerHeight), "facebook" === this.ji ? this.Si = window.setInterval(this.pollHeight, 100) : this.setInnerHeightVar(window.innerHeight)
        }, r.prototype.pollHeight = function() {
            var t = window.innerHeight;
            this.$i === t && this.Ui++, this.$i = t, this.Ui < 10 ? this.setInnerHeightVar(this.$i) : (this.$i = 0, window.clearInterval(this.Si))
        }, r.prototype.setInnerHeightVar = function(t) {
            document.documentElement.style.setProperty("--ihpx", `${t}px`), document.documentElement.style.setProperty("--ih", `${t}`)
        }, r.prototype.goToStartCard = function() {
            this.deck.goToStartCard()
        }, r.prototype.getUserAgentException = function() {
            var t = window.navigator.userAgent.toString().toLowerCase();
            return -1 != t.indexOf("fban") || -1 != t.indexOf("fbav") ? "facebook" : -1 != t.indexOf("safari") && -1 == t.indexOf("chrome") && -1 == t.indexOf("chromium") ? "safari" : "browser"
        }, i.exports = r
    }, function(t, e, i) {
        i.exports = {
            onEvent: function(t) {
                "screen" === t.type && window.fetch 
            }
        }
    }, function(t, e, i) {
        var a = t[28],
            o = document.querySelector("#famous-application"),
            s = new(0, t[38])(o);
    
        function r(t) {
            l(), s.resize(window.innerWidth, window.innerHeight)
        }
        s.edit([], a), window.addEventListener("resize", r), window.requestAnimationFrame(r), window.addEventListener("deviceorientation", l, !0), document.addEventListener("gesturestart", function(t) {
            t.preventDefault()
        }, !0), document.addEventListener("dragstart", function(t) {
            "IMAGE" === t.target.tagName.toUpperCase() && t.preventDefault()
        }), document.addEventListener("touchstart", function(t) {
            var e = t.target.tagName;
            "A" !== e && "INPUT" !== e && "BUTTON" !== e || t.preventDefault()
        }), s.deck.on("sendAnalytics", t[39].onEvent), s.start();
        var n = !0;
    
        function l() {
            window.isFamousStudio || !/iPhone|iPad|iPod/.test(window.navigator.userAgent) && !/Android/.test(window.navigator.userAgent) || (window.matchMedia("(orientation: landscape)").matches ? function() {
                const t = document.getElementById("unsupported-orientation");
                if (t.className = "show", !document.getElementById("bg-image-clone")) {
                    const e = document.querySelector(".card");
                    let i;
                    if ((e ? e.querySelectorAll("svg") : []).forEach(t => {
                            t.preserveAspectRatio && t.preserveAspectRatio.baseVal && 2 === t.preserveAspectRatio.baseVal.meetOrSlice && (i = t)
                        }), i) {
                        const e = i.cloneNode(!0);
                        e.id = "bg-image-clone", e.preserveAspectRatio.baseVal.align = 4, t.prepend(e)
                    }
                }
            }() : document.getElementById("unsupported-orientation").className = "")
        }
        window.addEventListener("unhandledrejection", function(t) {
            if (n && "The request is not allowed by the user agent or the platform in the current context, possibly because the user denied permission." === t.reason.message && t.reason.stack.indexOf("play") >= 0) {
                n = !1;
                var e = document.getElementById("low-power-mode-notification");
                e.classList.add("display"), setTimeout(function() {
                    e.classList.add("show")
                }, 1e3), document.getElementById("power-saving-proceed-button").addEventListener("click", function() {
                    e.remove()
                }), document.getElementById("power-saving-proceed-button").addEventListener("touchstart", function() {
                    e.remove()
                })
            }
        }), document.addEventListener("DOMContentLoaded", function(t) {
            var e, i = (e = RegExp("[?&]ref=([^&]*)").exec(window.location.search)) && decodeURIComponent(e[1].replace(/\+/g, " "));
            i && (document.cookie = "ref=" + i + ";path=/")
        }), window.onload = (() => {
            var t = document.querySelector(".card"),
                e = t ? t.querySelector("video[autoplay]") : void 0;
            e && e.play(), window.parent.location !== window.location && (document.body.style.zoom = 1.0000001, setTimeout(function() {
                document.body.style.zoom = 1
            }, 100))
        })
    }]);

}