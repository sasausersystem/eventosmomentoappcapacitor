import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.sasasocial.eventomomentonew',
  appName: 'EventosMomento APP',
  webDir: 'www',
  bundledWebRuntime: false,
  android: {

  },
  ios: {
    
  },
  plugins: {
    LocalNotifications: {
      "iconColor": '#000000',
      "smallIcon": 'ic_launcher_round',
      "sound": "notificationsound.wav"
    }
  }
};

export default config;
